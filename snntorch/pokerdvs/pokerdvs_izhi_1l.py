from pprint import pprint
from tonic import CachedDataset
import csv
import datetime
import gc
import sys
import time
from prettytable.prettytable import from_csv
import snntorch as snn
import tonic
from snntorch import IzhikevichParameter, IzhikevichState, surrogate
from snntorch import functional as SF
from tqdm import tqdm, trange
import torch
import torch.nn as nn
import matplotlib.pyplot as plt
import numpy as np

gc.collect()
torch.cuda.empty_cache()
sensor_size = tonic.datasets.POKERDVS.sensor_size
transform = tonic.transforms.Compose(
    [
        tonic.transforms.Downsample(time_factor=0.001),
        tonic.transforms.ToFrame(sensor_size=sensor_size,
                                #  time_window=1,
                                n_time_bins=2
                                 ),
        # tonic.transforms.Denoise(filter_time=10),
        # tonic.transforms.ToTimesurface(sensor_size=tonic.datasets.POKERDVS.sensor_size,
        #    tau=1000,
        #    decay='exp'
        #    )
    ]
)
dtype = torch.float
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

trainset = tonic.datasets.POKERDVS(
    save_to='../../data', transform=transform, train=True)
testset = tonic.datasets.POKERDVS(
    save_to='../../data', transform=transform, train=False)

#Frames
cached_dataset_train = CachedDataset(
    trainset, cache_path='../../data/cached_datasets/pokerdvs_TF_nt2_train')
cached_dataset_test = CachedDataset(
    testset, cache_path='../../data/cached_datasets/pokerdvs_TF_nt2_test')

# #Timesurfaces
# cached_dataset_train = CachedDataset(
#     trainset, cache_path='../../data/cached_datasets/pokerdvs_TTS_f100_train')
# cached_dataset_test = CachedDataset(
#     testset, cache_path='../../data/cached_datasets/pokerdvs_TTS_f100_test')

# train samples: 48, test samples: 20
BATCH_SIZE = 8
train_subset_len = 48
test_subset_len = 20

train_subset = torch.utils.data.random_split(
    cached_dataset_train, [train_subset_len, len(trainset)-train_subset_len])[0]
test_subset = torch.utils.data.random_split(
    cached_dataset_test, [test_subset_len, len(testset)-test_subset_len])[0]

train_loader = torch.utils.data.DataLoader(dataset=train_subset,
                                           batch_size=BATCH_SIZE,
                                           collate_fn=tonic.collation.PadTensors(),
                                           shuffle=True)

test_loader = torch.utils.data.DataLoader(dataset=test_subset,
                                          batch_size=BATCH_SIZE,
                                          collate_fn=tonic.collation.PadTensors(),
                                          )

train_len = train_subset_len/BATCH_SIZE
test_len = test_subset_len/BATCH_SIZE

EPOCHS = 25
lr = 0.002
num_inputs = np.product(sensor_size)
num_hidden = 100
num_outputs = len(trainset.classes)
slope = 25
spike_grad = surrogate.fast_sigmoid(slope=slope)  # slope = 25
# spike_grad = surrogate.LSO(slope = slope) #slope = 0.1
# spike_grad = surrogate.SSO(mean=0.1, variance=0.2) #mean=0.1, variance=0.2
# spike_grad = surrogate.SFS(slope=slope, B=0.9)
# spike_grad = surrogate.spike_rate_escape(beta=0.9, slope=slope)
dt=0.5
layers = (num_hidden)

params = IzhikevichParameter(a=0.1,
                             b=0.25,
                             c=-65,
                             d=8,
                             v_thres=-55)


class Net(nn.Module):
    def __init__(self, dt):
        super().__init__()

        # initialize layers
        self.fc1 = nn.Linear(num_inputs, num_hidden)
        self.fc2 = nn.Linear(num_hidden, num_outputs)
        self.izhi1 = snn.Izhikevich(spike_grad=spike_grad, p=params, dt=dt)
        self.izhi2 = snn.Izhikevich(spike_grad=spike_grad, p=params, dt=dt)

    def forward(self, x):
        seq_len, batch, _, _, _, = x.shape
        x = x.reshape(seq_len, batch, -1)
        s1 = so = IzhikevichState(
            v=torch.tensor(-65.0, requires_grad=True, device=device),
            u=torch.tensor(-65.0, device=device) * params.b)
        voltages = []
        spikes = []
                                  
        for ts in range(seq_len):
            z = x[ts, :, :]
            z = (z > 0).type(torch.float)
            cur1 = self.fc1(z)
            spk1, s1 = self.izhi1(cur1, s1)
            cur2 = self.fc2(spk1)
            spk2, so = self.izhi2(cur2, so)
            voltages.append(so[0]) 
            spikes.append(spk2)
        return torch.stack(spikes), torch.stack(voltages)

net = Net(dt=dt).to(device)
optimizer = torch.optim.Adam(net.parameters(), lr=lr)
# loss_fn = SF.mse_membrane_loss(on_target=1.0, off_target=0)
loss_fn = SF.ce_max_membrane_loss()
# loss_fn = SF.mse_count_loss(correct_rate=0.75, incorrect_rate=0.2)
# loss_fn = SF.ce_rate_loss()
# loss_fn = SF.ce_count_loss()
log_softmax = nn.LogSoftmax(dim=1)


def train(train_loader, device, optimizer,):
    losses = []
    correct = 0
    net.train()
    for data, target in tqdm(
            train_loader, leave=False, total=int(train_len), colour='green'):
        target = target.to(device)
        data = data.float().to(device)
        optimizer.zero_grad()
        spikes, output = net(data)
        log_y_p, _ = torch.max(output, 0)
        log_y_p = log_softmax(log_y_p)
        pred = log_y_p.argmax(dim=1, keepdim=True)
        correct += pred.eq(target.view_as(pred)).sum().item()
        loss = loss_fn(output, target)
        loss.backward()
        optimizer.step()
        losses.append(loss.item())

    accuracy = correct / len(train_loader.dataset)
    mean_loss = np.mean(losses)
    return losses, mean_loss, accuracy


def test(device, test_loader):
    test_loss = 0
    correct = 0
    losses = []
    with torch.no_grad():
        for data, target in tqdm(
                test_loader, total=int(test_len), leave=False, colour='red'):
            target = target.to(device)
            data = data.to(device)
            spikes, output = net(data)
            log_y_p, _ = torch.max(output, 0)
            log_y_p = log_softmax(log_y_p)
            pred = log_y_p.argmax(dim=1, keepdim=True)
            test_loss = loss_fn(output, target).item()
            pred.eq(target.view_as(pred)).sum().item()
            correct += pred.eq(target.view_as(pred)).sum().item()
            losses.append(test_loss)
    test_loss = np.mean(losses)
    accuracy = correct / len(test_loader.dataset)
    return test_loss, accuracy


training_losses = []
mean_losses = []
test_losses = []
train_accuracies = []
test_accuracies = []

torch.autograd.set_detect_anomaly(True)

pbar = tqdm(total=EPOCHS, colour='blue', leave=False)
start_time = time.time()
for epoch in range(EPOCHS):
    training_loss, mean_loss, train_accuracy = train(
        device=device, train_loader=train_loader, optimizer=optimizer)
    test_loss, test_accuracy = test(device=device, test_loader=test_loader)
    training_losses += training_loss
    mean_losses.append(mean_loss)
    test_losses.append(test_loss)
    train_accuracies.append(train_accuracy)
    test_accuracies.append(test_accuracy)
    pbar.set_description(
        f"Acc: [{train_accuracies[-1]:.2f}|{test_accuracies[-1]:.2f}], Loss: [{mean_losses[-1]:.2f}|{test_losses[-1]:.2f}]")
    pbar.update()

print(
    f"Train Accuracy:{train_accuracies[-1]:.2f}, Test Accuracy{test_accuracies[-1]:.2f}")
runtime = time.time() - start_time

date = datetime.datetime.now()
filetype = '.png'
d = str(date.day) + "_" + str(date.month) + "_" + \
    str(date.year) + "_" + str(date.time())
d = d.split('.')[0]
scriptname = sys.argv[0].split('.')[0]
tf_str = str(transform).replace(" ", "").splitlines(keepends=False)[1:-1]
loss_function = str(loss_fn).split('.')[2]
spike_gradient = str(spike_grad).split('.')[0]
try:
    tf_str[1] = tf_str[1].replace(",decay='exp'", "")
except:
    pass
opt = str(optimizer).replace(
    " ", "").strip().splitlines(keepends=False)[2:-1]
fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.plot(mean_losses)
plt.plot(test_losses)
plt.title("Loss Curves")
plt.legend(["Train Loss", "Test Loss"])
plt.xlabel("Epoch")
plt.ylabel("Loss")
# plt.show()
plt.savefig("./plots/" + scriptname + "/loss" + "_" + d + filetype)

np.save("./plots/" + scriptname + "/loss_data" +
        "_" + d + '.npy', [mean_losses, test_losses])

fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.plot(train_accuracies)
plt.plot(test_accuracies)
plt.title("Accuracy Curves")
plt.legend(["Train Accuracy", "Test Accuracy"])
plt.xlabel("Epoch")
plt.ylabel("Accuracy")
# plt.show()
plt.savefig("./plots/" + scriptname + "/acc" + "_" + d + filetype)

np.save("./plots/" + scriptname + "/acc_data" +
        "_" + d + '.npy', [train_accuracies, test_accuracies])

filename_raw = 'snntorch_pokerdvs_izhi_results_raw.csv'
filename_pretty = 'snntorch_pokerdvs_izhi_results_pretty.csv'

fields = ['n',
          ' date',
          ' filename',
          ' device',
          ' runtime',
          ' accuracy (train, test)',
          ' loss (train, test)',
          ' epochs',
          ' batch_size',
          ' opt',
          ' layers',
          ' loss function',
          ' spike gradient',
          ' v_th',
          ' slope',
          ' a',
          ' b',
          ' c',
          ' d',
          ' bias',
          ' transform']

num_rows = 0
for r in open(filename_raw):
    num_rows += 1
if num_rows != 0:
    num_rows -= 1

rows = [[num_rows,
        d.replace('_', ' '),
        sys.argv[0].split('.')[0] + '.py',
        device,
        runtime,
        (train_accuracies[-1], test_accuracies[-1]),
        (mean_losses[-1], test_losses[-1]),
        EPOCHS,
        BATCH_SIZE,
        opt,
        layers,
        loss_function.split(' ')[0],
        spike_gradient.split(' ')[1],
        params.v_thres,
        slope,
        params.a,
        params.b,
        params.c,
        params.d,
        params.bias,
        tf_str]]


with open(filename_raw, 'a') as csvfile:
    csvwriter = csv.writer(csvfile)
    if num_rows == 0:
        csvwriter.writerow(fields)
    csvwriter.writerows(rows)

with open(filename_raw) as fp:
    table = from_csv(fp)

f = open(filename_pretty, "w")
f.write(table.get_string())
f.close()
