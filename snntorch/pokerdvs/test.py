import datetime
import gc
import sys
import time
import snntorch as snn
import tonic
from snntorch import IzhikevichParameter, IzhikevichState, surrogate
from snntorch import functional as SF
from tqdm import tqdm, trange
import torch
import torch.nn as nn
import matplotlib.pyplot as plt
import numpy as np

gc.collect()
torch.cuda.empty_cache()

transform = tonic.transforms.Compose(
    [
        tonic.transforms.Downsample(time_factor=0.001),
        tonic.transforms.ToFrame(time_window=1,
                                 sensor_size=tonic.datasets.NMNIST.sensor_size,
                                 include_incomplete=True)
    ]
)
dtype = torch.float
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

first_saccade_only = True
trainset = tonic.datasets.NMNIST(
    save_to='../../data', 
    transform=transform,
    train=True,
    first_saccade_only=first_saccade_only)

testset = tonic.datasets.NMNIST(
    save_to='../../data', 
    transform=transform,
    train=False,
    first_saccade_only=first_saccade_only)

# train samples: 48, test samples: 20
BATCH_SIZE = 16
train_subset_len = 1280
test_subset_len = 256

train_subset = torch.utils.data.random_split(
    trainset, [train_subset_len, len(trainset)-train_subset_len])[0]
test_subset = torch.utils.data.random_split(
    testset, [test_subset_len, len(testset)-test_subset_len])[0]

train_loader = torch.utils.data.DataLoader(dataset=train_subset,
                                           batch_size=BATCH_SIZE,
                                           collate_fn=tonic.collation.PadTensors(),
                                           shuffle=True)

test_loader = torch.utils.data.DataLoader(dataset=test_subset,
                                          batch_size=BATCH_SIZE,
                                          collate_fn=tonic.collation.PadTensors(),
                                          shuffle=True)

train_len = train_subset_len/BATCH_SIZE
test_len = test_subset_len/BATCH_SIZE

EPOCHS = 25
num_inputs = np.product(trainset.sensor_size)
num_hidden = 100
num_outputs = len(trainset.classes)
beta = 0.85
alpha = 0.9
threshold = 1.0
spike_grad = surrogate.fast_sigmoid(slope=15)
# spike_grad = surrogate.LSO()
# spike_grad = surrogate.SSO(mean=0.1, variance=0.2)
# spike_grad = surrogate.SFS(slope=25, B=0.9)
# spike_grad = surrogate.spike_rate_escape(beta=0.9, slope=25)
dt=0.25

params = IzhikevichParameter(a=0.02, 
                             b=0.2,
                             c=-65,
                             d=8,
                             bias=140,
                             v_thres=30)

state = IzhikevichState(v=torch.tensor(-65.0, requires_grad=True),
                        u=torch.tensor(-65) * params.b)

# Define Network
class Net(nn.Module):
    def __init__(self, dt):
        super().__init__()

        # initialize layers
        self.fc1 = nn.Linear(num_inputs, num_hidden, device)
        self.fc2 = nn.Linear(num_hidden, num_outputs, device)
        self.izhi1 = snn.Izhikevich(p=params, 
                                    spike_grad=spike_grad, 
                                    dt=dt)
        
        self.izhi2 = snn.Izhikevich(p=params, 
                                    spike_grad=spike_grad, 
                                    dt=dt)

    def forward(self, x):
        seq_len, batch, _, _, _, = x.shape
        x = x.reshape(seq_len, batch, -1)
        # v1, u1 = self.izhi1.init_izhikevich()
        # v2, u2 = self.izhi2.init_izhikevich()
        v1 = torch.tensor(-65.0, requires_grad=True)
        v2 = torch.tensor(-65.0, requires_grad=True)
        u1 = torch.tensor(-65.0) * params.b
        u2 = torch.tensor(-65.0) * params.b
        voltages = []
                                  
        for ts in range(seq_len):
            z = x[ts, :, :]
            cur1 = self.fc1(z)
            spk1, v1, u1 = self.izhi1(cur1, v1, u1)
            cur2 = self.fc2(spk1)
            spk2, v2, u2 = self.izhi2(cur2, v2, u2)
            print(v2)
            voltages.append(spk2) 
        return torch.stack(voltages)

net = Net(dt=dt).to(device)
optimizer = torch.optim.Adam(net.parameters(), lr=0.002)
# loss_fn = SF.mse_count_loss()
# loss_fn = SF.mse_membrane_loss()
loss_fn = SF.ce_rate_loss()


def train(train_loader, device, optimizer,):
    losses = []
    correct = 0
    net.train()
    for data, target in tqdm(
            train_loader, leave=False, total=int(train_len), colour='green'):
        target = target.to(device)
        data = data.to(device)
        optimizer.zero_grad()
        output = net(data)
        _, pred = output.sum(dim=0).max(1)
        correct += (pred == target).sum().item()
        loss = loss_fn(output, target)
        loss.backward()
        optimizer.step()
        losses.append(loss.item())

    accuracy = correct / len(train_loader.dataset)
    mean_loss = np.mean(losses)
    return losses, mean_loss, accuracy


def test(device, test_loader):
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in tqdm(
                test_loader, total=int(test_len), leave=False, colour='red'):
            target = target.to(device)
            data = data.to(device)
            output = net(data)
            test_loss += loss_fn(output, target).item()
            _, pred = output.sum(dim=0).max(1)
            correct += (pred == target).sum().item()
    test_loss /= len(test_loader.dataset)
    accuracy = correct / len(test_loader.dataset)
    return test_loss, accuracy


training_losses = []
mean_losses = []
test_losses = []
train_accuracies = []
test_accuracies = []

torch.autograd.set_detect_anomaly(True)

pbar = tqdm(total=EPOCHS, colour='blue', leave=False)
start_time = time.time()
for epoch in range(EPOCHS):
    training_loss, mean_loss, train_accuracy = train(
        device=device, train_loader=train_loader, optimizer=optimizer)
    test_loss, test_accuracy = test(device=device, test_loader=test_loader)
    training_losses += training_loss
    mean_losses.append(mean_loss)
    test_losses.append(test_loss)
    train_accuracies.append(train_accuracy)
    test_accuracies.append(test_accuracy)
    pbar.update()
print(
    f"Train Accuracy:{train_accuracies[-1]:.2f}, Test Accuracy{test_accuracies[-1]:.2f}")
runtime = time.time() - start_time

date = datetime.datetime.now()
filetype = '.png'
d = str(date.day) + "_" + str(date.month) + "_" + \
    str(date.year) + "_" + str(date.time())
d = d.split('.')[0]
scriptname = sys.argv[0].split('.')[0]
tf_str = str(transform).replace(" ", "").splitlines(keepends=False)[1:-1]
loss_function = str(loss_fn).split('.')[2]
spike_gradient = str(spike_grad).split('.')[0]
