import csv
import datetime
import gc
import sys
import time
from prettytable.prettytable import from_csv
import snntorch as snn
import tonic
from snntorch import IzhikevichParameter, IzhikevichState, surrogate
from snntorch import functional as SF
from tqdm import tqdm, trange
import torch
import torch.nn as nn
import matplotlib.pyplot as plt
import numpy as np
from tonic import CachedDataset
import torchvision as tv

gc.collect()
torch.cuda.empty_cache()


dtype = torch.float
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
random_transforms = tonic.transforms.Compose(
    [
        torch.tensor,
        tv.transforms.RandomAffine(degrees=(0, 0),
                                   scale=(0.9, 1.0))
    ]
)
transform = tonic.transforms.Compose(
    [
        tonic.transforms.Downsample(time_factor=0.001),
        tonic.transforms.ToFrame(sensor_size=tonic.datasets.SHD.sensor_size,
                                #  time_window=5,
                                n_event_bins=50
                                 )
    ]
)

trainset = tonic.datasets.SHD(save_to='../../data',
                              transform=transform,
                              train=True)

testset = tonic.datasets.SHD(save_to='../../data',
                             transform=transform,
                             train=False)

#Frames
# cached_trainset = CachedDataset(
#     trainset, cache_path='../../data/cached_datasets/shd_TF_tw1_train')
# cached_testset = CachedDataset(
#     testset, cache_path='../../data/cached_datasets/shd_TF_tw1_test')

#Frames 5ms time window
cached_trainset = CachedDataset(
    trainset,
    transform=random_transforms, 
    cache_path='../../data/cached_datasets/shd_TF_ne50_train')
cached_testset = CachedDataset(
    testset, cache_path='../../data/cached_datasets/shd_TF_ne50_test')


# cached_trainset = CachedDataset(
#     trainset, cache_path='../../data/cached_datasets/shd_TF_tw25_train')
# cached_testset = CachedDataset(
#     testset, cache_path='../../data/cached_datasets/shd_TF_tw25_test')

BATCH_SIZE = 64
# train samples: 8156, test samples: 2264
train_subset_len = 8156
test_subset_len = 2264

train_subset = torch.utils.data.random_split(
    cached_trainset, [train_subset_len, len(trainset)-train_subset_len])[0]
test_subset = torch.utils.data.random_split(
    cached_testset, [test_subset_len, len(testset)-test_subset_len])[0]

train_loader = torch.utils.data.DataLoader(dataset=train_subset,
                                           batch_size=BATCH_SIZE,
                                           collate_fn=tonic.collation.PadTensors(),
                                           shuffle=True)

test_loader = torch.utils.data.DataLoader(dataset=test_subset,
                                          batch_size=BATCH_SIZE,
                                          collate_fn=tonic.collation.PadTensors(),
                                          )

train_len = train_subset_len/BATCH_SIZE
test_len = test_subset_len/BATCH_SIZE

EPOCHS = 100
lr = 0.002
num_inputs = 700
num_hidden = 100
num_outputs = 20
slope = 25
spike_grad = surrogate.fast_sigmoid(slope=slope)  # slope = 25
# spike_grad = surrogate.LSO(slope = slope) #slope = 0.1
# spike_grad = surrogate.SSO(mean=0.1, variance=0.2) #mean=0.1, variance=0.2
# spike_grad = surrogate.SFS(slope=slope, B=0.9)
# spike_grad = surrogate.spike_rate_escape(beta=0.9, slope=slope)
dt=0.5
layers = (num_hidden)

params = IzhikevichParameter(a=0.01,
                             b=0.25,
                             c=-65,
                             d=2,
                             v_thres=-45)

# Define Network
class Net(nn.Module):
    def __init__(self, dt):
        super().__init__()

        # initialize layers
        self.fc1 = nn.Linear(num_inputs, num_hidden)
        self.fc2 = nn.Linear(num_hidden, num_outputs)
        self.izhi1 = snn.Izhikevich(p=params, spike_grad=spike_grad, dt=dt)
        self.izhi2 = snn.Izhikevich(p=params, spike_grad=spike_grad, dt=dt)

        
    def forward(self, x):
        seq_len, batch, _, _,  = x.shape
        x = x.reshape(seq_len, batch, -1)
        s1 = so = IzhikevichState(
            v=torch.tensor(-65.0, requires_grad=True, device=device),
            u=torch.tensor(-65.0, device=device) * params.b)
        voltages = []
        spikes = []
                                  
        for ts in range(seq_len):
            z = x[ts, :, :]
            cur1 = self.fc1(z)
            spk1, s1 = self.izhi1(cur1, s1)
            cur2 = self.fc2(spk1)
            spk2, so = self.izhi2(cur2, so)
            voltages.append(so[0])
            spikes.append(spk2) 
        return torch.stack(spikes), torch.stack(voltages)

net = Net(dt=dt).to(device)
optimizer = torch.optim.Adam(net.parameters(), lr=lr, weight_decay=0.005)
# loss_fn = SF.mse_count_loss(correct_rate=0.75, incorrect_rate=0.2)
# loss_fn = SF.mse_membrane_loss(on_target=0.9, off_target=0.1)
# loss_fn = SF.ce_rate_loss()
loss_fn = SF.ce_max_membrane_loss()
# loss_fn = SF.ce_count_loss()
# loss_fn = nn.CrossEntropyLoss()
log_softmax = nn.LogSoftmax(dim=1)

def train(train_loader, device, optimizer,):
    losses = []
    correct = 0
    net.train()
    for data, target in tqdm(
            train_loader, leave=False, total=int(train_len), colour='green'):
        target = target.to(device)
        data = data.to(device)
        optimizer.zero_grad()
        spikes, output = net(data)
        log_y_p, _ = torch.max(output, 0)
        log_y_p = log_softmax(log_y_p)
        pred = log_y_p.argmax(dim=1, keepdim=True)
        correct += pred.eq(target.view_as(pred)).sum().item()
        loss = loss_fn(output, target)
        loss.backward()
        optimizer.step()
        losses.append(loss.item())

    accuracy = correct / len(train_loader.dataset)
    mean_loss = np.mean(losses)
    return losses, mean_loss, accuracy


def test(device, test_loader):
    test_loss = 0
    correct = 0
    losses = []
    with torch.no_grad():
        for data, target in tqdm(
                test_loader, total=int(test_len), leave=False, colour='red'):
            target = target.to(device)
            data = data.to(device)
            spikes, output = net(data)
            log_y_p, _ = torch.max(output, 0)
            log_y_p = log_softmax(log_y_p)
            pred = log_y_p.argmax(dim=1, keepdim=True)
            test_loss = loss_fn(output, target).item()
            pred.eq(target.view_as(pred)).sum().item()
            correct += pred.eq(target.view_as(pred)).sum().item()
            losses.append(test_loss)
    test_loss = np.mean(losses)
    accuracy = correct / len(test_loader.dataset)
    return test_loss, accuracy


training_losses = []
mean_losses = []
test_losses = []
train_accuracies = []
test_accuracies = []

torch.autograd.set_detect_anomaly(True)

pbar = tqdm(total=EPOCHS, colour='blue', leave=False)
start_time = time.time()
for epoch in range(EPOCHS):
    training_loss, mean_loss, train_accuracy = train(
        device=device, train_loader=train_loader, optimizer=optimizer)
    test_loss, test_accuracy = test(device=device, test_loader=test_loader)
    training_losses += training_loss
    mean_losses.append(mean_loss)
    test_losses.append(test_loss)
    train_accuracies.append(train_accuracy)
    test_accuracies.append(test_accuracy)
    pbar.set_description(
        f"Acc: [{train_accuracies[-1]:.2f}|{test_accuracies[-1]:.2f}], Loss: [{mean_losses[-1]:.2f}|{test_losses[-1]:.2f}]")
    pbar.update()
print(
    f"Train Accuracy:{train_accuracies[-1]:.2f}, Test Accuracy{test_accuracies[-1]:.2f}")
runtime = time.time() - start_time

date = datetime.datetime.now()
filetype = '.png'
d = str(date.day) + "_" + str(date.month) + "_" + \
    str(date.year) + "_" + str(date.time())
d = d.split('.')[0]
scriptname = sys.argv[0].split('.')[0]
tf_str = None
loss_function = str(loss_fn).split('.')[2]
spike_gradient = str(spike_grad).split('.')[0]
opt = str(optimizer).replace(
    " ", "").strip().splitlines(keepends=False)[2:-1]
fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.plot(mean_losses)
plt.plot(test_losses)
plt.title("Loss Curves")
plt.legend(["Train Loss", "Test Loss"])
plt.xlabel("Epoch")
plt.ylabel("Loss")
# plt.show()
plt.savefig("./plots/" + scriptname + "/loss" + "_" + d + filetype)

np.save("./plots/" + scriptname + "/loss_data" +
        "_" + d + '.npy', [mean_losses, test_losses])

fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.plot(train_accuracies)
plt.plot(test_accuracies)
plt.title("Accuracy Curves")
plt.legend(["Train Accuracy", "Test Accuracy"])
plt.xlabel("Epoch")
plt.ylabel("Accuracy")
# plt.show()
plt.savefig("./plots/" + scriptname + "/acc" + "_" + d + filetype)

np.save("./plots/" + scriptname + "/acc_data" +
        "_" + d + '.npy', [train_accuracies, test_accuracies])

filename_raw = 'snntorch_shd_izhi_results_raw.csv'
filename_pretty = 'snntorch_shd_izhi_results_pretty.csv'

fields = ['n',
          ' date',
          ' filename',
          ' device',
          ' runtime',
          ' accuracy (train, test)',
          ' loss (train, test)',
          ' epochs',
          ' batch_size',
          ' opt',
          ' layers',
          ' loss function',
          ' spike gradient',
          ' v_th',
          ' slope',
          ' a',
          ' b',
          ' c',
          ' d',
          ' bias',
          ' transform']

num_rows = 0
for r in open(filename_raw):
    num_rows += 1
if num_rows != 0:
    num_rows -= 1

rows = [[num_rows,
        d.replace('_', ' '),
        sys.argv[0].split('.')[0] + '.py',
        device,
        runtime,
        (train_accuracies[-1], test_accuracies[-1]),
        (mean_losses[-1], test_losses[-1]),
        EPOCHS,
        BATCH_SIZE,
        opt,
        layers,
        loss_function.split(' ')[0],
        spike_gradient.split(' ')[1],
        params.v_thres,
        slope,
        params.a,
        params.b,
        params.c,
        params.d,
        params.bias,
        tf_str]]


with open(filename_raw, 'a') as csvfile:
    csvwriter = csv.writer(csvfile)
    if num_rows == 0:
        csvwriter.writerow(fields)
    csvwriter.writerows(rows)

with open(filename_raw) as fp:
    table = from_csv(fp)

f = open(filename_pretty, "w")
f.write(table.get_string())
f.close()
