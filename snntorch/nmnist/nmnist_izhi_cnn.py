from tonic import CachedDataset
import csv
import datetime
import gc
import sys
import time
from prettytable.prettytable import from_csv
import snntorch as snn
import tonic
from snntorch import IzhikevichParameter, IzhikevichState, surrogate, IzhikevichRecurrentState
from snntorch import functional as SF
import torch.nn.functional as F
from tqdm import tqdm, trange
import torch
import torch.nn as nn
import matplotlib.pyplot as plt
import numpy as np

gc.collect()
torch.cuda.empty_cache()
torch.manual_seed(4663028626650247043)
sensor_size = tonic.datasets.NMNIST.sensor_size
transform = tonic.transforms.Compose(
    [
        tonic.transforms.Downsample(time_factor=0.001),
        tonic.transforms.ToFrame(sensor_size=sensor_size,
                                #  time_window=1,
                                n_event_bins=25
                                 ),
        # tonic.transforms.Denoise(filter_time=10),
        # tonic.transforms.ToTimesurface(sensor_size=sensor_size,
        #    tau=2000,
        #    decay='exp'
        #    )
    ]
)
dtype = torch.float
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
first_saccade_only = False

trainset = tonic.datasets.NMNIST(
    save_to='../../data',
    first_saccade_only=first_saccade_only,
    transform=transform,
    train=True)
testset = tonic.datasets.NMNIST(
    save_to='../../data',
    transform=transform,
    first_saccade_only=first_saccade_only,
    train=False)

#Frames
cached_trainset = CachedDataset(
    trainset, cache_path='../../data/cached_datasets/nmnist_TF_ne25_train')
cached_testset = CachedDataset(
    testset, cache_path='../../data/cached_datasets/nmnist_TF_ne25_test')

# #Timesurfaces
# cached_trainset = CachedDataset(
#     trainset, cache_path='../../data/cached_datasets/nmnist_TTS_f10_train')
# cached_testset = CachedDataset(
#     testset, cache_path='../../data/cached_datasets/nmnist_TTS_f10_test')

# train samples: 70000, test samples: 10000
BATCH_SIZE = 64
train_subset_len = 60000
test_subset_len = 10000

train_subset = torch.utils.data.random_split(
    cached_trainset, [train_subset_len, len(trainset)-train_subset_len], generator=torch.Generator().manual_seed(42))[0]
test_subset = torch.utils.data.random_split(
    cached_testset, [test_subset_len, len(testset)-test_subset_len], generator=torch.Generator().manual_seed(42))[0]

train_loader = torch.utils.data.DataLoader(dataset=train_subset,
                                           batch_size=BATCH_SIZE,
                                           collate_fn=tonic.collation.PadTensors(),
                                           shuffle=True)

test_loader = torch.utils.data.DataLoader(dataset=test_subset,
                                          batch_size=BATCH_SIZE,
                                          collate_fn=tonic.collation.PadTensors(),
                                          )

train_len = train_subset_len/BATCH_SIZE
test_len = test_subset_len/BATCH_SIZE
lr = 0.002
slope = 25
spike_grad = surrogate.fast_sigmoid(slope=slope)  # slope = 25
# spike_grad = surrogate.LSO(slope = slope) #slope = 0.1
# spike_grad = surrogate.SSO(mean=0.1, variance=0.2) #mean=0.1, variance=0.2
# spike_grad = surrogate.SFS(slope=slope, B=0.9)
# spike_grad = surrogate.spike_rate_escape(beta=0.9, slope=slope)
dt=0.5

params = IzhikevichParameter(a=0.1,
                             b=0.25,
                             c=-55,
                             d=8,
                             v_thres=-55)

# Define Network
class Net(nn.Module):
    def __init__(self, num_filters,
                 kernel_size,
                 num_channels,
                 dense_features,
                 max_pooling,
                 output_features):
        super().__init__()
        self.mp = max_pooling

        # initialize layers
        self.conv1 = nn.Conv2d(num_channels, num_filters[0], kernel_size[0], padding=1)
        self.conv2 = nn.Conv2d(num_filters[0], num_filters[1], kernel_size[1], padding=1)
        self.conv3 = nn.Conv2d(num_filters[1], num_filters[2], kernel_size[2], padding=1)
        self.izhi1 = snn.Izhikevich(p=params, spike_grad=spike_grad, dt=dt)
        self.izhi2 = snn.Izhikevich(p=params, spike_grad=spike_grad, dt=dt)
        self.izhi3 = snn.Izhikevich(p=params, spike_grad=spike_grad, dt=dt)
        self.izhi4 = snn.Izhikevich(p=params, spike_grad=spike_grad, dt=dt)
        self.fc1 = nn.Linear(dense_features, output_features)
        
    def forward(self, x):
        seq_len, batch, _, _, _, = x.shape
        s1 = s2 = s3 = so = IzhikevichState(
            v=torch.tensor(-55.0, requires_grad=True, device=device),
            u=torch.tensor(-55.0, device=device) * params.b)
        voltages = []
        spikes = []
                                  
        for ts in range(seq_len):
            z = x[ts, :, :]
            z = (z > 0).type(torch.float)
            cur1 = self.conv1(z)
            spk1, s1 = self.izhi1(cur1, s1)
            z = F.max_pool2d(spk1, self.mp[0])
            cur2 = self.conv2(z)
            spk2, s2 = self.izhi2(cur2, s2)
            z = F.max_pool2d(spk2, self.mp[1])
            cur3 = self.conv3(z)
            spk3, s3 = self.izhi3(cur3, s3)
            cur4 = self.fc1(spk3.view(batch, -1))
            spk4, so = self.izhi4(cur4, so)
            voltages.append(so[0]) 
            spikes.append(spk4)
        return torch.stack(spikes), torch.stack(voltages)


EPOCHS = 50
IN_FEATURES = 34
N_CHANNELS = 2
N_FILTERS_1 = 8
N_FILTERS_2 = 16
N_FILTERS_3 = 32
KERNEL_SIZE_1 = 3
KERNEL_SIZE_2 = 3
KERNEL_SIZE_3 = 3
MP_KERNEL_SIZE_1 = 2
MP_KERNEL_SIZE_2 = 2
OUTPUT_FEATURES = len(trainset.classes)
cl1 = int(IN_FEATURES + 2 - 1 * (KERNEL_SIZE_1 - 1))
mpl1 = int((cl1-MP_KERNEL_SIZE_1)/MP_KERNEL_SIZE_1) + 1
cl2 = int(mpl1 + 2 - 1 * (KERNEL_SIZE_2 - 1))
mpl2 = int((cl2-MP_KERNEL_SIZE_2)/MP_KERNEL_SIZE_2) + 1
cl3 = int(mpl2 + 2 - 1 * (KERNEL_SIZE_3-1))
DENSE_FEATURES = cl3**2 * N_FILTERS_3
layers = ('34x34x2' + '-' +
          str(N_FILTERS_1) + 'c' + str(KERNEL_SIZE_1) + '-' +
          str(MP_KERNEL_SIZE_1) + 'p-' +
          str(N_FILTERS_2) + 'c' + str(KERNEL_SIZE_2) + '-' +
          str(MP_KERNEL_SIZE_2) + 'p-' +
          str(N_FILTERS_3) + 'c' + str(KERNEL_SIZE_3) + '-' +
          str(DENSE_FEATURES) + '-' + str(OUTPUT_FEATURES))

net = Net(
    num_filters=[N_FILTERS_1, N_FILTERS_2, N_FILTERS_3],
    kernel_size=[KERNEL_SIZE_1, KERNEL_SIZE_2, KERNEL_SIZE_3],
    num_channels=N_CHANNELS,
    max_pooling=[MP_KERNEL_SIZE_1, MP_KERNEL_SIZE_2],
    dense_features=DENSE_FEATURES,
    output_features=OUTPUT_FEATURES,
).to(device)

optimizer = torch.optim.Adam(net.parameters(), lr=lr)
# loss_fn = SF.mse_count_loss(correct_rate=0.75, incorrect_rate=0.2)
# loss_fn = SF.mse_membrane_loss(on_target=0.8, off_target=0.3)
# loss_fn = SF.ce_rate_loss()
loss_fn = SF.ce_max_membrane_loss()
# loss_fn = SF.ce_count_loss()
log_softmax = nn.LogSoftmax(dim=1)

def train(train_loader, device, optimizer,):
    losses = []
    correct = 0
    net.train()
    for data, target in tqdm(
            train_loader, leave=False, total=int(train_len), colour='green'):
        target = target.to(device)
        data = data.float().to(device)
        optimizer.zero_grad()
        spikes, output = net(data)
        log_y_p, _ = torch.max(output, 0)
        log_y_p = log_softmax(log_y_p)
        pred = log_y_p.argmax(dim=1, keepdim=True)
        correct += pred.eq(target.view_as(pred)).sum().item()
        loss = loss_fn(output, target)
        loss.backward()
        optimizer.step()
        losses.append(loss.item())

    accuracy = correct / len(train_loader.dataset)
    mean_loss = np.mean(losses)
    return losses, mean_loss, accuracy


def test(device, test_loader):
    test_loss = 0
    correct = 0
    losses = []
    with torch.no_grad():
        for data, target in tqdm(
                test_loader, total=int(test_len), leave=False, colour='red'):
            target = target.to(device)
            data = data.to(device)
            spikes, output = net(data)
            log_y_p, _ = torch.max(output, 0)
            log_y_p = log_softmax(log_y_p)
            pred = log_y_p.argmax(dim=1, keepdim=True)
            test_loss = loss_fn(output, target).item()
            pred.eq(target.view_as(pred)).sum().item()
            correct += pred.eq(target.view_as(pred)).sum().item()
            losses.append(test_loss)
    test_loss = np.mean(losses)
    accuracy = correct / len(test_loader.dataset)
    return test_loss, accuracy


training_losses = []
mean_losses = []
test_losses = []
train_accuracies = []
test_accuracies = []

torch.autograd.set_detect_anomaly(True)

pbar = tqdm(total=EPOCHS, colour='blue', leave=False)
start_time = time.time()
for epoch in range(EPOCHS):
    training_loss, mean_loss, train_accuracy = train(
        device=device, train_loader=train_loader, optimizer=optimizer)
    test_loss, test_accuracy = test(device=device, test_loader=test_loader)
    training_losses += training_loss
    mean_losses.append(mean_loss)
    test_losses.append(test_loss)
    train_accuracies.append(train_accuracy)
    test_accuracies.append(test_accuracy)
    pbar.set_description(
        f"Acc: [{train_accuracies[-1]:.5f}|{test_accuracies[-1]:.5f}], Loss: [{mean_losses[-1]:.5f}|{test_losses[-1]:.5f}]")
    pbar.update()
print(
    f"Train Accuracy:{train_accuracies[-1]:.2f}, Test Accuracy{test_accuracies[-1]:.2f}")
runtime = time.time() - start_time
seed = torch.seed()
date = datetime.datetime.now()
filetype = '.png'
d = str(date.day) + "_" + str(date.month) + "_" + \
    str(date.year) + "_" + str(date.time())
d = d.split('.')[0]
scriptname = sys.argv[0].split('.')[0]
tf_str = str(transform).replace(" ", "").splitlines(keepends=False)[1:-1]
loss_function = str(loss_fn).split('.')[2]
spike_gradient = str(spike_grad).split('.')[0]
try:
    tf_str[1] = tf_str[1].replace(",decay='exp'", "")
except:
    pass
opt = str(optimizer).replace(
    " ", "").strip().splitlines(keepends=False)[2:-1]
fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.plot(mean_losses)
plt.plot(test_losses)
plt.title("Loss Curves")
plt.legend(["Train Loss", "Test Loss"])
plt.xlabel("Epoch")
plt.ylabel("Loss")
# plt.show()
plt.savefig("./plots/" + scriptname + "/loss" + "_" + d + filetype)

np.save("./plots/" + scriptname + "/loss_data" +
        "_" + d + '.npy', [mean_losses, test_losses])

fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.plot(train_accuracies)
plt.plot(test_accuracies)
plt.title("Accuracy Curves")
plt.legend(["Train Accuracy", "Test Accuracy"])
plt.xlabel("Epoch")
plt.ylabel("Accuracy")
# plt.show()
plt.savefig("./plots/" + scriptname + "/acc" + "_" + d + filetype)

np.save("./plots/" + scriptname + "/acc_data" +
        "_" + d + '.npy', [train_accuracies, test_accuracies])

filename_raw = 'snntorch_nmnist_izhi_results_raw.csv'
filename_pretty = 'snntorch_nmnist_izhi_results_pretty.csv'

fields = ['n',
          ' date',
          ' filename',
          ' device, seed',
          ' runtime',
          ' accuracy (train, test)',
          ' loss (train, test)',
          ' epochs',
          ' batch_size',
          ' opt',
          ' layers',
          ' first saccade only',
          ' loss function',
          ' spike gradient',
          ' v_th',
          ' slope',
          ' a',
          ' b',
          ' c',
          ' d',
          ' bias',
          ' transform']

num_rows = 0
for r in open(filename_raw):
    num_rows += 1
if num_rows != 0:
    num_rows -= 1

rows = [[num_rows,
        d.replace('_', ' '),
        sys.argv[0].split('.')[0] + '.py',
        (device, seed),
        runtime,
        (train_accuracies[-1], test_accuracies[-1]),
        (mean_losses[-1], test_losses[-1]),
        EPOCHS,
        BATCH_SIZE,
        opt,
        layers,
        first_saccade_only,
        loss_function.split(' ')[0],
        spike_gradient.split(' ')[1],
        params.v_thres,
        slope,
        params.a,
        params.b,
        params.c,
        params.d,
        params.bias,
        tf_str]]


with open(filename_raw, 'a') as csvfile:
    csvwriter = csv.writer(csvfile)
    if num_rows == 0:
        csvwriter.writerow(fields)
    csvwriter.writerows(rows)

with open(filename_raw) as fp:
    table = from_csv(fp)

f = open(filename_pretty, "w")
f.write(table.get_string())
f.close()
