a
    l��a':  �                   @   s�   d dl Z d dlmZ d dlmZ d dlZe jZG dd� d�Z	G dd� de	�Z
G dd� de	�ZG d	d
� d
e	�ZG dd� de	�ZG dd� de	�Zddd�ZG dd� d�Zdd� Zdd� ZdS )�    N)�spikegenc                   @   s   e Zd Zdd� Zdd� ZdS )�LossFunctionsc                 C   s,   d}|j rd}|�d�}|�d�}|||fS �N�cpu�cudar   �������is_cuda�size)�self�spk_out�device�	num_steps�num_outputs� r   �Q/home/js/.pyenv/versions/3.9.7/lib/python3.9/site-packages/snntorch/functional.py�_prediction_check   s    

zLossFunctions._prediction_checkc              	   C   s�   |st d��|| r*t d|� d|� d���d}| jr8d}t�t| �d�g|g ���|�}t|�D ]R}| dd�dd�t|| | �t||d  | ��f �	d	��	d
�|dd�|f< qb|S �z1Count up spikes sequentially from output classes.z>``num_classes`` must be specified if ``population_code=True``.z``num_outputs z! must be a factor of num_classes �.r   r   �   Nr   r   �
�	Exceptionr	   �torch�zeros�tupler
   �to�range�int�sum�r   �num_classesr   r   Zpop_code�idxr   r   r   �_population_code   s6    ��"������zLossFunctions._population_codeN)�__name__�
__module__�__qualname__r   r"   r   r   r   r   r      s   
r   c                   @   s    e Zd ZdZdd� Zdd� ZdS )�ce_rate_lossa�  Cross Entropy Spike Rate Loss.
    When called, the spikes at each time step are sequentially passed through the Cross Entropy Loss function.
    This criterion combines log_softmax and NLLLoss in a single function.
    The losses are accumulated over time steps to give the final loss.
    The Cross Entropy Loss encourages the correct class to fire at all time steps, and aims to suppress incorrect classes from firing.

    The Cross Entropy Rate Loss applies the Cross Entropy function at every time step. In contrast, the Cross Entropy Count Loss accumulates spikes first, and applies Cross Entropy Loss only once.


    Example::

        import snntorch.functional as SF

        loss_fn = SF.ce_rate_loss()
        loss = loss_fn(outputs, targets)

    c                 C   s
   d| _ d S )Nr&   �r#   �r   r   r   r   �__init__F   s    zce_rate_loss.__init__c                 C   sd   | � |�\}}}tjdd�}t�� }||�}tjdt|d�}	t|�D ]}
|	|||
 |�7 }	qD|	| S )Nr   �Zdimr   ��dtyper   )r   �nn�
LogSoftmax�NLLLossr   r   r,   r   )r   r   �targetsr   r   �_�log_softmax_fn�loss_fn�log_p_y�loss�stepr   r   r   �__call__I   s    zce_rate_loss.__call__N�r#   r$   r%   �__doc__r)   r7   r   r   r   r   r&   3   s   r&   c                   @   s"   e Zd ZdZddd�Zdd� ZdS )	�ce_count_lossa�  Cross Entropy Spike Count Loss.

    The spikes at each time step [num_steps x batch_size x num_outputs] are accumulated and then passed through the Cross Entropy Loss function.
    This criterion combines log_softmax and NLLLoss in a single function.
    The Cross Entropy Loss encourages the correct class to fire at all time steps, and aims to suppress incorrect classes from firing.

    The Cross Entropy Count Loss accumulates spikes first, and applies Cross Entropy Loss only once.
    In contrast, the Cross Entropy Rate Loss applies the Cross Entropy function at every time step.

    Example::

        import snntorch.functional as SF

        # if not using population codes (i.e., more output neurons than there are classes)
        loss_fn = ce_count_loss()
        loss = loss_fn(spk_out, targets)

        # if using population codes; e.g., 200 output neurons, 10 output classes --> 20 output neurons p/class
        loss_fn = ce_count_loss(population_code=True, num_classes=10)
        loss = loss_fn(spk_out, targets)

    :param population_code: Specify if a population code is applied, i.e., the number of outputs is greater than the number of classes. Defaults to ``False``
    :type population_code: bool, optional

    :param num_classes: Number of output classes must be specified if ``population_code=True``. Must be a factor of the number of output neurons if population code is enabled. Defaults to ``False``
    :type num_classes: int, optional

    Fc                 C   s   || _ || _d| _d S )Nr:   )�population_coder    r#   )r   r;   r    r   r   r   r)   u   s    zce_count_loss.__init__c           
      C   s\   t jdd�}t �� }| jr:| �|�\}}}t|| j|�}nt�|d�}||�}|||�}	|	S �Nr   r*   r   )	r-   r.   r/   r;   r   r"   r    r   r   )
r   r   r0   r2   r3   r1   r   �spike_countr4   r5   r   r   r   r7   z   s    
zce_count_loss.__call__N)FFr8   r   r   r   r   r:   W   s   
r:   c                   @   s    e Zd ZdZdd� Zdd� ZdS )�ce_max_membrane_lossay  Cross Entropy Max Membrane Loss.
    When called, the maximum membrane potential value for each output neuron is sampled and passed through the Cross Entropy Loss Function.
    This criterion combines log_softmax and NLLLoss in a single function.
    The Cross Entropy Loss encourages the maximum membrane potential of the correct class to increase, while suppressing the maximum membrane potential of incorrect classes.
    This function is adopted from SpyTorch by Friedemann Zenke.

    Example::

        import snntorch.functional as SF

        loss_fn = SF.ce_max_membrane_loss()
        loss = loss_fn(outputs, targets)

    c                 C   s
   d| _ d S )Nr>   r'   r(   r   r   r   r)   �   s    zce_max_membrane_loss.__init__c           	      C   s:   t jdd�}t �� }t�|d�\}}||�}|||�}|S r<   )r-   r.   r/   r   �max)	r   �mem_outr0   r2   r3   Zmax_mem_outr1   r4   r5   r   r   r   r7   �   s    
zce_max_membrane_loss.__call__Nr8   r   r   r   r   r>   �   s   r>   c                   @   s"   e Zd ZdZd
dd�Zdd� Zd	S )�mse_count_lossa  Mean Square Error Spike Count Loss.
    When called, the total spike count is accumulated over time for each neuron.
    The target spike count for correct classes is set to (num_steps * correct_rate), and for incorrect classes (num_steps * incorrect_rate).
    The spike counts and target spike counts are then applied to a Mean Square Error Loss Function.
    This function is adopted from SLAYER by Sumit Bam Shrestha and Garrick Orchard.

    Example::

        import snntorch.functional as SF

        loss_fn = SF.mse_count_loss(correct_rate=0.75, incorrect_rate=0.25)
        loss = loss_fn(outputs, targets)


    :param correct_rate: Firing frequency of correct class as a ratio, e.g., ``1`` promotes firing at every step; ``0.5`` promotes firing at 50% of steps, ``0`` discourages any firing, defaults to ``1``
    :type correct_rate: float, optional

    :param incorrect_rate: Firing frequency of incorrect class(es) as a ratio, e.g., ``1`` promotes firing at every step; ``0.5`` promotes firing at 50% of steps, ``0`` discourages any firing, defaults to ``1``
    :type incorrect_rate: float, optional

    :param population_code: Specify if a population code is applied, i.e., the number of outputs is greater than the number of classes. Defaults to ``False``
    :type population_code: bool, optional

    :param num_classes: Number of output classes must be specified if ``population_code=True``. Must be a factor of the number of output neurons if population code is enabled. Defaults to ``False``
    :type num_classes: int, optional


    r   r   Fc                 C   s"   || _ || _|| _|| _d| _d S )NrA   )�correct_rate�incorrect_rater;   r    r#   )r   rB   rC   r;   r    r   r   r   r)   �   s
    zmse_count_loss.__init__c                 C   s�   | � |�\}}}t�� }| jsZt|| j �}t|| j �}tj||||d�}	t	�
|d�}
nRt|| j || j  �}t|| j || j  �}tj|| j||d�}	t|| j|�}
||
|	�}|| S )N�r    �	on_target�
off_targetr   )r   r-   �MSELossr;   r   rB   rC   r   �targets_convertr   r   r    r"   )r   r   r0   r1   r   r   r3   rE   rF   Zspike_count_targetr=   r5   r   r   r   r7   �   s6    ����
zmse_count_loss.__call__N)r   r   FFr8   r   r   r   r   rA   �   s    �
	rA   c                   @   s"   e Zd ZdZd
dd�Zdd� Zd	S )�mse_membrane_lossaA  Mean Square Error Membrane Loss.
    When called, pass the output membrane of shape [num_steps x batch_size x num_outputs] and the target tensor of membrane potential.
    The membrane potential and target are then applied to a Mean Square Error Loss Function.
    This function is adopted from Spike-Op by Jason K. Eshraghian.

    Example::

        import snntorch.functional as SF

        # if targets are the same at each time-step
        loss_fn = mse_membrane_loss(time_var_targets=False)
        loss = loss_fn(outputs, targets)

        # if targets are time-varying
        loss_fn = mse_membrane_loss(time_var_targets=True)
        loss = loss_fn(outputs, targets)

    :param time_var_targets: Specifies whether the targets are time-varying, defaults to ``False``
    :type correct_rate: bool, optional

    :param on_target: Specify target membrane potential for correct class, defaults to ``1``
    :type on_target: float, optional

    :param off_target: Specify target membrane potential for incorrect class, defaults to ``0``
    :type off_target: float, optional


    Fr   r   c                 C   s   || _ || _|| _d| _d S )NrI   )�time_var_targetsrE   rF   r#   )r   rJ   rE   rF   r   r   r   r)     s    zmse_membrane_loss.__init__c           	      C   s�   | � |�\}}}tj||| j| jd�}tjdt|d�}t�	� }| j
rjt|�D ]}|||| || �7 }qLn t|�D ]}|||| |�7 }qr|| S )NrD   r   r+   )r   r   rH   rE   rF   r   r   r,   r-   rG   rJ   r   )	r   r@   r0   r   r   r   r5   r3   r6   r   r   r   r7     s    �zmse_membrane_loss.__call__N)Fr   r   r8   r   r   r   r   rI   �   s   
rI   Fc                 C   s^   |r*t | �\}}}t| ||��d�\}}n| jdd��d�\}}t�||k�� �� �� �}|S )aA  Use spike count to measure accuracy.

    :param spk_out: Output spikes of shape [num_steps x batch_size x num_outputs]
    :type spk_out: torch.Tensor

    :param targets: Target tensor (without one-hot-encoding) of shape [batch_size]
    :type targets: torch.Tensor

    :return: accuracy
    :rtype: numpy.float64
    r   r   r*   )	r   r"   r?   r   �np�mean�detachr   �numpy)r   r0   r;   r    r1   r   r!   Zaccuracyr   r   r   �accuracy_rateM  s    rO   c                   @   s"   e Zd ZdZddd�Zdd� ZdS )	�l1_rate_sparsityzpL1 regularization using total spike count as the penalty term.
    Lambda is a scalar factor for regularization.��h㈵��>c                 C   s   || _ d| _d S )NrP   )�Lambdar#   )r   rR   r   r   r   r)   h  s    zl1_rate_sparsity.__init__c                 C   s   | j t�|� S )N)rR   r   r   )r   r   r   r   r   r7   l  s    zl1_rate_sparsity.__call__N)rQ   r8   r   r   r   r   rP   d  s   
rP   c                 C   s,   d}| j rd}| �d�}| �d�}|||fS r   r   )r   r   r   r   r   r   r   r   u  s    

r   c              	   C   s�   |st d��|| r*t d|� d|� d���d}| jr8d}t�t| �d�g|g ���|�}t|�D ]R}| dd�dd�t|| | �t||d  | ��f �	d	��	d
�|dd�|f< qb|S r   r   r   r   r   r   r"   �  s6    ��"������r"   )FF)r   Ztorch.nnr-   Zsnntorchr   rN   rK   �floatr,   r   r&   r:   r>   rA   rI   rO   rP   r   r"   r   r   r   r   �<module>   s   ($3LX
