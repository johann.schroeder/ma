import tonic
from torch.utils.data import Dataset, DataLoader
import torch
import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime
from slayerPytorch.src.learningStats import learningStats
from prettytable.prettytable import from_csv
import gc
import time
import csv
import slayerSNN as snn
import sys
import os
import torchvision as tv
from tonic import CachedDataset
CURRENT_TEST_DIR = os.getcwd()
sys.path.append(CURRENT_TEST_DIR + "/../../src")
gc.collect()
torch.cuda.empty_cache()
random_transforms = tonic.transforms.Compose(
    [
        torch.tensor,
        tv.transforms.RandomAffine(degrees=(0, 0), scale=(0.9, 1.0)),
    ]
)
transform = tonic.transforms.Compose(
    [
        tonic.transforms.Downsample(time_factor=0.001),
        tonic.transforms.ToFrame(sensor_size=tonic.datasets.SHD.sensor_size,
                                #  time_window=5,
                                n_event_bins=50,
                                # n_time_bins=50,
                                # event_count=200
                                 )
    ]
)

trainset = tonic.datasets.SHD(save_to='../data',
                              transform=transform,
                              train=True)

testset = tonic.datasets.SHD(save_to='../data',
                             transform=transform,
                             train=False)

#Frames
cached_trainset = CachedDataset(
    trainset,
    transform=random_transforms, 
    cache_path='../data/cached_datasets/shd_TF_ne50_train')
cached_testset = CachedDataset(
    testset, cache_path='../data/cached_datasets/shd_TF_ne50_test')

# train samples: 8156, test samples: 2264
train_subset_len = 8156
test_subset_len = 2264

train_subset = torch.utils.data.random_split(
    cached_trainset, [train_subset_len, len(trainset)-train_subset_len])[0]
test_subset = torch.utils.data.random_split(
    cached_testset, [test_subset_len, len(testset)-test_subset_len])[0]

BATCH_SIZE = 64
EPOCHS = 100
lr = 0.003
input_size = tonic.datasets.SHD.sensor_size
hidden_size = 100
n_classes = 20
layers = [hidden_size]
netParams = snn.params('network_shd.yaml')
# Network definition
class Network(torch.nn.Module):
    def __init__(self, netParams):
        super(Network, self).__init__()
        slayer = snn.layer(netParams['neuron'], netParams['simulation'])
        self.slayer = slayer
        self.fc1   = slayer.dense(input_size, hidden_size)
        self.fc2   = slayer.dense(hidden_size, n_classes)

    def forward(self, spikeInput):
        spikeInput = (spikeInput > 0).type(torch.float)
        cur1 = self.slayer.psp(self.fc1(spikeInput.unsqueeze(dim=1)))
        spikeLayer1 = self.slayer.spike(cur1)
        cur2 = self.slayer.psp(self.fc2(spikeLayer1))
        spikeLayer2 = self.slayer.spike(cur2)     
        
        return spikeLayer2
 
if __name__ == '__main__':      
    # Define the cuda device to run the code on.
    device = torch.device('cuda')

    # Create network instance.
    net = Network(netParams).to(device)

    # Create snn loss instance.
    error = snn.loss(netParams).to(device)

    # Define optimizer module.
    optimizer = torch.optim.Adam(net.parameters(), lr = lr, weight_decay=0.05)

    # Dataset and dataLoader instances.
    train_loader = torch.utils.data.DataLoader(dataset=train_subset,
                                               batch_size=BATCH_SIZE,
                                               collate_fn=tonic.collation.PadTensors(),
                                               shuffle=True)

    test_loader = torch.utils.data.DataLoader(test_subset,
                                          batch_size=BATCH_SIZE,
                                          collate_fn=tonic.collation.PadTensors())

    # Learning stats instance.
    stats = learningStats()
    start_time = time.time()
    
    # Main loop
    for epoch in range(EPOCHS):
        tSt = datetime.now()
        for i, (events, label) in enumerate(iter(train_loader), 0):
            target = torch.zeros((len(label), len(trainset.classes), 1, 1, 1))
            # events = drop_event(events)
            input = events.permute([1, 2, 3, 0])
            for idx, l in enumerate(label):
                target[idx, l.item(), ...] = 1

            # Move the input and target to correct GPU.
            input = input.to(device)
            target = target.to(device)

            # Forward pass of the network.
            output = net.forward(input)

            # Gather the training stats.
            stats.training.correctSamples += torch.sum(
                snn.predict.getClass(output) == label).data.item()
            stats.training.numSamples += len(label)
            
            # Calculate loss.
            loss = error.numSpikes(output, target)

            # Reset gradients to zero.
            optimizer.zero_grad()

            # Backward pass of the network.
            loss.backward()

            # Update weights.
            optimizer.step()

            # Gather training loss stats.
            stats.training.lossSum += loss.cpu().data.item()

            # Display training stats.
            stats.print(epoch, i, (datetime.now() - tSt).total_seconds())

        # Testing loop.
        # Same steps as Training loops except loss backpropagation and weight update.
        for i, (events, label) in enumerate(iter(test_loader), 0):
            target = torch.zeros((len(label), len(trainset.classes), 1, 1, 1))
            input = events.permute([1, 2, 3, 0])
            for idx, l in enumerate(label):
                target[idx, l.item(), ...] = 1
            input  = input.to(device)
            target = target.to(device) 
            # print(input.shape, target.shape, label.shape)
            output = net.forward(input)
            stats.testing.correctSamples += torch.sum( 
                snn.predict.getClass(output) == label ).data.item()
            stats.testing.numSamples     += len(label)

            loss = error.numSpikes(output, target)
            stats.testing.lossSum += loss.cpu().data.item()
            stats.print(epoch, i)
        
        # Update stats.
        stats.update()

    print(net)
    runtime = time.time() - start_time
    date = datetime.now()
    filetype = '.png'
    d = str(date.day) + "_" + str(date.month) + "_" + \
        str(date.year) + "_" + str(date.time())
    d = d.split('.')[0]
    scriptname = sys.argv[0].split('.')[0]
    tf_str = str(transform).replace(" ", "").splitlines(keepends=False)[1:-1]
    opt = str(optimizer).replace(
        " ", "").strip().splitlines(keepends=False)[2:-1]
    
    # Plot the results.
    # Learning loss
    fig = plt.figure(facecolor="w", figsize=(10, 5))
    plt.semilogy(stats.training.lossLog, label='Training')
    plt.semilogy(stats.testing.lossLog, label='Testing')
    plt.title('Loss Curves')
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.legend()

    plt.savefig("./shd/plots/" + scriptname +
                "/loss" + "_" + d + filetype)

    np.save("./shd/plots/" + scriptname + "/loss_data" +
            "_" + d + '.npy', [stats.training.lossLog, stats.testing.lossLog])

    # Learning accuracy
    fig = plt.figure(facecolor="w", figsize=(10, 5))
    plt.plot(stats.training.accuracyLog, label='Training')
    plt.plot(stats.testing.accuracyLog, label='Testing')
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.legend()

    plt.savefig("./shd/plots/" + scriptname +
                "/acc" + "_" + d + filetype)

    np.save("./shd/plots/" + scriptname + "/acc_data" +
            "_" + d + '.npy', [stats.training.accuracyLog, stats.testing.accuracyLog])

    filename_raw = 'slayer_shd_results_raw.csv'
    filename_pretty = 'slayer_shd_results_pretty.csv'

    fields = ['n',
              ' date',
              ' filename',
              ' device',
              ' runtime',
              ' accuracy (train, test)',
              ' loss (train, test)',
              ' epochs',
              ' (batch_size samples (train, test))',
              ' optimizer',
              ' layers',
              ' tSample',
              ' type',
              ' theta',
              ' tauSr',
              ' tauRef',
              ' scaleRef',
              ' tauRho',
              ' scaleRho',
              ' tgtSpikeRegion',
              ' tgtSpikeCount',
              ' transform']

    num_rows = 0
    for r in open(filename_raw):
        num_rows += 1
    if num_rows != 0:
        num_rows -= 1

    rows = [[num_rows,
            d.replace('_', ' '),
            sys.argv[0].split('.')[0] + '.py',
            device,
            runtime,
            (stats.training.accuracyLog[-1], stats.testing.accuracyLog[-1]),
            (stats.training.lossLog[-1], stats.testing.lossLog[-1]),
            EPOCHS,
            (BATCH_SIZE, (train_subset_len, test_subset_len)),
            opt,
            layers,
            netParams['simulation']['tSample'],
            netParams['neuron']['type'],
            netParams['neuron']['theta'],
            netParams['neuron']['tauSr'],
            netParams['neuron']['tauRef'],
            netParams['neuron']['scaleRef'],
            netParams['neuron']['tauRho'],
            netParams['neuron']['scaleRho'],
            netParams['training']['error']['tgtSpikeRegion'],
            netParams['training']['error']['tgtSpikeCount'],
            tf_str]]

    with open(filename_raw, 'a') as csvfile:
        csvwriter = csv.writer(csvfile)
        if num_rows == 0:
            csvwriter.writerow(fields)
        csvwriter.writerows(rows)

    with open(filename_raw) as fp:
        table = from_csv(fp)

    f = open(filename_pretty, "w")
    f.write(table.get_string())
    f.close()
