import tonic
import tonic.transforms as transforms
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.animation import FuncAnimation

dataset = tonic.datasets.NMNIST(save_to='./data', train=False)
sensor_size = tonic.datasets.NMNIST.sensor_size
frame_transform = transforms.ToFrame(sensor_size=sensor_size, n_time_bins=3)
events, target = dataset[3]
frames = frame_transform(events)


def plot_frames(frames):
    fig, axes = plt.subplots(1, len(frames))
    for axis, frame in zip(axes, frames):
        axis.imshow(frame[1]-frame[0])
        axis.axis("off")
    plt.tight_layout()


plot_frames(frames)
plt.show()


