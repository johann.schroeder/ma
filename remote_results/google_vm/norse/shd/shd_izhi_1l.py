import csv
import datetime
import sys
import time
from prettytable.prettytable import from_csv
from tqdm import tqdm
from norse.torch.module.izhikevich import IzhikevichCell
from norse.torch.functional.izhikevich import IzhikevichSpikingBehavior, IzhikevichState, IzhikevichParameters
import torch
import numpy as np
import matplotlib.pyplot as plt
import tonic
import gc
from tonic import CachedDataset
plt.style.use('science')
gc.collect()
torch.cuda.empty_cache()

transform = tonic.transforms.Compose(
    [
        tonic.transforms.Downsample(time_factor=0.001),
        tonic.transforms.ToFrame(sensor_size=tonic.datasets.SHD.sensor_size,
                                 time_window=5,
                                 ),
        # tonic.transforms.DropEvent(p=0.1),
        # tonic.transforms.Denoise(filter_time=10),
        # tonic.transforms.ToTimesurface(sensor_size=tonic.datasets.SHD.sensor_size,
        #    tau=5000,
        #    decay='exp'
        #    )
    ]
)

trainset = tonic.datasets.SHD(save_to='../../data',
                              transform=transform,
                              train=True)

testset = tonic.datasets.SHD(save_to='../../data',
                             transform=transform,
                             train=False)

#Frames
# cached_trainset = CachedDataset(
#     trainset, cache_path='../../data/cached_datasets/shd_TF_tw1_train')
# cached_testset = CachedDataset(
#     testset, cache_path='../../data/cached_datasets/shd_TF_tw1_test')

#Frames 5ms time window
cached_trainset = CachedDataset(
    trainset, cache_path='../../data/cached_datasets/shd_TF_tw5_train')
cached_testset = CachedDataset(
    testset, cache_path='../../data/cached_datasets/shd_TF_tw5_test')

# train samples: 8156, test samples: 2264
train_subset_len = 8156
test_subset_len = 2264

train_subset = torch.utils.data.random_split(
    cached_trainset, [train_subset_len, len(trainset)-train_subset_len])[0]
test_subset = torch.utils.data.random_split(
    cached_testset, [test_subset_len, len(testset)-test_subset_len])[0]

BATCH_SIZE = 64
EPOCHS = 50
LR = 0.002
INPUT_FEATURES = np.product(tonic.datasets.SHD.sensor_size)
HIDDEN_FEATURES = 100
OUTPUT_FEATURES = len(trainset.classes)
DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")
layers = (HIDDEN_FEATURES)
print(DEVICE.type, DEVICE.index)
train_loader = torch.utils.data.DataLoader(dataset=train_subset,
                                           batch_size=BATCH_SIZE,
                                           collate_fn=tonic.collation.PadTensors(),
                                           shuffle=True)

test_loader = torch.utils.data.DataLoader(test_subset,
                                          batch_size=BATCH_SIZE,
                                          collate_fn=tonic.collation.PadTensors(),
                                          shuffle=True)

train_len = train_subset_len/BATCH_SIZE
test_len = test_subset_len/BATCH_SIZE

p_izhi = IzhikevichParameters(a=0.1,
                              b=0.25,
                              c=-65,
                              d=2,
                              mn=5,
                              sq=0.049,
                              bias=141,
                              v_th=30,
                              tau_inv=500,
                              method='super',
                              alpha=100)

spiking_behavior = IzhikevichSpikingBehavior(
    p=p_izhi,
    s=IzhikevichState(
        v=torch.tensor(-65.0, requires_grad=True),
        u=torch.tensor(-65) * p_izhi.b
    )
)

class SNN(torch.nn.Module):
    def __init__(self, input_features,
                 hidden_features,
                 output_features,
                 dt=1e-3):
        super(SNN, self).__init__()
        self.fc_in = torch.nn.Linear(input_features, hidden_features)
        self.fc_out = torch.nn.Linear(hidden_features, output_features)
        self.izhi1 = IzhikevichCell(spiking_method=spiking_behavior, dt=dt)
        self.izhi_out = IzhikevichCell(spiking_method=spiking_behavior, dt=dt)

    def forward(self, x):
        seq_length, batch, _, _ = x.shape
        x = x.reshape(seq_length, batch, -1)
        s1 = so = None
        voltages = []
        for ts in range(seq_length):
            z = x[ts, :, :]
            z = self.fc_in(z)  # Input -> Hidden
            z, s1 = self.izhi1(z, s1)  # IZHIKEVICH
            z = self.fc_out(z)  # Hidden -> Output
            vo, so = self.izhi_out(z, so)  # Izhikevich Output
            voltages += [so[0]]  # Output
        return torch.stack(voltages)


def decode(x):
    x, _ = torch.max(x, 0)
    log_p_y = torch.nn.functional.log_softmax(x, dim=1)
    return log_p_y


class Model(torch.nn.Module):
    def __init__(self, snn, decoder):
        super(Model, self).__init__()
        self.snn = snn
        self.decoder = decoder

    def forward(self, x):
        x = self.snn(x)
        log_p_y = self.decoder(x)
        return log_p_y

model = Model(
    snn=SNN(
        input_features=INPUT_FEATURES,
        hidden_features=HIDDEN_FEATURES,
        output_features=OUTPUT_FEATURES),
    decoder=decode
).to(DEVICE)

optimizer = torch.optim.Adam(model.parameters(), lr=LR)


def train(model, device, train_loader, optimizer):
    model.train()
    losses = []
    correct = 0
    for data, target in tqdm(train_loader, leave=False, colour='green', total=int(train_len)):
        data, target = data.to(device), torch.LongTensor(target).to(device)
        optimizer.zero_grad()
        output = model(data)
        pred = output.argmax(dim=1, keepdim=True)
        correct += pred.eq(target.view_as(pred)).sum().item()
        loss = torch.nn.functional.nll_loss(output, target)
        loss.backward()
        optimizer.step()
        losses.append(loss.item())
    accuracy = correct / len(train_loader.dataset)
    mean_loss = np.mean(losses)
    return losses, mean_loss, accuracy


def test(model, device, test_loader):
    model.eval()
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in tqdm(
            test_loader, colour='red', leave=False, total=int(test_len)):
            data, target = data.to(device), torch.LongTensor(target).to(device)
            output = model(data)
            test_loss += torch.nn.functional.nll_loss(
                output, target, reduction="sum").item()  # sum up batch loss
            pred = output.argmax(
                dim=1, keepdim=True
            )  # get the index of the max log-probability
            correct += pred.eq(target.view_as(pred)).sum().item()
    test_loss /= len(test_loader.dataset)
    accuracy = correct / len(test_loader.dataset)
    return test_loss, accuracy


training_losses = []
mean_losses = []
test_losses = []
train_accuracies = []
test_accuracies = []

torch.autograd.set_detect_anomaly(True)

pbar = tqdm(total=EPOCHS, colour='blue', leave=False)
start_time = time.time()
for epoch in range(EPOCHS):
    training_loss, mean_loss, train_accuracy = train(
        model, DEVICE, train_loader, optimizer)
    test_loss, test_accuracy = test(model, DEVICE, test_loader)
    training_losses += training_loss
    mean_losses.append(mean_loss)
    test_losses.append(test_loss)
    train_accuracies.append(train_accuracy)
    test_accuracies.append(test_accuracy)
    pbar.set_description(
        f"Acc: [{train_accuracies[-1]:.2f}|{test_accuracies[-1]:.2f}], Loss: [{mean_losses[-1]:.2f}|{test_losses[-1]:.2f}]")
    pbar.update()


runtime = time.time() - start_time

date = datetime.datetime.now()
filetype = '.png'
d = str(date.day) + "_" + str(date.month) + "_" + \
    str(date.year) + "_" + str(date.time())
d = d.split('.')[0]
scriptname = sys.argv[0].split('.')[0]
tf_str = str(transform).replace(" ", "").splitlines(keepends=False)[1:-1]
try:
    tf_str[1] = tf_str[1].replace(",decay='exp'", "")
except:
    pass
opt = str(optimizer).replace(
    " ", "").strip().splitlines(keepends=False)[2:-1]

print(
    f"Train Accuracy:{train_accuracies[-1]:.2f}, Test Accuracy{test_accuracies[-1]:.2f}")
fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.plot(mean_losses)
plt.plot(test_losses)
plt.title("Loss Curves")
plt.legend(["Train Loss", "Test Loss"])
plt.xlabel("Epoch")
plt.ylabel("Loss")
# plt.show()
plt.savefig("./plots/" + scriptname + "/loss" + "_" + d + filetype)

np.save("./plots/" + scriptname + "/loss_data" +
        "_" + d + '.npy', [mean_losses, test_losses])

fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.plot(train_accuracies)
plt.plot(test_accuracies)
plt.title("Accuracy Curves")
plt.legend(["Train Accuracy", "Test Accuracy"])
plt.xlabel("Epoch")
plt.ylabel("Accuracy")
# plt.show()
plt.savefig("./plots/" + scriptname + "/acc" + "_" + d + filetype)

np.save("./plots/" + scriptname + "/acc_data" +
        "_" + d + '.npy', [train_accuracies, test_accuracies])

filename_raw = 'norse_shd_izhi_results_raw.csv'
filename_pretty = 'norse_shd_izhi_results_pretty.csv'

fields = ['n',
          ' date',
          ' filename',
          ' device',
          ' runtime',
          ' accuracy (train, test)',
          ' loss (train, test)',
          ' epochs',
          ' batch_size',
          ' opt',
          ' layers',
          ' method',
          ' alpha',
          ' a',
          ' b',
          ' c',
          ' d',
          ' bias',
          ' tau_inv',
          ' v_th',
          ' transform']

num_rows = 0
for r in open(filename_raw):
    num_rows += 1
if num_rows != 0:
    num_rows -= 1

rows = [[num_rows,
        d.replace('_', ' '),
        sys.argv[0].split('.')[0] + '.py',
        DEVICE,
        runtime,
        (train_accuracies[-1], test_accuracies[-1]),
        (mean_losses[-1], test_losses[-1]),
        EPOCHS,
        BATCH_SIZE,
        opt,
        layers,
        p_izhi.method,
        p_izhi.alpha,
        p_izhi.a,
        p_izhi.b,
        p_izhi.c,
        p_izhi.d,
        p_izhi.bias,
        p_izhi.tau_inv,
        p_izhi.v_th,
        tf_str]]


with open(filename_raw, 'a') as csvfile:
    csvwriter = csv.writer(csvfile)
    if num_rows == 0:
        csvwriter.writerow(fields)
    csvwriter.writerows(rows)

with open(filename_raw) as fp:
    table = from_csv(fp)

f = open(filename_pretty, "w")
f.write(table.get_string())
f.close()
