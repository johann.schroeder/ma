import csv
import datetime
import gc
import sys
from bindsnet.analysis.plotting import (plot_assignments, plot_conv2d_weights,
                                        plot_performance,
                                        plot_spikes,
                                        plot_voltages,
                                        plot_weights,
                                        plot_input)
from bindsnet.evaluation import (all_activity,
                                 assign_labels,
                                 proportion_weighting)
from bindsnet.utils import get_square_assignments, get_square_weights
from custom_models import (ConvNetwork_LIF, TwoLayerConvNetwork_Izhi, 
                           TwoLayerConvNetwork_LIF)
from numpy.lib.utils import source
import tonic
import torch
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
from bindsnet.network.monitors import Monitor
import torchvision as tv
import torchvision.transforms.functional as F
import time
from prettytable.prettytable import from_csv
plt.style.use('science')
gc.collect()
torch.cuda.empty_cache()

surface_dim = 35

transform = tonic.transforms.Compose(
    [
        # tonic.transforms.Downsample(time_factor=0.001),  
        tonic.transforms.Denoise(filter_time=1),
        # tonic.transforms.ToFrame(sensor_size=tonic.datasets.POKERDVS.sensor_size,
                                # event_count=25
                                #  time_window=1
                                # )
        tonic.transforms.ToTimesurface(sensor_size=tonic.datasets.POKERDVS.sensor_size,
                                        tau=5000,
                                        )
    ]
)

# transform = tonic.transforms.Compose(
#     [
#         # tonic.transforms.Downsample(time_factor=0.001),
#         # tonic.transforms.Denoise(filter_time=100),
#         tonic.transforms.ToVoxelGrid(sensor_size=tonic.datasets.POKERDVS.sensor_size,
#                                      n_time_bins=25
#                                      )
#     ]
# )

epochs = 1
BATCH_SIZE = 8
train_subset_len = 48
test_subset_len = 20

trainset = tonic.datasets.POKERDVS(save_to='../data',
                                 transform=transform,
                                 train=True)

testset = tonic.datasets.POKERDVS(save_to='../data',
                                transform=transform,
                                train=False)

train_subset = torch.utils.data.random_split(
    trainset, [train_subset_len, len(trainset)-train_subset_len])[0]
test_subset = torch.utils.data.random_split(
    testset, [test_subset_len, len(testset)-test_subset_len])[0]

train_len = -(-train_subset_len//BATCH_SIZE)
test_len = -(-test_subset_len//BATCH_SIZE)

#Dataloader
train_loader = torch.utils.data.DataLoader(
    dataset=train_subset,
    batch_size=BATCH_SIZE,
    collate_fn=tonic.collation.PadTensors(),
    shuffle=True,
)

test_loader = torch.utils.data.DataLoader(
    dataset=test_subset,
    batch_size=BATCH_SIZE,
    collate_fn=tonic.collation.PadTensors(),
    shuffle=True
)


dt = 1
t = None
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
sensor_size = tonic.datasets.POKERDVS.sensor_size
# input_size = np.product(tonic.datasets.POKERDVS.sensor_size)
# input_size = (35*35)
input_size = (surface_dim * surface_dim * 2)
n_classes = len(trainset.classes)
exc = 22.5
inh = 120
nu = [1e-4, 1e-2]
wmin = 0.0
wmax = 1.0
threshold = -52.0 # LIF
# threshold = 40.0  # Izhi
plot = True

kernel_size_1 = 8
kernel_size_2 = 3
kernel_size_3 = 3
padding_1 = 0
padding_2 = 1
padding_3 = 1
stride_1 = 4
stride_2 = 2
stride_3 = 2
# max_pool_1 = 2
# max_pool_2 = 2
# pooling_size_1 = int((conv_size_1 - max_pool_2) / 2) + 2
# pooling_size_2 = int((conv_size_2 - max_pool_2) / 2) + 1
conv_size_1 = int((surface_dim - kernel_size_1 + 2 * padding_1) / stride_1) + 1
conv_size_2 = int((conv_size_1 - kernel_size_2 + 2 * padding_2) / stride_2) + 1
conv_size_3 = int((conv_size_2 - kernel_size_3 + 2 * padding_3) / stride_3) + 1
n_filters_1 = 25
n_filters_2 = 16
n_filters_3 = 32
norm_1 = 0.5 * kernel_size_1 ** 2
norm_2 = 0.5 * kernel_size_2 ** 2
norm_3 = 0.5 * kernel_size_3 ** 2
norm_4 = 0.5 * input_size
n_neurons_output = conv_size_1 ** 2 * n_filters_1  # ConvNet
n_neurons_hidden = conv_size_3 ** 2 * n_filters_3  # TwoLayerConvNet
layers = str(input_size) + '-' + str(n_filters_1) + 'c' + str(conv_size_1)  
# n_neurons_output = n_classes**2

# network = TwoLayerConvNetwork_LIF(
#     n_inpt=input_size,
#     # shape=(1, 35, 35),
#     shape=(2, 35, 35),
#     kernel_size_1=kernel_size_1,
#     kernel_size_2=kernel_size_2,
#     kernel_size_3=kernel_size_3,
#     stride_1=stride_1,
#     stride_2=stride_2,
#     stride_3=stride_3,
#     conv_size_1=conv_size_1,
#     conv_size_2=conv_size_2,
#     conv_size_3=conv_size_3,
#     n_filters_1=n_filters_1,
#     n_filters_2=n_filters_2,
#     n_filters_3=n_filters_3,
#     padding_1=padding_1,
#     padding_2=padding_2,
#     padding_3=padding_3,
#     nu=nu,
#     norm_1=norm_1,
#     norm_2=norm_2,
#     norm_3=norm_3,
#     norm_4=norm_4,
#     n_neurons_hidden=n_neurons_hidden,
#     n_neurons_output=n_neurons_output,
#     batch_size=BATCH_SIZE
# )

network = TwoLayerConvNetwork_Izhi(
    n_inpt=input_size,
    # shape=(2, 35, 35),
    shape=(2, surface_dim, surface_dim),
    kernel_size=kernel_size_1,
    conv_size=conv_size_1,
    n_filters=n_filters_1,
    norm=norm_1,
    stride=stride_1,
    padding=padding_1,
    nu=nu,
    wmax=wmax,
    wmin=wmin,
    batch_size=BATCH_SIZE,
    n_neurons=n_neurons_output
)




spikes = {}


source_monitor = Monitor(
    obj=network.layers["X"],
    state_vars=("s"),
    time=t,
    device=device
)
network.add_monitor(source_monitor, name="X")

# hidden_monitor = Monitor(
#     obj=network.layers["C1"],
#     state_vars=("s"),
#     time=t,
#     device=device
# )
# network.add_monitor(hidden_monitor, name="C2")

output_monitor = Monitor(
    obj=network.layers["Y"],
    state_vars=("s"),
    time=t,
    device=device
)
network.add_monitor(output_monitor, name="Y")

network.to(device)

assignments = -torch.ones(n_neurons_output, device=device)
proportions = torch.zeros((n_neurons_output, n_classes), device=device)
rates = torch.zeros((n_neurons_output, n_classes), device=device)
per_class = int((n_filters_3 * conv_size_3 * conv_size_3) / n_classes)


acc_batch_train = []
acc_batch_test = []
acc_train = []
acc_test = []
n_sqrt = int(np.ceil(np.sqrt(n_neurons_output)))
weights_im = None
assigns_im = None
perf_ax = None
inpt_axes = None
inpt_ims = None
acc = {"train": [], "test": []}
spike_ims, spike_axes = None, None
voltage_axes, voltage_ims = None, None
# spike_record = torch.zeros(
#     (BATCH_SIZE, time, n_neurons), device=device)

start_time = time.time()
pbar = tqdm(total=epochs, colour='blue')
for epoch in range(epochs):
    #Training
    network.train(mode=True)
    correct_train = 0
    correct_test = 0
    for idx, (events, targets) in enumerate(
            tqdm(train_loader, colour='green', leave=False, total=train_len)):
        t = int(events.shape[0])
        b = int(events.shape[1])
        spike_record = torch.zeros((b, t, n_neurons_output), device=device)
        input = {"X": events.to(device)}
        # input = {"X": events.unsqueeze(dim=2).to(device)}
        network.run(inputs=input, time=t)
        
        spikes = {
            "X": source_monitor.get("s"),
            "Y": output_monitor.get("s"),
        }
        spike_record = spikes["Y"].view(t, b, -1).permute((1, 0, 2))

        all_activity_pred = all_activity(
            spikes=spike_record.to(device),
            assignments=assignments,
            n_labels=n_classes)
        
        proportion_pred = proportion_weighting(
            spikes=spike_record.to(device),
            assignments=assignments,
            proportions=proportions,
            n_labels=n_classes)

        correct_train += torch.sum(targets.long().to(device) ==
                                   all_activity_pred).item()

        assignments, proportions, rates = assign_labels(
            spikes=spike_record.to(device),
            labels=targets.to(device),
            n_labels=n_classes,
            rates=rates)

        
        
        if plot:
            # image = events[0, 0, 0, :].view((34, 34))
            # inpt = input["X"].view(time, input_size).sum(
            #     0).view(sensor_size)
            input_exc_weights = network.connections[("X", "Y")].w
            # square_weights = get_square_weights(
                # weights=input_exc_weights, n_sqrt=n_sqrt, side=(16, 32))
            square_assignments = get_square_assignments(
                assignments=assignments, n_sqrt=n_sqrt)

            # Plots
            # inpt_axes, inpt_ims = plot_input(
            # batch,
            # inpt,
            # label=labels[idx],
            # axes=inpt_axes,
            # ims=inpt_ims)
            # weights_im = plot_weights(input_exc_weights, im=weights_im)
            weights_im = plot_conv2d_weights(input_exc_weights, im=weights_im)            
            assigns_im = plot_assignments(
                square_assignments, im=assigns_im, classes=trainset.classes)
            # voltage_ims, voltage_axes = plot_voltages(
            #     voltages, ims=voltage_ims, axes=voltage_axes, plot_type="line"
            # )
            spike_ims, spike_axes = plot_spikes(
                spikes, ims=spike_ims, axes=spike_axes)
            # perf_ax = plot_performance(acc_print, x_scale=time, ax=perf_ax)
            plt.pause(1e-24)
        network.reset_state_variables()
    acc["train"].append(correct_train/len(train_loader.dataset))

    #Testing
    network.train(mode=False)
    for events, targets in tqdm(test_loader, leave=False, colour='red', total=test_len):
        b = int(events.shape[1])
        t = int(events.shape[0])
        spike_record = torch.zeros((b, t, n_neurons_output), device=device)

        input = {"X": events.to(device)}
        # input = {"X": events.unsqueeze(dim=2).to(device)}
        network.run(inputs=input, time=t, input_time_dim=1)


        spikes = {
            "X": source_monitor.get("s"),
            "Y": output_monitor.get("s"),
        }
        spike_record = spikes["Y"].view(t, b, -1).permute(1, 0, 2)
        
        all_activity_pred = all_activity(
            spikes=spike_record.to(device),
            assignments=assignments,
            n_labels=n_classes)

        proportion_pred = proportion_weighting(
            spikes=spike_record.to(device),
            assignments=assignments,
            proportions=proportions,
            n_labels=n_classes)

        correct_test += torch.sum(targets.long().to(device) ==
                                  all_activity_pred).item()

        network.reset_state_variables()
    acc["test"].append(correct_test/len(test_loader.dataset))
    pbar.update()

runtime = time.time() - start_time

date = datetime.datetime.now()
filetype = '.png'
d = str(date.day) + "_" + str(date.month) + "_" + \
    str(date.year) + "_" + str(date.time())
d = d.split('.')[0]
scriptname = sys.argv[0].split('.')[0]
tf_str = str(transform).replace(' ', '').splitlines(keepends=False)[1:-1]
print(tf_str[1].remove("decay='lin'"))
plot_path = "./plots/" + scriptname + "/acc" + "_" + d + filetype
np.save("./plots/" + scriptname + "/acc_data" +
        "_" + d + '.npy', [acc["train"], acc["test"]])
final_acc = {"train": acc["train"], "test": acc["test"]}
network_str = str(network).replace("(", " ").split(' ')[0]
plot_performance(performances=final_acc, save=plot_path)

filename_raw = 'pokerdvs_results_raw.csv'
filename_pretty = 'pokerdvs_results_pretty.csv'

fields = ['n',
          ' date',
          ' filename',
          ' device',
          ' runtime',
          ' accuracy (train, test)',
          ' samples (train, test)',
          ' network',
          ' epochs',
          ' batch_size',
          ' nu',
          ' layers',
          ' exc',
          ' inh',
          ' wmin',
          ' wmax',
          ' norm',
          ' transform']

num_rows = 0
for r in open(filename_raw):
    num_rows += 1
if num_rows != 0:
    num_rows -= 1

rows = [[num_rows,
        d.replace('_', ' '),
        sys.argv[0].split('.')[0] + '.py',
        device,
        runtime,
        (acc["train"][-1], acc["test"][-1]),
        (len(train_loader.dataset), len(test_loader.dataset)),
        network_str,
        epochs,
        BATCH_SIZE,
        nu,
        layers,
        exc,
        inh,
        wmin,
        wmax,
        norm_4,
        tf_str]]


with open(filename_raw, 'a') as csvfile:
    csvwriter = csv.writer(csvfile)
    if num_rows == 0:
        csvwriter.writerow(fields)
    csvwriter.writerows(rows)

with open(filename_raw) as fp:
    table = from_csv(fp)

f = open(filename_pretty, "w")
f.write(table.get_string())
f.close()
