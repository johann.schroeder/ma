from tonic import CachedDataset
import datetime
import gc
import sys
import time
from prettytable.prettytable import from_csv
import snntorch as snn
import tonic
from snntorch import surrogate
from snntorch import functional as SF
from tqdm import tqdm
import torch
import torch.nn as nn
import matplotlib.pyplot as plt
import numpy as np
import csv
gc.collect()
torch.cuda.empty_cache()
plt.style.use('science')
sensor_size = tonic.datasets.NMNIST.sensor_size
transform = tonic.transforms.Compose(
    [
        tonic.transforms.Downsample(time_factor=0.001),
        tonic.transforms.ToFrame(sensor_size=sensor_size,
                                #  time_window=1,
                                #  n_time_bins=25,
                                 n_event_bins=25,
                                # event_count=1000
                                 ),
        
    ]
)
dtype = torch.float
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
first_saccade_only = False

trainset = tonic.datasets.NMNIST(
    save_to='../data', 
    transform=transform, 
    train=True,
    first_saccade_only=first_saccade_only)
testset = tonic.datasets.NMNIST(
    save_to='../data', 
    transform=transform, 
    train=False,
    first_saccade_only=first_saccade_only)

# time_window 1, 25, 50
# cached_trainset = CachedDataset(
#     trainset, cache_path='../data/cached_datasets/nmnist_TF_tw1_train')
# cached_testset = CachedDataset(
#     testset, cache_path='../data/cached_datasets/nmnist_TF_tw1_test')

# n_time_bins 25, 50, 100
# cached_trainset = CachedDataset(
#     trainset, cache_path='../data/cached_datasets/nmnist_TF_nt25_train')
# cached_testset = CachedDataset(
#     testset, cache_path='../data/cached_datasets/nmnist_TF_nt25_test')

# n_event_bins 25, 50, 100
cached_trainset = CachedDataset(
    trainset, cache_path='../data/cached_datasets/nmnist_TF_ne25_train')
cached_testset = CachedDataset(
    testset, cache_path='../data/cached_datasets/nmnist_TF_ne25_test')

# event_count 100, 250, 500
# cached_trainset = CachedDataset(
#     trainset, cache_path='../data/cached_datasets/nmnist_TF_ec1000_train')
# cached_testset = CachedDataset(
#     testset, cache_path='../data/cached_datasets/nmnist_TF_ec1000_test')

# train samples: 70000, test samples: 10000
BATCH_SIZE = 32
train_subset_len = 10000
test_subset_len = 1000

train_subset = torch.utils.data.random_split(
    cached_trainset, [train_subset_len, len(trainset)-train_subset_len])[0]
test_subset = torch.utils.data.random_split(
    cached_testset, [test_subset_len, len(testset)-test_subset_len])[0]

train_loader = torch.utils.data.DataLoader(dataset=train_subset,
                                           batch_size=BATCH_SIZE,
                                           collate_fn=tonic.collation.PadTensors(),
                                           shuffle=True)

test_loader = torch.utils.data.DataLoader(dataset=test_subset,
                                          batch_size=BATCH_SIZE,
                                          collate_fn=tonic.collation.PadTensors(),
                                          shuffle=False)

train_len = train_subset_len/BATCH_SIZE
test_len = test_subset_len/BATCH_SIZE

EPOCHS = 10
lr = 0.002
num_inputs = np.product(trainset.sensor_size)
num_hidden = 100
num_outputs = len(trainset.classes)
layers = (num_hidden)
beta = 0.5
alpha = 0.5
threshold = 0.5
slope = 50
spike_grad = surrogate.fast_sigmoid(slope=slope)  # slope = 25
# spike_grad = surrogate.LSO(slope = slope) #slope = 0.1
# spike_grad = surrogate.SSO(mean=0.1, variance=0.2) #mean=0.1, variance=0.2
# spike_grad = surrogate.SFS(slope=slope, B=0.9)
# spike_grad = surrogate.spike_rate_escape(beta=0.9, slope=slope)
dt = 1

# Define Network


class Net(nn.Module):
    def __init__(self, dt):
        super().__init__()

        # initialize layers
        self.fc1 = nn.Linear(num_inputs, num_hidden)
        self.fc2 = nn.Linear(num_hidden, num_outputs)
        self.lif1 = snn.Synaptic(
            alpha=alpha, beta=beta, spike_grad=spike_grad, threshold=threshold)
        self.lif2 = snn.Synaptic(alpha=alpha, beta=beta, spike_grad=spike_grad,
                                 threshold=threshold, output=True)

    def forward(self, x):
        seq_len, batch, _, _, _, = x.shape
        x = x.reshape(seq_len, batch, -1)
        syn1, mem1 = self.lif1.init_synaptic()
        syn2, mem2 = self.lif2.init_synaptic()
        spikes = []

        for ts in range(seq_len):
            z = x[ts, :, :]
            z = (z > 0).type(torch.float)
            cur1 = self.fc1(z)
            spk1, syn1, mem1 = self.lif1(cur1, syn1, mem1)
            cur2 = self.fc2(spk1)
            spk2, syn2, mem2 = self.lif2(cur2, syn2, mem2)
            spikes.append(spk2)

        return torch.stack(spikes)

net = Net(dt=dt).to(device)
optimizer = torch.optim.Adam(net.parameters(), lr=lr)
# loss_fn = SF.mse_count_loss()
loss_fn = SF.ce_rate_loss()
# loss_fn = SF.l1_rate_sparsity()
# loss_fn = SF.ce_count_loss()
# loss_fn = SF.mse_membrane_loss()
# loss_fn = SF.ce_max_membrane_loss()


def train(train_loader, device, optimizer):
    losses = []
    correct = 0
    net.train()
    for data, target in tqdm(
        train_loader, leave=False, total=int(train_len), colour='green'):
        target = target.to(device)
        data = data.float().to(device)
        optimizer.zero_grad()
        output = net(data)
        _, pred = output.sum(dim=0).max(1)
        correct += (pred == target).sum().item()
        loss = loss_fn(output, target)
        loss.backward()
        optimizer.step()
        losses.append(loss.item())
    accuracy = correct / len(train_loader.dataset)
    mean_loss = np.mean(losses)
    return losses, mean_loss, accuracy


def test(device, test_loader):
    test_loss = 0
    losses = []
    correct = 0
    with torch.no_grad():
        for data, target in tqdm(
                test_loader, total=int(test_len), leave=False, colour='red'):
            target = target.to(device)
            data = data.float().to(device)
            output = net(data)
            test_loss = loss_fn(output, target).item()
            _, pred = output.sum(dim=0).max(1)
            correct += (pred == target).sum().item()
            losses.append(test_loss)
    test_loss = np.mean(losses)
    accuracy = correct / len(test_loader.dataset)
    return test_loss, accuracy


training_losses = []
mean_losses = []
test_losses = []
train_accuracies = []
test_accuracies = []

torch.autograd.set_detect_anomaly(True)

pbar = tqdm(total=EPOCHS, colour='blue', leave=False)
start_time = time.time()
for epoch in range(EPOCHS):
    training_loss, mean_loss, train_accuracy = train(
        device=device, train_loader=train_loader, optimizer=optimizer)
    test_loss, test_accuracy = test(device=device, test_loader=test_loader)
    training_losses += training_loss
    mean_losses.append(mean_loss)
    test_losses.append(test_loss)
    train_accuracies.append(train_accuracy)
    test_accuracies.append(test_accuracy)
    pbar.set_description(
        f"Acc: [{train_accuracies[-1]:.2f}|{test_accuracies[-1]:.2f}], Loss: [{mean_losses[-1]:.2f}|{test_losses[-1]:.2f}]")
    pbar.update()

runtime = time.time() - start_time

date = datetime.datetime.now()
filetype = '.png'
d = str(date.day) + "_" + str(date.month) + "_" + \
    str(date.year) + "_" + str(date.time())
d = d.split('.')[0]
scriptname = sys.argv[0].split('.')[0]
tf_str = str(transform).replace(" ", "").splitlines(keepends=False)[1:-1]
loss_function = str(loss_fn).split('.')[2]
spike_gradient = str(spike_grad).split('.')[0]
try:
    tf_str[1] = tf_str[1].replace(",decay='exp'", "")
except:
    pass
opt = str(optimizer).replace(
    " ", "").strip().splitlines(keepends=False)[2:-1]
print(
    f"Train Accuracy:{train_accuracies[-1]:.2f}, Test Accuracy{test_accuracies[-1]:.2f}")
fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.plot(mean_losses)
plt.plot(test_losses)
plt.title("Loss Curves")
plt.legend(["Train Loss", "Test Loss"])
plt.xlabel("Epoch")
plt.ylabel("Loss")
# plt.show()
# plt.savefig("./plots/" + scriptname + "/loss" + "_" + d + filetype)

np.save("./plots/" + scriptname + "/loss_data" +
        "_" + d + '.npy', [mean_losses, test_losses])

fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.plot(train_accuracies)
plt.plot(test_accuracies)
plt.title("Accuracy Curves")
plt.legend(["Train Accuracy", "Test Accuracy"])
plt.xlabel("Epoch")
plt.ylabel("Accuracy")
# plt.show()
# plt.savefig("./plots/" + scriptname + "/acc" + "_" + d + filetype)

np.save("./plots/" + scriptname + "/acc_data" +
        "_" + d + '.npy', [train_accuracies, test_accuracies])

filename_raw = 'nmnist_exp_raw.csv'
filename_pretty = 'nmnist_exp_pretty.csv'

fields = ['n',
          ' date',
          ' filename',
          ' device',
          ' runtime',
          ' accuracy (train, test)',
          ' loss (train, test)',
          ' epochs',
          ' batch_size',
          ' opt',
          ' layers',
          ' alpha',
          ' beta',
          ' v_th',
          ' slope',
          ' loss function',
          ' spike gradient',
          ' transform']

num_rows = 0
for r in open(filename_raw):
    num_rows += 1
if num_rows != 0:
    num_rows -= 1

rows = [[num_rows,
        d.replace('_', ' '),
        sys.argv[0].split('.')[0] + '.py',
        device,
        runtime,
        (train_accuracies[-1], test_accuracies[-1]),
        (mean_losses[-1], test_losses[-1]),
        EPOCHS,
        BATCH_SIZE,
        opt,
        layers,
        alpha,
        beta,
        threshold,
        slope,
        loss_function.split(' ')[0],
        spike_gradient.split(' ')[1],
        tf_str]]
with open(filename_raw, 'a') as csvfile:
    csvwriter = csv.writer(csvfile)
    if num_rows == 0:
        csvwriter.writerow(fields)
    csvwriter.writerows(rows)

with open(filename_raw) as fp:
    table = from_csv(fp)

f = open(filename_pretty, "w")
f.write(table.get_string())
f.close()
