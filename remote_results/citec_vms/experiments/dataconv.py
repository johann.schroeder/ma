# import necessary libraries
import pandas as pd
import numpy as np

# create a dummy array
data = np.load('plots/shd_lif_cnnrec/acc_data_14_2_2022_23:46:39.npy')

# convert array into dataframe
DF = pd.DataFrame(data)
DF = DF.transpose()
print(DF)
# save the dataframe as a csv file
# DF.to_latex("data1.dat")
DF.to_csv("acc_test.dat", sep=' ')