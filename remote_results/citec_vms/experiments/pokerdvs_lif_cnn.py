import random
from tonic import CachedDataset
import datetime
import gc
import sys
import time
from prettytable.prettytable import from_csv
import snntorch as snn
import tonic
from snntorch import surrogate, backprop
from snntorch import functional as SF
from tqdm import tqdm, trange
import torch
import torch.nn as nn
from torchvision import transforms
import matplotlib.pyplot as plt
import numpy as np
import torch.nn.functional as F
import csv
plt.style.use('science')
gc.collect()
torch.cuda.empty_cache()
sensor_size = tonic.datasets.POKERDVS.sensor_size
transform = tonic.transforms.Compose(
    [
        tonic.transforms.Downsample(time_factor=0.001),
        tonic.transforms.ToFrame(sensor_size=sensor_size,
                                 time_window=1,
                                # n_event_bins=250,
                                # n_event_bins=1,
                                # event_count=100,
                                # include_incomplete=True
                                 ),
        # tonic.transforms.Denoise(filter_time=10),
        # tonic.transforms.ToTimesurface(sensor_size=tonic.datasets.POKERDVS.sensor_size,
        #    tau=1000,
        #    decay='exp'
        #    )
    ]
)
dtype = torch.float
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

trainset = tonic.datasets.POKERDVS(
    save_to='../data', transform=transform, train=True)
testset = tonic.datasets.POKERDVS(
    save_to='../data', transform=transform, train=False)


# time_window 1, 10, 25
cached_trainset = CachedDataset(
    trainset, cache_path='../data/cached_datasets/pokerdvs_TF_tw1_train')
cached_testset = CachedDataset(
    testset, cache_path='../data/cached_datasets/pokerdvs_TF_tw1_test')

# event_count 25, 50, 100
# cached_trainset = CachedDataset(
#     trainset, cache_path='../data/cached_datasets/poker_TF_ec25_train')
# cached_testset = CachedDataset(
#     testset, cache_path='../data/cached_datasets/poker_TF_ec25_test')

# n_time_bins 10, 25, 100
# cached_trainset = CachedDataset(
#     trainset, cache_path='../data/cached_datasets/poker_TF_nt100_train')
# cached_testset = CachedDataset(
#     testset, cache_path='../data/cached_datasets/poker_TF_nt100_test')

# n_event_bins 10, 25, 100
# cached_trainset = CachedDataset(
#     trainset, cache_path='../data/cached_datasets/poker_TF_ne100_train')
# cached_testset = CachedDataset(
#     testset, cache_path='../data/cached_datasets/poker_TF_ne100_test')

# train samples: 48, test samples: 20
BATCH_SIZE = 8
train_subset_len = 48
test_subset_len = 20

train_subset = torch.utils.data.random_split(
    cached_trainset, [train_subset_len, len(trainset)-train_subset_len])[0]
test_subset = torch.utils.data.random_split(
    cached_testset, [test_subset_len, len(testset)-test_subset_len])[0]

train_loader = torch.utils.data.DataLoader(dataset=train_subset,
                                           batch_size=BATCH_SIZE,
                                           collate_fn=tonic.collation.PadTensors(),
                                           shuffle=True)

test_loader = torch.utils.data.DataLoader(dataset=test_subset,
                                          batch_size=BATCH_SIZE,
                                          collate_fn=tonic.collation.PadTensors(),
                                          shuffle=True)

train_len = train_subset_len/BATCH_SIZE
test_len = test_subset_len/BATCH_SIZE

beta = 0.5
alpha = 0.5
threshold = 0.5
slope = 50
spike_grad = surrogate.fast_sigmoid(slope = slope) #slope = 25
# spike_grad = surrogate.LSO(slope = slope) #slope = 0.1
# spike_grad = surrogate.SSO(mean=0.1, variance=0.2) #mean=0.1, variance=0.2
# spike_grad = surrogate.SFS(slope=slope, B=0.9) 
# spike_grad = surrogate.spike_rate_escape(beta=0.9, slope=slope)
dt=1
# Define Network
class Net(nn.Module):
    def __init__(self, num_filters,
                 kernel_size,
                 num_channels,
                 dense_features,
                 max_pooling,
                 output_features):
        super().__init__()

        # initialize layers
        self.mp = max_pooling

        self.conv1 = nn.Conv2d(num_channels, num_filters[0], kernel_size[0], padding=1)
        self.conv2 = nn.Conv2d(num_filters[0], num_filters[1], kernel_size[1], padding=1)
        self.conv3 = nn.Conv2d(num_filters[1], num_filters[2], kernel_size[2], padding=1)
        self.lif1 = snn.Synaptic(alpha=alpha, beta=beta, spike_grad=spike_grad)
        self.lif2 = snn.Synaptic(alpha=alpha, beta=beta, spike_grad=spike_grad)
        self.lif3 = snn.Synaptic(alpha=alpha, beta=beta, spike_grad=spike_grad)
        self.lif4 = snn.Synaptic(alpha=alpha, beta=beta, spike_grad=spike_grad, output=True)
        self.fc1 = nn.Linear(dense_features, output_features)

    def forward(self, x):
        seq_len, batch, _, _, _, = x.shape
        syn1, mem1 = self.lif1.init_synaptic()
        syn2, mem2 = self.lif2.init_synaptic()
        syn3, mem3 = self.lif3.init_synaptic()
        syn4, mem4 = self.lif4.init_synaptic()
        spikes = []

        for ts in range(seq_len):
            z = x[ts, :, :]
            cur1 = self.conv1(z)
            spk1, syn1, mem1 = self.lif1(cur1, syn1, mem1)
            z = F.max_pool2d(spk1, self.mp[0])
            cur2 = self.conv2(z)
            spk2, syn2, mem2 = self.lif2(cur2, syn2, mem2)
            z = F.max_pool2d(spk2, self.mp[1])
            cur3 = self.conv3(z)
            spk3, syn3, mem3 = self.lif3(cur3, syn3, mem3)
            cur4 = self.fc1(spk3.view(batch, -1))
            spk4, syn4, mem4 = self.lif4(cur4, syn4, mem4)
            spikes.append(spk4)

        return torch.stack(spikes)


EPOCHS = 25
IN_FEATURES = 35
N_CHANNELS = 2
N_FILTERS_1 = 8
N_FILTERS_2 = 12
N_FILTERS_3 = 16
KERNEL_SIZE_1 = 3
KERNEL_SIZE_2 = 3
KERNEL_SIZE_3 = 3
MP_KERNEL_SIZE_1 = 2
MP_KERNEL_SIZE_2 = 2
OUTPUT_FEATURES = len(trainset.classes)
cl1 = int(IN_FEATURES + 2 - 1 * (KERNEL_SIZE_1 - 1))
mpl1 = int((cl1-MP_KERNEL_SIZE_1)/MP_KERNEL_SIZE_1) + 1
cl2 = int(mpl1 + 2 - 1 * (KERNEL_SIZE_2 - 1))
mpl2 = int((cl2-MP_KERNEL_SIZE_2)/MP_KERNEL_SIZE_2) + 1
cl3 = int(mpl2 + 2 - 1 * (KERNEL_SIZE_3-1))
DENSE_FEATURES = cl3**2 * N_FILTERS_3
layers = ('35x35x2' + '-' +
          str(N_FILTERS_1) + 'c' + str(KERNEL_SIZE_1) + '-' +
          str(MP_KERNEL_SIZE_1) + 'p-' +
          str(N_FILTERS_2) + 'c' + str(KERNEL_SIZE_2) + '-' +
          str(MP_KERNEL_SIZE_2) + 'p-' +
          str(N_FILTERS_3) + 'c' + str(KERNEL_SIZE_3) + '-' +
          str(DENSE_FEATURES) + '-' + str(OUTPUT_FEATURES))

net = Net(
    num_filters=[N_FILTERS_1, N_FILTERS_2, N_FILTERS_3],
    kernel_size=[KERNEL_SIZE_1, KERNEL_SIZE_2, KERNEL_SIZE_3],
    num_channels=N_CHANNELS,
    max_pooling=[MP_KERNEL_SIZE_1, MP_KERNEL_SIZE_2],
    dense_features=DENSE_FEATURES,
    output_features=OUTPUT_FEATURES,
    ).to(device)

lr = 0.002
optimizer = torch.optim.Adam(net.parameters(), lr=lr)
# loss_fn = SF.mse_count_loss()
loss_fn = SF.ce_rate_loss()
# loss_fn = SF.ce_count_loss()    
# loss_fn = SF.mse_membrane_loss()
# loss_fn = SF.ce_max_membrane_loss()

def train(train_loader, device, optimizer,):
    losses = []
    correct = 0
    net.train()
    for data, target in tqdm(
            train_loader, leave=False, total=int(train_len), colour='green'):
        target = target.to(device)
        data = data.float().to(device)    
        optimizer.zero_grad()
        output = net(data)
        _, pred = output.sum(dim=0).max(1)
        correct += (pred == target).sum().item()
        loss = loss_fn(output, target)
        loss.backward()
        optimizer.step()
        losses.append(loss.item())
    accuracy = correct / len(train_loader.dataset)
    mean_loss = np.mean(losses)
    return losses, mean_loss, accuracy


def test(device, test_loader):
    test_loss = 0
    losses = []
    correct = 0
    with torch.no_grad():
        for data, target in tqdm(
                test_loader, total=int(test_len), leave=False, colour='red'):
            target = target.to(device)
            data = data.float().to(device)
            output = net(data)
            test_loss = loss_fn(output, target).item()
            _, pred = output.sum(dim=0).max(1)
            correct += (pred == target).sum().item()
            losses.append(test_loss)
    test_loss = np.mean(losses)
    accuracy = correct / len(test_loader.dataset)
    return test_loss, accuracy


training_losses = []
mean_losses = []
test_losses = []
train_accuracies = []
test_accuracies = []

torch.autograd.set_detect_anomaly(True)

pbar = tqdm(total=EPOCHS, colour='blue', leave=False)
start_time = time.time()
for epoch in range(EPOCHS):
    training_loss, mean_loss, train_accuracy = train(
        device=device, train_loader=train_loader, optimizer=optimizer)
    test_loss, test_accuracy = test(device=device, test_loader=test_loader)
    training_losses += training_loss
    mean_losses.append(mean_loss)
    test_losses.append(test_loss)
    train_accuracies.append(train_accuracy)
    test_accuracies.append(test_accuracy)
    pbar.set_description(
        f"Acc: [{train_accuracies[-1]:.2f}|{test_accuracies[-1]:.2f}], Loss: [{mean_losses[-1]:.2f}|{test_losses[-1]:.2f}]")
    pbar.update()

runtime = time.time() - start_time

date = datetime.datetime.now()
filetype = '.png'
d = str(date.day) + "_" + str(date.month) + "_" + \
    str(date.year) + "_" + str(date.time())
d = d.split('.')[0]
scriptname = sys.argv[0].split('.')[0]
tf_str = str(transform).replace(" ", "").splitlines(keepends=False)[1:-1]
try:
    tf_str[1] = tf_str[1].replace(",decay='exp'", "")
except:
    pass
loss_function = str(loss_fn).split('.')[2]
spike_gradient = str(spike_grad).split('.')[0]
opt = str(optimizer).replace(
    " ", "").strip().splitlines(keepends=False)[2:-1]
print(
    f"Train Accuracy:{train_accuracies[-1]:.2f}, Test Accuracy{test_accuracies[-1]:.2f}")
fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.plot(mean_losses)
plt.plot(test_losses)
plt.title("Loss Curves")
plt.legend(["Train Loss", "Test Loss"])
plt.xlabel("Epoch")
plt.ylabel("Loss")
# plt.show()
plt.savefig("./plots/" + scriptname + "/loss" + "_" + d + filetype)

np.save("./plots/" + scriptname + "/loss_data" +
        "_" + d + '.npy', [mean_losses, test_losses])

fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.plot(train_accuracies)
plt.plot(test_accuracies)
plt.title("Accuracy Curves")
plt.legend(["Train Accuracy", "Test Accuracy"])
plt.xlabel("Epoch")
plt.ylabel("Accuracy")
# plt.show()
plt.savefig("./plots/" + scriptname + "/acc" + "_" + d + filetype)

np.save("./plots/" + scriptname + "/acc_data" +
        "_" + d + '.npy', [train_accuracies, test_accuracies])

filename_raw = 'experiment_results_raw.csv'
filename_pretty = 'experiment_results_pretty.csv'

fields = ['n',
          ' date',
          ' filename',
          ' device',
          ' runtime',
          ' accuracy (train, test)',
          ' loss (train, test)',
          ' epochs',
          ' batch_size',
          ' opt',
          ' layers',
          ' alpha',
          ' beta',
          ' v_th',
          ' slope',
          ' loss function',
          ' spike gradient',
          ' transform']

num_rows = 0
for r in open(filename_raw):
    num_rows += 1
if num_rows != 0:
    num_rows -= 1

rows = [[num_rows,
        d.replace('_', ' '),
        sys.argv[0].split('.')[0] + '.py',
        device,
        runtime,
        (train_accuracies[-1], test_accuracies[-1]),
        (mean_losses[-1], test_losses[-1]),
        EPOCHS,
        BATCH_SIZE,
        opt,
        layers,
        alpha,
        beta,
        threshold,
        slope,
        loss_function.split(' ')[0],
        spike_gradient.split(' ')[1],
        tf_str]]


with open(filename_raw, 'a') as csvfile:
    csvwriter = csv.writer(csvfile)
    if num_rows == 0:
        csvwriter.writerow(fields)
    csvwriter.writerows(rows)

with open(filename_raw) as fp:
    table = from_csv(fp)

f = open(filename_pretty, "w")
f.write(table.get_string())
f.close()
