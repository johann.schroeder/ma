from torch import nn
from tqdm import tqdm
from typing import NamedTuple
from norse.torch import LICell, LIState
from norse.torch.module.lif import LIFCell
from norse.torch import LIFParameters, LIFState
import torch
import numpy as np
import matplotlib.pyplot as plt
import tonic
import gc
import csv
import datetime
import time
from prettytable.prettytable import from_csv
import sys
import torch.nn.functional as F
from tonic import CachedDataset
gc.collect()
torch.cuda.empty_cache()
plt.style.use('science')
transform = tonic.transforms.Compose(
    [
        tonic.transforms.Downsample(time_factor=0.001),
        tonic.transforms.ToFrame(sensor_size=tonic.datasets.NMNIST.sensor_size,
                                #  time_window=1,
                                 n_event_bins=5
                                 ),
        # tonic.transforms.Denoise(filter_time=10),
        # tonic.transforms.ToTimesurface(sensor_size=tonic.datasets.NMNIST.sensor_size,
        #    tau=2000,
        #    decay='exp'
        #    )
    ]
)

first_saccade_only = False
trainset = tonic.datasets.NMNIST(
    save_to='../../data',
    transform=transform,
    first_saccade_only=first_saccade_only,
    train=True)
testset = tonic.datasets.NMNIST(
    save_to='../../data',
    transform=transform,
    first_saccade_only=first_saccade_only,
    train=False)

BATCH_SIZE = 64

train_subset_len = 10000
test_subset_len = 1000

#Frames
# cached_trainset = CachedDataset(
#     trainset, cache_path='../../data/cached_datasets/nmnist_TF_tw1_train')
# cached_testset = CachedDataset(
#     testset, cache_path='../../data/cached_datasets/nmnist_TF_tw1_test')

cached_trainset = CachedDataset(
    trainset, cache_path='../../data/cached_datasets/nmnist_TF_ne5_train')
cached_testset = CachedDataset(
    testset, cache_path='../../data/cached_datasets/nmnist_TF_ne5_test')

train_subset = torch.utils.data.random_split(
    cached_trainset, [train_subset_len, len(trainset)-train_subset_len])[0]
test_subset = torch.utils.data.random_split(
    cached_testset, [test_subset_len, len(testset)-test_subset_len])[0]

train_loader = torch.utils.data.DataLoader(dataset=train_subset,
                                           batch_size=BATCH_SIZE,
                                           collate_fn=tonic.collation.PadTensors(),
                                           shuffle=True)

test_loader = torch.utils.data.DataLoader(dataset=test_subset,
                                          batch_size=BATCH_SIZE,
                                          collate_fn=tonic.collation.PadTensors(),
                                          )

train_len = train_subset_len/BATCH_SIZE
test_len = test_subset_len/BATCH_SIZE
dt = 1e-3
neuron_params = LIFParameters(
    alpha=torch.as_tensor(25),
    tau_mem_inv=torch.as_tensor(100),
    tau_syn_inv=torch.as_tensor(200),
    v_th=torch.as_tensor(0.5),
    v_leak=torch.as_tensor(0.0),
    v_reset=torch.as_tensor(0.0),
    method='super'
)
class SNNState(NamedTuple):
    lif0: LIFState
    readout: LIState

class SNN(torch.nn.Module):
    def __init__(self, num_filters,
                 kernel_size,
                 num_channels,
                 dense_features,
                 max_pooling,
                 output_features,
                 dt=dt):
        super(SNN, self).__init__()
        self.mp = max_pooling
        
        self.conv1 = nn.Conv2d(num_channels, num_filters[0], kernel_size[0], padding=1)
        self.conv2 = nn.Conv2d(num_filters[0], num_filters[1], kernel_size[1], padding=1)
        self.conv3 = nn.Conv2d(num_filters[1], num_filters[2], kernel_size[2], padding=1)
        self.lif1 = LIFCell(p=neuron_params, dt=dt)
        self.lif2 = LIFCell(p=neuron_params, dt=dt)
        self.lif3 = LIFCell(p=neuron_params, dt=dt)
        self.fc1 = nn.Linear(dense_features, output_features)
        self.out = LICell(dt=dt)
    def forward(self, x):
        seq_length, batch, _, _, _ = x.shape
        s1 = so = s2 = s3 = None
        voltages = []
        for ts in range(seq_length):
            z = x[ts, :, :]
            z = (z > 0).type(torch.float)
            z = self.conv1(z)
            z, s1 = self.lif1(z, s1)
            z = F.max_pool2d(z, self.mp[0])
            z = self.conv2(z)
            z, s2 = self.lif2(z, s2)
            z = F.max_pool2d(z, self.mp[1])
            z = self.conv3(z)
            z, s3 = self.lif3(z, s3)
            z = self.fc1(z.view(batch, -1))
            vo, so = self.out(z, so)
            voltages += [vo]  # LIF
        return torch.stack(voltages)


def decode(x):
    x, _ = torch.max(x, 0)
    log_p_y = torch.nn.functional.log_softmax(x, dim=1)
    return log_p_y


class Model(torch.nn.Module):
    def __init__(self, snn, decoder):
        super(Model, self).__init__()
        self.snn = snn
        self.decoder = decoder

    def forward(self, x):
        x = self.snn(x)
        log_p_y = self.decoder(x)
        return log_p_y


EPOCHS = 10
LR = 0.002
DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")
IN_FEATURES = 34
N_CHANNELS = 2
N_FILTERS_1 = 8
N_FILTERS_2 = 12
N_FILTERS_3 = 16
KERNEL_SIZE_1 = 3
KERNEL_SIZE_2 = 3
KERNEL_SIZE_3 = 3
MP_KERNEL_SIZE_1 = 2
MP_KERNEL_SIZE_2 = 2
OUTPUT_FEATURES = len(trainset.classes)
cl1 = int(IN_FEATURES + 2 - 1 * (KERNEL_SIZE_1 - 1))
mpl1 = int((cl1-MP_KERNEL_SIZE_1)/MP_KERNEL_SIZE_1) + 1
cl2 = int(mpl1 + 2 - 1 * (KERNEL_SIZE_2 - 1))
mpl2 = int((cl2-MP_KERNEL_SIZE_2)/MP_KERNEL_SIZE_2) + 1
cl3 = int(mpl2 + 2 - 1 * (KERNEL_SIZE_3-1))
DENSE_FEATURES = cl3**2 * N_FILTERS_3
layers = ('34x34x2' + '-' +
          str(N_FILTERS_1) + 'c' + str(KERNEL_SIZE_1) + '-' +
          str(MP_KERNEL_SIZE_1) + 'p-' +
          str(N_FILTERS_2) + 'c' + str(KERNEL_SIZE_2) + '-' +
          str(MP_KERNEL_SIZE_2) + 'p-' +
          str(N_FILTERS_3) + 'c' + str(KERNEL_SIZE_3) + '-' +
          str(DENSE_FEATURES) + '-' + str(OUTPUT_FEATURES))

model = Model(
    snn=SNN(
        num_filters=[N_FILTERS_1, N_FILTERS_2, N_FILTERS_3],
        kernel_size=[KERNEL_SIZE_1, KERNEL_SIZE_2, KERNEL_SIZE_3],
        num_channels=N_CHANNELS,
        max_pooling=[MP_KERNEL_SIZE_1, MP_KERNEL_SIZE_2],
        dense_features=DENSE_FEATURES,
        output_features=OUTPUT_FEATURES),
    decoder=decode
).to(DEVICE)

optimizer = torch.optim.Adam(model.parameters(), lr=LR)

def train(model, device, train_loader, optimizer):
    model.train()
    losses = []
    correct = 0
    for data, target in tqdm(train_loader, leave=False, colour='green', total=int(train_len)):
        data, target = data.float().to(device), torch.LongTensor(target).to(device)
        optimizer.zero_grad()
        output = model(data)
        pred = output.argmax(dim=1, keepdim=True)
        correct += pred.eq(target.view_as(pred)).sum().item()
        loss = torch.nn.functional.nll_loss(output, target)
        loss.backward()
        optimizer.step()
        losses.append(loss.item())
    accuracy = correct / len(train_loader.dataset)
    mean_loss = np.mean(losses)
    return losses, mean_loss, accuracy


def test(model, device, test_loader):
    model.eval()
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in tqdm(test_loader, colour='red', leave=False, total=int(test_len)):
            data, target = data.float().to(device), torch.LongTensor(target).to(device)
            output = model(data)
            test_loss += torch.nn.functional.nll_loss(
                output, target, reduction="sum").item()
            pred = output.argmax(dim=1, keepdim=True)
            correct += pred.eq(target.view_as(pred)).sum().item()
    test_loss /= len(test_loader.dataset)
    accuracy = correct / len(test_loader.dataset)
    return test_loss, accuracy


training_losses = []
mean_losses = []
test_losses = []
train_accuracies = []
test_accuracies = []

torch.autograd.set_detect_anomaly(True)

pbar = tqdm(total=EPOCHS, colour='blue', leave=False)
start_time = time.time()
for epoch in range(EPOCHS):
    training_loss, mean_loss, train_accuracy = train(
        model, DEVICE, train_loader, optimizer)
    test_loss, test_accuracy = test(model, DEVICE, test_loader)
    training_losses += training_loss
    mean_losses.append(mean_loss)
    test_losses.append(test_loss)
    train_accuracies.append(train_accuracy)
    test_accuracies.append(test_accuracy)
    pbar.set_description(
        f"Acc: [{train_accuracies[-1]:.2f}|{test_accuracies[-1]:.2f}], Loss: [{mean_losses[-1]:.2f}|{test_losses[-1]:.2f}]")
    pbar.update()

runtime = time.time() - start_time

date = datetime.datetime.now()
filetype = '.png'
d = str(date.day) + "_" + str(date.month) + "_" + \
    str(date.year) + "_" + str(date.time())
d = d.split('.')[0]
scriptname = sys.argv[0].split('.')[0]
tf_str = str(transform).replace(" ", "").splitlines(keepends=False)[1:-1]
try:
    tf_str[1] = tf_str[1].replace(",decay='exp'", "")
except:
    pass

print(
    f"Train Accuracy:{train_accuracies[-1]:.2f}, Test Accuracy{test_accuracies[-1]:.2f}")
fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.plot(mean_losses)
plt.plot(test_losses)
plt.title("Loss Curves")
plt.legend(["Train Loss", "Test Loss"])
plt.xlabel("Epoch")
plt.ylabel("Loss")
# plt.show()
plt.savefig("./plots/" + scriptname + "/loss" + "_" + d + filetype)

np.save("./plots/" + scriptname + "/loss_data" +
        "_" + d + '.npy', [mean_losses, test_losses])

fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.plot(train_accuracies)
plt.plot(test_accuracies)
plt.title("Accuracy Curves")
plt.legend(["Train Accuracy", "Test Accuracy"])
plt.xlabel("Epoch")
plt.ylabel("Accuracy")
# plt.show()
plt.savefig("./plots/" + scriptname + "/acc" + "_" + d + filetype)

np.save("./plots/" + scriptname + "/acc_data" +
        "_" + d + '.npy', [train_accuracies, test_accuracies])

filename_raw = 'norse_nmnist_lif_results_raw.csv'
filename_pretty = 'norse_nmnist_lif_results_pretty.csv'

fields = ['n',
          ' date',
          ' filename',
          ' device',
          ' runtime',
          ' accuracy (train, test)',
          ' loss (train, test)',
          ' epochs',
          ' batch_size',
          ' lr, dt',
          ' layers',
          ' first saccade only'
          ' method',
          ' alpha',
          ' tau_mem_inv',
          ' tau_syn_inv',
          ' v_leak',
          ' v_reset',
          ' v_th',
          ' transform']

num_rows = 0
for r in open(filename_raw):
    num_rows += 1
if num_rows != 0:
    num_rows -= 1

rows = [[num_rows,
        d.replace('_', ' '),
        sys.argv[0].split('.')[0] + '.py',
        DEVICE,
        runtime,
        (train_accuracies[-1], test_accuracies[-1]),
        (mean_losses[-1], test_losses[-1]),
        EPOCHS,
        BATCH_SIZE,
        (LR, dt),
        layers,
        first_saccade_only,
        neuron_params.method,
        neuron_params.alpha.item(),
        neuron_params.tau_mem_inv.item(),
        neuron_params.tau_syn_inv.item(),
        neuron_params.v_leak.item(),
        neuron_params.v_reset.item(),
        neuron_params.v_th.item(),
        tf_str]]


with open(filename_raw, 'a') as csvfile:
    csvwriter = csv.writer(csvfile)
    if num_rows == 0:
        csvwriter.writerow(fields)
    csvwriter.writerows(rows)

with open(filename_raw) as fp:
    table = from_csv(fp)

f = open(filename_pretty, "w")
f.write(table.get_string())
f.close()
