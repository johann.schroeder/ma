from dataclasses import field
import sys
from typing_extensions import runtime
from tqdm import tqdm, trange
from typing import NamedTuple
from norse.torch import LICell, LIState
from norse.torch.module.lif import LIFCell
from norse.torch import LIFParameters, LIFState
import torch
import numpy as np
import matplotlib.pyplot as plt
import tonic
import gc
import datetime
import csv
from prettytable import PrettyTable, from_csv
import time

gc.collect()
torch.cuda.empty_cache()
plt.style.use('science')
pt = PrettyTable()
transform = tonic.transforms.Compose(
    [
        # tonic.transforms.Downsample(time_factor=0.001),
        # tonic.transforms.ToFrame(sensor_size=tonic.datasets.POKERDVS.sensor_size,
        #                          time_window=1,
        #                          ),
        tonic.transforms.Denoise(filter_time=10),
        tonic.transforms.ToTimesurface(sensor_size=tonic.datasets.POKERDVS.sensor_size,
           tau=2000,
           decay='exp'
           )
    ]
)

trainset = tonic.datasets.POKERDVS(
    save_to='../../data', transform=transform, train=True)
testset = tonic.datasets.POKERDVS(
    save_to='../../data', transform=transform, train=False)

BATCH_SIZE = 8

train_loader = torch.utils.data.DataLoader(dataset=trainset,
                                           batch_size=BATCH_SIZE,
                                           collate_fn=tonic.collation.PadTensors(),
                                           shuffle=True)

test_loader = torch.utils.data.DataLoader(testset,
                                          batch_size=BATCH_SIZE,
                                          collate_fn=tonic.collation.PadTensors(),
                                          shuffle=False)


neuron_params = LIFParameters(
    alpha=torch.as_tensor(100),
    tau_mem_inv=torch.as_tensor(1.0/2e-2),
    tau_syn_inv=torch.as_tensor(1.0/2e-2),
    v_th=torch.as_tensor(0.7),
    v_leak=torch.as_tensor(0.0),
    v_reset=torch.as_tensor(0.0),
    method='super'
)

class SNNState(NamedTuple):
    lif0: LIFState
    readout: LIState


class SNN(torch.nn.Module):
    def __init__(self, input_features,
                 hidden_features_l1,
                 hidden_features_l2,
                 hidden_features_l3,
                 output_features,
                 dt=1e-3):
        super(SNN, self).__init__()
        
        self.fc_in = torch.nn.Linear(input_features, hidden_features_l1)
        self.fc_out = torch.nn.Linear(hidden_features_l1, hidden_features_l2)
        self.fc_out2 = torch.nn.Linear(hidden_features_l2, hidden_features_l3)
        self.fc_out3 = torch.nn.Linear(hidden_features_l3, output_features)
        self.lif1 = LIFCell(p=neuron_params, dt=dt)
        self.lif2 = LIFCell(p=neuron_params, dt=dt)
        self.lif3 = LIFCell(p=neuron_params, dt=dt)
        self.out = LICell(dt=dt)

    def forward(self, x):
        seq_length, batch, _, _, _ = x.shape
        x = x.reshape(seq_length, batch, -1)
        s1 = so = s2 = s3 = None
        voltages = []

        for ts in range(seq_length):
            z = x[ts, :, :]
            z = self.fc_in(z)  # Input -> Hidden_L1
            z, s1 = self.lif1(z, s1)  # LIF
            z = self.fc_out(z)  # Hidden_L1 -> Hidden_L2
            z, s2 = self.lif2(z, s2)  # LIF
            z = self.fc_out2(z)  # Hidden_L2 -> Hidden_L3
            z, s3 = self.lif3(z, s3) # LIF
            z = self.fc_out3(z)  # Hidden -> Output
            vo, so = self.out(z, so)  # LICell Output
            voltages += [vo]  # LIF
        return torch.stack(voltages)


def decode(x):
    x, _ = torch.max(x, 0)
    # x = x[-1]
    log_p_y = torch.nn.functional.log_softmax(x, dim=1)
    return log_p_y


class Model(torch.nn.Module):
    def __init__(self, snn, decoder):
        super(Model, self).__init__()
        self.snn = snn
        self.decoder = decoder

    def forward(self, x):
        x = self.snn(x)
        log_p_y = self.decoder(x)
        return log_p_y


EPOCHS = 25
LR = 0.002
INPUT_FEATURES = np.product(trainset.sensor_size)
HIDDEN_FEATURES_L1 = 500
HIDDEN_FEATURES_L2 = 200
HIDDEN_FEATURES_L3 = 100
OUTPUT_FEATURES = len(trainset.classes)
DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")
layers = (HIDDEN_FEATURES_L1, HIDDEN_FEATURES_L2, HIDDEN_FEATURES_L3)

model = Model(
    snn=SNN(
        input_features=INPUT_FEATURES,
        hidden_features_l1=HIDDEN_FEATURES_L1,
        hidden_features_l2=HIDDEN_FEATURES_L2,
        hidden_features_l3=HIDDEN_FEATURES_L3,
        output_features=OUTPUT_FEATURES),
    decoder=decode
).to(DEVICE)

optimizer = torch.optim.Adam(model.parameters(), lr=LR)


def train(model, device, train_loader, optimizer):
    model.train()
    losses = []
    correct = 0
    for (data, target) in tqdm(train_loader, leave=False, colour='green'):
        data, target = data.float().to(device), torch.LongTensor(target).to(device)
        optimizer.zero_grad()
        output = model(data)
        pred = output.argmax(dim=1, keepdim=True)
        correct += pred.eq(target.view_as(pred)).sum().item()
        loss = torch.nn.functional.nll_loss(output, target)
        loss.backward()
        optimizer.step()
        losses.append(loss.item())
    accuracy = correct / len(train_loader.dataset)
    mean_loss = np.mean(losses)
    return losses, mean_loss, accuracy


def test(model, device, test_loader):
    model.eval()
    test_loss = 0
    correct = 0
    losses = []
    with torch.no_grad():
        for data, target in tqdm(test_loader, colour='red', leave=False):
            data, target = data.float().to(device), torch.LongTensor(target).to(device)
            output = model(data)
            test_loss += torch.nn.functional.nll_loss(
                output, target, reduction="sum"
            ).item()  # sum up batch loss
            pred = output.argmax(dim=1, keepdim=True)
            correct += pred.eq(target.view_as(pred)).sum().item()

    test_loss /= len(test_loader.dataset)
    accuracy = correct / len(test_loader.dataset)
    return test_loss, accuracy


training_losses = []
mean_losses = []
test_losses = []
train_accuracies = []
test_accuracies = []

torch.autograd.set_detect_anomaly(True)

pbar = tqdm(total=EPOCHS, colour='blue', leave=False)
start_time = time.time()
for epoch in range(EPOCHS):
    training_loss, mean_loss, train_accuracy = train(
        model, DEVICE, train_loader, optimizer)
    test_loss, test_accuracy = test(model, DEVICE, test_loader)
    training_losses += training_loss
    mean_losses.append(mean_loss)
    test_losses.append(test_loss)
    train_accuracies.append(train_accuracy)
    test_accuracies.append(test_accuracy)
    pbar.update()
    
runtime = time.time() - start_time

date = datetime.datetime.now()
filetype = '.png'
d = str(date.day) + "_" + str(date.month) + "_" + str(date.year) + "_" + str(date.time())
d = d.split('.')[0]
scriptname = sys.argv[0].split('.')[0]
tf_str = str(transform).replace(" ", "").splitlines(keepends=False)[1:-1]
try:
    tf_str[1] = tf_str[1].replace(",decay='exp'", "")
except:
    pass

print(
    f"Train Accuracy:{train_accuracies[-1]:.2f}, Test Accuracy{test_accuracies[-1]:.2f}")
fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.plot(mean_losses)
plt.plot(test_losses)
plt.title("Loss Curves")
plt.legend(["Train Loss", "Test Loss"])
plt.xlabel("Epoch")
plt.ylabel("Loss")
# plt.show()
plt.savefig("./plots/" + scriptname + "/loss" + "_" + d + filetype)

np.save("./plots/" + scriptname + "/loss_data" +
        "_" + d + '.npy', [mean_losses, test_losses])

fig = plt.figure(facecolor="w", figsize=(10, 5))
plt.plot(train_accuracies)
plt.plot(test_accuracies)
plt.title("Accuracy Curves")
plt.legend(["Train Accuracy", "Test Accuracy"])
plt.xlabel("Epoch")
plt.ylabel("Accuracy")
# plt.show()
plt.savefig("./plots/" + scriptname + "/acc" + "_" + d + filetype)

np.save("./plots/" + scriptname + "/acc_data" +
        "_" + d + '.npy', [train_accuracies, test_accuracies])

filename_raw = 'norse_pokerdvs_lif_results_raw.csv'
filename_pretty = 'norse_pokerdvs_lif_results_pretty.csv'
    
fields = ['n',
          ' date', 
          ' filename',
          ' device',
          ' runtime',
          ' accuracy (train, test)',
          ' loss (train, test)',
          ' epochs', 
          ' batch_size',
          ' lr',
          ' layers',
          ' method', 
          ' alpha', 
          ' tau_mem_inv',
          ' tau_syn_inv',
          ' v_leak',
          ' v_reset',
          ' v_th',
          ' transform']

num_rows = 0
for r in open(filename_raw):
    num_rows +=1
if num_rows != 0: num_rows -=1
    
rows = [[num_rows,
        d.replace('_', ' '),
        sys.argv[0].split('.')[0] + '.py',
        DEVICE,
        runtime,
        (train_accuracies[-1], test_accuracies[-1]),
        (mean_losses[-1], test_losses[-1]),
        EPOCHS,
        BATCH_SIZE,
        LR,
        layers,
        neuron_params.method,
        neuron_params.alpha.item(),
        neuron_params.tau_mem_inv.item(),
        neuron_params.tau_syn_inv.item(),
        neuron_params.v_leak.item(),
        neuron_params.v_reset.item(),
        neuron_params.v_th.item(),
        tf_str]]


with open(filename_raw, 'a') as csvfile:
    csvwriter = csv.writer(csvfile)
    if num_rows == 0:
        csvwriter.writerow(fields)
    csvwriter.writerows(rows)

with open(filename_raw) as fp:
    table = from_csv(fp)

f = open(filename_pretty, "w")
f.write(table.get_string()) 
f.close()
