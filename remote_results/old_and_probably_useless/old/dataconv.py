# import necessary libraries
import pandas as pd
import numpy as np

sep = '_'
file = 'pokerdvs_tw5'
prefix = '/plots/'
acc_str = '/acc_data_'
loss_str = '/loss_data_'
date = '23_2_2022'
time = '14:28:24'
filetype = '.npy'

out_type = 'mean_acc'
out_filetype = '.dat'
output_folder = 'convergence/'

# acc = np.load(prefix + file + acc_str + date + sep + time + filetype)
# loss = np.load(prefix + file + loss_str + date + sep + time + filetype)
acc = np.load('pokerdvs_lif_1l/acc_data_28_2_2022_17:22:54.npy')
acc1 = np.load('pokerdvs_lif_1l/acc_data_28_2_2022_17:23:02.npy')
acc2 = np.load('pokerdvs_lif_1l/acc_data_28_2_2022_17:23:10.npy')
acc3 = np.load('pokerdvs_lif_1l/acc_data_28_2_2022_17:23:18.npy')
acc4 = np.load('pokerdvs_lif_1l/acc_data_28_2_2022_17:23:26.npy')
acc5 = np.load('pokerdvs_lif_1l/acc_data_28_2_2022_17:23:34.npy')
acc6 = np.load('pokerdvs_lif_1l/acc_data_28_2_2022_17:23:41.npy')
acc7 = np.load('pokerdvs_lif_1l/acc_data_28_2_2022_17:23:50.npy')
acc8 = np.load('pokerdvs_lif_1l/acc_data_28_2_2022_17:23:58.npy')
acc9 = np.load('pokerdvs_lif_1l/acc_data_28_2_2022_17:24:06.npy')
# loss = np.load('pokerdvs_lif_1l/loss_data_22_2_2022_13:33:14.npy')

acc = (acc+acc1+acc2+acc3+acc4+acc5+acc6+acc7+acc8+acc9)/10
DF = pd.DataFrame(acc)
DF = DF.transpose()
DF.columns = ['Train', 'Test']
DF.to_csv('acc_' + file + sep + out_type + out_filetype, sep=' ', index_label='Epoch')
# DF.to_csv("acc_pokerdvs_ec100.dat", sep=' ', index_label='Epoch')

# DF = pd.DataFrame(loss)
# DF = DF.transpose()
# DF.columns = ['Train', 'Test']
# DF.to_csv(output_folder + 'loss_' + file + sep + out_type + out_filetype, sep=' ', index_label='Epoch')
# DF.to_csv("loss_pokerdvs_ec100.dat", sep=' ', index_label='Epoch')

