#!/bin/bash

#Job parameters
#SBATCH --job-name=slayer_dvsgesture_cnn
#SBATCH --output=./output_cnn.out
#SBATCH --error=./error_cnn.err
#SBATCH --mail-user=joschroeder@techfak.uni-bielefeld.de --mail-type=FAIL

#Resources
#SBATCH --time=16:00:00
#SBATCH --partition=volta_compute
#SBATCH --qos=regular
#SBATCH --ntasks=2
#SBATCH --nodes=1
#SBATCH --tasks-per-node=2
#SBATCH --cpus-per-task=2
#SBATCH --gpus-per-task=1

source $HOME/senv/bin/activate

echo -e "Start $(date +"%F %T") | $SLURM_JOB_ID $SLURM_JOB_NAME | $(hostname) | $(pwd) \n" #log start time etc.

#Job step(s)
srun --ntasks=1 python -u dvsgesture_cnn_lif.py 
srun --ntasks=1 python -u dvsgesture_cnn.py 

echo -e "End $(date +"%F %T") | $SLURM_JOB_ID $SLURM_JOB_NAME | $(hostname) | $(pwd) \n" #log end time etc.

