#!/bin/bash

#Job parameters
#SBATCH --job-name=slayer_dvsgesture_3l_tm
#SBATCH --output=./output.out
#SBATCH --error=./error.err
#SBATCH --mail-user=joschroeder@techfak.uni-bielefeld.de --mail-type=FAIL

#Resources
#SBATCH --time=00:30:00
#SBATCH --partition=volta_compute
#SBATCH --qos=devel
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --tasks-per-node=1
#SBATCH --cpus-per-task=2
#SBATCH --gpus-per-task=2

source $HOME/senv/bin/activate

echo -e "Start $(date +"%F %T") | $SLURM_JOB_ID $SLURM_JOB_NAME | $(hostname) | $(pwd) \n" #log start time etc.

#Job step(s)
srun --ntasks=1 --nodes=1  python -u dvsgesture_3l.py 

echo -e "End $(date +"%F %T") | $SLURM_JOB_ID $SLURM_JOB_NAME | $(hostname) | $(pwd) \n" #log end time etc.

