#!/bin/bash

#Job parameters
#SBATCH --job-name=slayer_dvsgesture_fc_srm
#SBATCH --output=./output_fc.out
#SBATCH --error=./error_fc.err
#SBATCH --mail-user=joschroeder@techfak.uni-bielefeld.de --mail-type=FAIL

#Resources
#SBATCH --time=10:00:00
#SBATCH --partition=volta_compute
#SBATCH --qos=regular
#SBATCH --ntasks=4
#SBATCH --nodes=1
#SBATCH --tasks-per-node=4
#SBATCH --cpus-per-task=2
#SBATCH --gpus-per-task=1

source $HOME/senv/bin/activate

echo -e "Start $(date +"%F %T") | $SLURM_JOB_ID $SLURM_JOB_NAME | $(hostname) | $(pwd) \n" #log start time etc.

#Job step(s)
srun --ntasks=1 python -u dvsgesture_1l.py 
srun --ntasks=1 python -u dvsgesture_2l.py
srun --ntasks=1 python -u dvsgesture_3l.py
# srun --ntasks=1 python -u dvsgesture_1l_lif.py 
# srun --ntasks=1 python -u dvsgesture_2l_lif.py
# srun --ntasks=1 python -u dvsgesture_3l_lif.py



echo -e "End $(date +"%F %T") | $SLURM_JOB_ID $SLURM_JOB_NAME | $(hostname) | $(pwd) \n" #log end time etc.

