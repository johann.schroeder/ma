#!/bin/bash

#Job parameters
#SBATCH --job-name=norse_dvsgesture_fc_lif
#SBATCH --output=./norse_output_fc.out
#SBATCH --error=./norse_error_fc.err
#SBATCH --mail-user=joschroeder@techfak.uni-bielefeld.de --mail-type=FAIL

#Resources
#SBATCH --time=16:00:00
#SBATCH --partition=volta_compute
#SBATCH --qos=regular
#SBATCH --ntasks=4
#SBATCH --nodes=1
#SBATCH --tasks-per-node=4
#SBATCH --cpus-per-task=2
#SBATCH --gpus-per-task=1

source $HOME/senv/bin/activate

echo -e "Start $(date +"%F %T") | $SLURM_JOB_ID $SLURM_JOB_NAME | $(hostname) | $(pwd) \n" #log start time etc.

#Job step(s)
# srun --ntasks=1 python -u dvsgesture_izhi_1l.py 
# srun --ntasks=1 python -u dvsgesture_izhi_2l.py
# srun --ntasks=1 python -u dvsgesture_izhi_3l.py
# srun --ntasks=1 python -u dvsgesture_izhi_rec.py
srun --ntasks=1 python -u dvsgesture_lif_1l.py 
srun --ntasks=1 python -u dvsgesture_lif_2l.py
srun --ntasks=1 python -u dvsgesture_lif_3l.py
srun --ntasks=1 python -u dvsgesture_lif_rec.py

echo -e "End $(date +"%F %T") | $SLURM_JOB_ID $SLURM_JOB_NAME | $(hostname) | $(pwd) \n" #log end time etc.

