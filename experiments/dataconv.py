# import necessary libraries
import pandas as pd
import numpy as np

sep = '_'
file = 'nmnist_exp'
prefix = 'plots/'
acc_str = '/acc_data_'
loss_str = '/loss_data_'
date = '20_2_2022'
time = '17:31:55'
filetype = '.npy'

out_type = 'exptw1'
out_filetype = '.dat'

# acc = np.load(prefix + file + acc_str + date + sep + time + filetype)
# loss = np.load(prefix + file + loss_str + date + sep + time + filetype)
# acc = np.load('experiments/snntorch/nmnist/1acc_nmnist_pl_1l_exptw1.dat')
loss = np.load('plots/pokerdvs_lif_1l/loss_data_22_2_2022_12:08:15.npy')
acc = pd.read_csv('snntorch/nmnist/unbn/acc_nmnist_pl_1l_tw25_bn_1.dat', delimiter=' ')
acc1 = pd.read_csv('snntorch/nmnist/unbn/acc_nmnist_pl_1l_tw25_bn_2.dat', delimiter=' ')
acc2 = pd.read_csv('snntorch/nmnist/unbn/acc_nmnist_pl_1l_tw25_bn_3.dat', delimiter=' ')
acc3 = pd.read_csv('snntorch/nmnist/unbn/acc_nmnist_pl_1l_tw25_bn_4.dat', delimiter=' ')
acc4 = pd.read_csv('snntorch/nmnist/unbn/acc_nmnist_pl_1l_tw25_bn_5.dat', delimiter=' ')

print((acc + acc1 + acc2 + acc3 + acc4)/5)
DF = pd.DataFrame(acc)
DF = DF.transpose()
DF.columns = ['Test']
DF.to_csv('acc_' + file + sep + out_type + out_filetype, sep=' ', index_label='Epoch')
# DF.to_csv("acc_pokerdvs_ec100.dat", sep=' ', index_label='Epoch')

DF = pd.DataFrame(loss)
DF = DF.transpose()
DF.columns = ['Train', 'Test']
DF.to_csv('pokerdvs/' + 'loss_' + file + sep + out_type + out_filetype, sep=' ', index_label='Epoch')
# DF.to_csv("loss_pokerdvs_ec100.dat", sep=' ', index_label='Epoch')

