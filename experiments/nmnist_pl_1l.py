import pandas as pd
from pytorch_lightning.callbacks import RichProgressBar
import pytorch_lightning as pl
import datetime
import gc
import sys
import time
from prettytable.prettytable import from_csv
import snntorch as snn
import tonic
from snntorch import surrogate, utils
from snntorch import functional as SF
import torch
import torch.nn as nn
import matplotlib.pyplot as plt
import numpy as np
import csv
import torchvision as tv
from tonic import CachedDataset
import torchmetrics
gc.collect()
torch.cuda.empty_cache()
sensor_size = tonic.datasets.NMNIST.sensor_size
transform = tonic.transforms.Compose(
    [
        tonic.transforms.Downsample(time_factor=0.001, spatial_factor=1),
        tonic.transforms.ToFrame(sensor_size=tonic.datasets.NMNIST.sensor_size,
                                 time_window=25,
                                 #  n_time_bins=100,
                                 #  n_event_bins=100,
                                 #  event_count=500
                                 ),
        # tonic.transforms.Denoise(filter_time=50),
        # tonic.transforms.ToTimesurface(sensor_size=tonic.datasets.NMNIST.sensor_size,
        #                                 tau=5000,
        #                                 decay='exp'
        #                                 )
    ]
)


trainset = tonic.datasets.NMNIST(
    save_to='../data', transform=transform, train=True)
testset = tonic.datasets.NMNIST(
    save_to='../data',  transform=transform, train=False)

# Frames
cached_dataset_train = CachedDataset(
    trainset, cache_path='../data/cached_datasets/nmnist_TF_tw25_train')
cached_dataset_test = CachedDataset(
    testset, cache_path='../data/cached_datasets/nmnist_TF_tw25_test')

# #Timesurfaces
# cached_dataset_train = CachedDataset(
#     trainset, cache_path='../data/cached_datasets/nmnist_TTS_f50_5k_train')
# cached_dataset_test = CachedDataset(
#     testset, cache_path='../data/cached_datasets/nmnist_TTS_f50_5k_test')

# train samples: 1077, test samples: 264
BATCH_SIZE = 32
train_subset_len = 10000
test_subset_len = 1000

train_subset = torch.utils.data.random_split(
    cached_dataset_train, [train_subset_len, len(trainset)-train_subset_len])[0]
test_subset = torch.utils.data.random_split(
    cached_dataset_test, [test_subset_len, len(testset)-test_subset_len])[0]

train_loader = torch.utils.data.DataLoader(dataset=train_subset,
                                           batch_size=BATCH_SIZE,
                                           collate_fn=tonic.collation.PadTensors(),
                                           shuffle=True,
                                           num_workers=4)

test_loader = torch.utils.data.DataLoader(dataset=test_subset,
                                          batch_size=BATCH_SIZE,
                                          collate_fn=tonic.collation.PadTensors(),
                                          shuffle=False,
                                          num_workers=4)


dtype = torch.float
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
EPOCHS = 15
lr = 0.002
num_inputs = np.product(sensor_size)
num_hidden = 100
num_outputs = len(trainset.classes)
layers = (num_hidden)
beta = 0.5
alpha = 0.5
threshold = 0.5
slope = 50
spike_grad = surrogate.fast_sigmoid(slope=slope)  # slope = 25
# spike_grad = surrogate.LSO(slope = slope) #slope = 0.1
# spike_grad = surrogate.SSO(mean=0.1, variance=0.2) #mean=0.1, variance=0.2
# spike_grad = surrogate.SFS(slope=slope, B=0.9)
# spike_grad = surrogate.spike_rate_escape(beta=0.9, slope=slope)

# loss_fn = SF.mse_count_loss()
loss_fn = SF.ce_rate_loss()
# loss_fn = SF.ce_count_loss()
# loss_fn = SF.mse_membrane_loss()
# loss_fn = SF.ce_max_membrane_loss()
dt = 1

train_losses = []
test_losses = []
train_acc = []
test_acc = []


class SNN(pl.LightningModule):
    def __init__(self):
        super(SNN, self).__init__()
        self.fc1 = nn.Linear(num_inputs, num_hidden)
        self.fc2 = nn.Linear(num_hidden, num_outputs)
        self.lif1 = snn.Synaptic(alpha=alpha, beta=beta, spike_grad=spike_grad,
                                 threshold=threshold)
        self.lif2 = snn.Synaptic(alpha=alpha, beta=beta, spike_grad=spike_grad,
                                 threshold=threshold, output=True)

    def forward(self, x):
        seq_len, batch, _, _, _, = x.shape
        x = x.reshape(seq_len, batch, -1)
        syn1, mem1 = self.lif1.init_synaptic()
        syn2, mem2 = self.lif2.init_synaptic()
        spikes = voltages = []

        for ts in range(seq_len):
            z = x[ts, :, :].type(torch.float)
            # z = (z > 0).type(torch.float)
            cur1 = self.fc1(z)
            spk1, syn1, mem1 = self.lif1(cur1, syn1, mem1)
            cur2 = self.fc2(spk1)
            spk2, syn2, mem2 = self.lif2(cur2, syn2, mem2)
            spikes.append(spk2)
            voltages.append(mem2)
        return torch.stack(spikes), torch.stack(voltages)

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=lr)
        return optimizer

    def training_step(self, batch, batch_idx):
        data, target = batch
        spikes, output = self(data)
        _, pred = spikes.sum(dim=0).max(1)
        correct = (pred == target).sum()
        train_acc = correct/len(target)
        loss = loss_fn(spikes, target)
        values = {'loss': loss,
                  'train_acc': train_acc}
        self.log_dict(values, on_step=False,
                      on_epoch=True, batch_size=BATCH_SIZE)
        return values

    def training_epoch_end(self, outputs):
        avg_loss = torch.stack([x['loss'] for x in outputs]).mean()
        avg_acc = torch.stack([x['train_acc'] for x in outputs]).mean()
        train_losses.append(avg_loss.item())
        train_acc.append(avg_acc.item())

    def validation_step(self, batch, batch_idx):
        data, target = batch
        spikes, output = self(data)
        _, pred = spikes.sum(dim=0).max(1)
        correct = (pred == target).sum()
        test_acc = correct/len(target)
        loss = loss_fn(spikes, target)
        values = {'loss': loss,
                  'test_acc': test_acc}
        self.log_dict(values, on_step=False,
                      on_epoch=True, batch_size=BATCH_SIZE)
        return values

    def validation_epoch_end(self, outputs):
        avg_loss = torch.stack([x['loss'] for x in outputs]).mean()
        avg_acc = torch.stack([x['test_acc'] for x in outputs]).mean()
        test_losses.append(avg_loss.item())
        test_acc.append(avg_acc.item())


model = SNN()
trainer = pl.Trainer(gpus=1,
                     max_epochs=EPOCHS,
                     log_every_n_steps=1,
                     num_nodes=1,
                     num_sanity_val_steps=0,
                     callbacks=RichProgressBar())

start_time = time.time()
trainer.fit(model=model,
            train_dataloaders=train_loader,
            val_dataloaders=test_loader)
runtime = time.time() - start_time


print(
    f"Train Accuracy:{train_acc[-1]:.2f}, Test Accuracy{test_acc[-1]:.2f}")

date = datetime.datetime.now()
d = str(date.day) + "_" + str(date.month) + "_" + \
    str(date.year) + "_" + str(date.time())
d = d.split('.')[0]
scriptname = sys.argv[0].split('/')[-1]
scriptname = scriptname.split('.')[0]
tf_str = str(transform).replace(" ", "").splitlines(keepends=False)[1:-1]
loss_function = str(loss_fn).split('.')[2]
spike_gradient = str(spike_grad).split('.')[0]
try:
    tf_str[1] = tf_str[1].replace(",decay='exp'", "")
except:
    pass
opt = str(trainer.optimizers).replace(
    " ", "").strip().splitlines(keepends=False)[2:-1]
pr_folder = 'snntorch/nmnist/unbn/'
exp = '_tw25_bn_2'
DF = pd.DataFrame([train_acc, test_acc])
DF = DF.transpose()
DF.columns = ['Train', 'Test']
DF.to_csv(pr_folder + 'acc_' + scriptname +
          exp + '.dat', sep=' ', index_label='Epoch')


# DF = pd.DataFrame([train_losses, test_losses])
# DF = DF.transpose()
# DF.columns = ['Train', 'Test']
# DF.to_csv(pr_folder +  '_loss_' + scriptname +
#           exp + '.dat', sep=' ', index_label='Epoch')

filename_raw = 'nmnist_exp_raw.csv'
filename_pretty = 'nmnist_exp_pretty.csv'

fields = ['n',
          ' date',
          ' filename',
          ' device',
          ' runtime',
          ' accuracy (train, test)',
          ' loss (train, test)',
          ' epochs',
          ' batch_size',
          ' opt',
          ' layers',
          ' alpha',
          ' beta',
          ' v_th',
          ' slope',
          ' loss function',
          ' spike gradient',
          ' transform']

num_rows = 0
for r in open(filename_raw):
    num_rows += 1
if num_rows != 0:
    num_rows -= 1

rows = [[num_rows,
        d.replace('_', ' '),
        scriptname + '.py',
        device,
        runtime,
        (train_acc[-1], test_acc[-1]),
        (train_losses[-1], test_losses[-1]),
        EPOCHS,
        BATCH_SIZE,
        opt,
        layers,
        alpha,
        beta,
        threshold,
        slope,
        loss_function.split(' ')[0],
        spike_gradient.split(' ')[1],
        tf_str]]


with open(filename_raw, 'a') as csvfile:
    csvwriter = csv.writer(csvfile)
    if num_rows == 0:
        csvwriter.writerow(fields)
    csvwriter.writerows(rows)

with open(filename_raw) as fp:
    table = from_csv(fp)

f = open(filename_pretty, "w")
f.write(table.get_string())
f.close()
