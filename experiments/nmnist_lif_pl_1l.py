import csv
import datetime
import time
import pandas as pd
from prettytable.prettytable import from_csv
import torchmetrics
from tqdm import tqdm
from norse.torch import LICell, LIState
from norse.torch.module.lif import LIFCell
from norse.torch import LIFParameters, LIFState
import torch
import numpy as np
import matplotlib.pyplot as plt
import tonic
import gc
import sys
from tonic import CachedDataset
import pytorch_lightning as pl
from pytorch_lightning.callbacks import RichProgressBar
gc.collect()
torch.cuda.empty_cache()
plt.style.use('science')
sensor_size = tonic.datasets.NMNIST.sensor_size
transform = tonic.transforms.Compose(
    [
        tonic.transforms.Downsample(time_factor=0.001),
        tonic.transforms.ToFrame(sensor_size=sensor_size,
                                #  time_window=25,
                                 n_time_bins=50,
                                # event_count=250,
                                #  n_event_bins=10,
                                 include_incomplete=True
                                 ),
        # tonic.transforms.Denoise(filter_time=10),
        # tonic.transforms.ToTimesurface(sensor_size=tonic.datasets.POKERDVS.sensor_size,
        #    tau=1000,
        #    decay='exp'
        #    )
    ]
)

test_transform = tonic.transforms.Compose(
    [
        tonic.transforms.Downsample(time_factor=0.001),
        tonic.transforms.ToFrame(sensor_size=sensor_size,
                                #  time_window=25,
                                #   n_time_bins=50,
                                #  event_count=250,
                                n_event_bins=25,
                                 include_incomplete=True
                                 ),
        # tonic.transforms.Denoise(filter_time=10),
        # tonic.transforms.ToTimesurface(sensor_size=tonic.datasets.POKERDVS.sensor_size,
        #    tau=1000,
        #    decay='exp'
        #    )
    ]
)


trainset = tonic.datasets.NMNIST(
    save_to='../data', transform=transform, train=True)
valset = tonic.datasets.NMNIST(
    save_to='../data', transform=transform, train=False)
testset = tonic.datasets.NMNIST(
    save_to='../data', transform=test_transform, train=False)

#Frames
cached_dataset_train = CachedDataset(
    trainset, cache_path='../data/cached_datasets/nmnist_TF_nt50_train')
cached_dataset_val = CachedDataset(
    valset, cache_path='../data/cached_datasets/nmnist_TF_nt50_val')
cached_dataset_test = CachedDataset(
    testset, cache_path='../data/cached_datasets/nmnist_TF_nt50_test')

# #Timesurfaces
# cached_dataset_train = CachedDataset(
#     trainset, cache_path='../../data/cached_datasets/pokerdvs_TTS_f100_train')
# cached_dataset_test = CachedDataset(
#     testset, cache_path='../../data/cached_datasets/pokerdvs_TTS_f100_test')

BATCH_SIZE = 32
train_subset_len = 10000
test_subset_len = 1000

train_subset = torch.utils.data.random_split(
    cached_dataset_train, [train_subset_len, len(trainset)-train_subset_len])[0]
val_subset = torch.utils.data.random_split(
    cached_dataset_val, [test_subset_len, len(valset)-test_subset_len])[0]
test_subset = torch.utils.data.random_split(
    cached_dataset_test, [test_subset_len, len(testset)-test_subset_len])[0]

train_loader = torch.utils.data.DataLoader(dataset=train_subset,
                                           batch_size=BATCH_SIZE,
                                           collate_fn=tonic.collation.PadTensors(),
                                           num_workers=4,
                                           shuffle=True)

val_loader = torch.utils.data.DataLoader(dataset=val_subset,
                                         batch_size=BATCH_SIZE,
                                         collate_fn=tonic.collation.PadTensors(),
                                         num_workers=4,
                                         shuffle=False)

test_loader = torch.utils.data.DataLoader(dataset=test_subset,
                                          batch_size=BATCH_SIZE,
                                          collate_fn=tonic.collation.PadTensors(),
                                          num_workers=4,
                                          shuffle=False)


neuron_params = LIFParameters(
    alpha=torch.as_tensor(50),
    tau_syn_inv=torch.as_tensor(100),
    tau_mem_inv=torch.as_tensor(200),
    v_th=torch.as_tensor(0.5),
    v_leak=torch.as_tensor(0.0),
    v_reset=torch.as_tensor(0.0),
    method='super'
)

EPOCHS = 15
LR = 0.002
dt = 1e-3
INPUT_FEATURES = np.product(sensor_size)
HIDDEN_FEATURES = 100
OUTPUT_FEATURES = len(tonic.datasets.NMNIST.classes)
DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")
layers = (HIDDEN_FEATURES)
train_losses = []
val_losses = []
train_acc = []
val_acc = []
test_acc = []
test_losses = []
# torch.manual_seed(2)
test_mode = False


class SNN(pl.LightningModule):
    def __init__(self, dt=dt):
        super(SNN, self).__init__()
        self.fc_in = torch.nn.Linear(np.product(sensor_size), HIDDEN_FEATURES)
        self.fc_out = torch.nn.Linear(HIDDEN_FEATURES, OUTPUT_FEATURES)
        self.lif1 = LIFCell(p=neuron_params, dt=dt)
        self.out = LICell(dt=dt)
        self.lif1_test = LIFCell(p=neuron_params, dt=dt)
        self.out_test = LICell(dt=dt)
        self.train_acc = torchmetrics.Accuracy()
        self.val_acc = torchmetrics.Accuracy()
        self.test_acc = torchmetrics.Accuracy()

    def forward(self, x):
        if test_mode == False:
            seq_length, batch, _, _, _ = x.shape
            x = x.reshape(seq_length, batch, -1)
            s1 = so = None
            voltages = []
            for ts in range(seq_length):
                z = x[ts, :, :]
                z = (z > 0).type(torch.float)
                z = self.fc_in(z)  # Input -> Hidden
                z, s1 = self.lif1(z, s1)  # LIF
                z = self.fc_out(z)  # Hidden -> Output
                vo, so = self.out(z, so)  # LI Output
                voltages += [vo]  # Output
            x, _ = torch.max(torch.stack(voltages), 0)
            log_p_y = torch.nn.functional.log_softmax(x, dim=1)
            return log_p_y
        else:
            seq_length, batch, _, _, _ = x.shape
            x = x.reshape(seq_length, batch, -1)
            s1 = so = None
            voltages = []
            for ts in range(seq_length):
                z = x[ts, :, :]
                z = (z > 0).type(torch.float)
                z = self.fc_in(z)  # Input -> Hidden
                z, s1 = self.lif1_test(z, s1)  # LIF
                z = self.fc_out(z)  # Hidden -> Output
                vo, so = self.out_test(z, so)  # LI Output
                voltages += [vo]  # Output
            x, _ = torch.max(torch.stack(voltages), 0)
            log_p_y = torch.nn.functional.log_softmax(x, dim=1)
            return log_p_y

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=LR)
        return optimizer

    def training_step(self, batch, batch_idx):
        data, target = batch
        output = self(data)
        train_acc = self.train_acc(output, target)
        loss = torch.nn.functional.nll_loss(output, target)
        values = {'loss': loss,
                  'train_acc': train_acc}
        self.log_dict(values, on_step=False,
                      on_epoch=True, batch_size=BATCH_SIZE)
        return values

    def training_epoch_end(self, outputs):
        avg_loss = torch.stack([x['loss'] for x in outputs]).mean()
        avg_acc = torch.stack([x['train_acc'] for x in outputs]).mean()
        train_losses.append(avg_loss.item())
        train_acc.append(avg_acc.item())

    def validation_step(self, batch, batch_idx):
        data, target = batch
        output = self(data)
        val_acc = self.val_acc(output, target)
        loss = torch.nn.functional.nll_loss(output, target)
        values = {'loss': loss,
                  'val_acc': val_acc}
        self.log_dict(values, on_step=False,
                      on_epoch=True, batch_size=BATCH_SIZE)
        return values

    def validation_epoch_end(self, outputs):
        avg_loss = torch.stack([x['loss'] for x in outputs]).mean()
        avg_acc = torch.stack([x['val_acc'] for x in outputs]).mean()
        val_losses.append(avg_loss.item())
        val_acc.append(avg_acc.item())

    def test_step(self, batch, batch_idx):
        data, target = batch
        output = self(data)
        test_acc = self.test_acc(output, target)
        loss = torch.nn.functional.nll_loss(output, target)
        values = {'loss': loss,
                  'test_acc': test_acc}
        self.log_dict(values, on_step=False,
                      on_epoch=True, batch_size=BATCH_SIZE)
        return values

    def test_epoch_end(self, outputs):
        avg_loss = torch.stack([x['loss'] for x in outputs]).mean()
        avg_acc = torch.stack([x['test_acc'] for x in outputs]).mean()
        test_losses.append(avg_loss.item())
        test_acc.append(avg_acc.item())


model = SNN(dt=dt)
trainer = pl.Trainer(gpus=1,
                     max_epochs=EPOCHS,
                     log_every_n_steps=1,
                     num_nodes=1,
                     num_sanity_val_steps=0,
                     callbacks=RichProgressBar())

start_time = time.time()
trainer.fit(model=model,
            train_dataloaders=train_loader,
            val_dataloaders=val_loader)
test_mode = True
trainer.test(model=model,
             dataloaders=test_loader)
runtime = time.time() - start_time
exp = '_test'
pr_folder = 'norse/nmnist/'
date = datetime.datetime.now()
filetype = '.png'
d = str(date.day) + "_" + str(date.month) + "_" + \
    str(date.year) + "_" + str(date.time())
d = d.split('.')[0]
scriptname = sys.argv[0].split('/')[-1]
scriptname = scriptname.split('.')[0]
tf_str = str(transform).replace(" ", "").splitlines(keepends=False)[1:-1]
try:
    tf_str[-1] = tf_str[-1].replace(",decay='exp'", "")
except:
    pass
opt = str(trainer.optimizers).replace(
    " ", "").strip().splitlines(keepends=False)[2:-1]
print(
    f"Train Accuracy:{train_acc[-1]:.2f}, Val Accuracy{val_acc[-1]:.2f}, Test Accuracy{test_acc[0]:.2f}")

DF = pd.DataFrame([train_acc, val_acc])
DF = DF.transpose()
DF.columns = ['Train', 'Test']
DF.to_csv(pr_folder + d + '_acc_' + scriptname +
          exp + '.dat', sep=' ', index_label='Epoch')


DF = pd.DataFrame([train_losses, val_losses])
DF = DF.transpose()
DF.columns = ['Train', 'Test']
DF.to_csv(pr_folder + d + '_loss_' + scriptname +
          exp + '.dat', sep=' ', index_label='Epoch')

filename_raw = 'norse_lif_results_raw.csv'
filename_pretty = 'norse_lif_results_pretty.csv'

fields = ['n',
          ' date',
          ' filename',
          ' device',
          ' runtime',
          ' accuracy (train, val, test)',
          ' loss (train, val, test)',
          ' epochs',
          ' batch_size',
          ' optimizer',
          ' layers, n_spikes',
          ' method',
          ' alpha',
          ' tau_mem_inv',
          ' tau_syn_inv',
          ' v_leak',
          ' v_reset',
          ' v_th',
          ' transform']

num_rows = 0
for r in open(filename_raw):
    num_rows += 1
if num_rows != 0:
    num_rows -= 1

rows = [[num_rows,
        d.replace('_', ' '),
        scriptname + '.py',
        DEVICE,
        runtime,
        (train_acc[-1], val_acc[-1], test_acc[0]),
        (train_losses[-1], val_losses[-1], test_losses[0]),
        EPOCHS,
        BATCH_SIZE,
        opt,
        layers,
        neuron_params.method,
        neuron_params.alpha.item(),
        neuron_params.tau_mem_inv.item(),
        neuron_params.tau_syn_inv.item(),
        neuron_params.v_leak.item(),
        neuron_params.v_reset.item(),
        neuron_params.v_th.item(),
        tf_str]]


with open(filename_raw, 'a') as csvfile:
    csvwriter = csv.writer(csvfile)
    if num_rows == 0:
        csvwriter.writerow(fields)
    csvwriter.writerows(rows)

with open(filename_raw) as fp:
    table = from_csv(fp)

f = open(filename_pretty, "w")
f.write(table.get_string())
f.close()
