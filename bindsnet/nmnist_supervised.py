from tonic import CachedDataset
import csv
import datetime
import gc
from re import S
import sys
from bindsnet.analysis.plotting import (plot_assignments,
                                        plot_performance,
                                        plot_spikes,
                                        plot_voltages,
                                        plot_weights,
                                        plot_input)
from bindsnet.evaluation import (all_activity,
                                 assign_labels,
                                 proportion_weighting)
from bindsnet.utils import get_square_assignments, get_square_weights
from custom_models import (DiehlAndCook2015, DiehlAndCook2015v2, L2_Network_LIF,
                           TwoLayerNetworkSimple, TestNet)
from numpy.lib.utils import source
import tonic
import torch
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
from bindsnet.network.monitors import Monitor
import time
from prettytable.prettytable import from_csv
plt.style.use('science')
gc.collect()
torch.cuda.empty_cache()

transform = tonic.transforms.Compose(
    [
        tonic.transforms.Downsample(time_factor=0.001),
        tonic.transforms.ToFrame(sensor_size=tonic.datasets.NMNIST.sensor_size,
                                 time_window=1,
                                 ),
        # tonic.transforms.Denoise(filter_time=10),
        # tonic.transforms.ToTimesurface(sensor_size=tonic.datasets.NMNIST.sensor_size,
        #    tau=2000,
        #    decay='exp'
        #    )
    ]
)
epochs = 100
BATCH_SIZE = 1
#NMNIST Train Samples: 70000, Test Samples 10000
train_subset_len = 10000
test_subset_len = 1000
first_saccade_only = True

trainset = tonic.datasets.NMNIST(save_to='../data',
                                 transform=transform,
                                 first_saccade_only=first_saccade_only,
                                 train=True)

testset = tonic.datasets.NMNIST(save_to='../data',
                                transform=transform,
                                first_saccade_only=first_saccade_only,
                                train=False)

#Frames
cached_trainset = CachedDataset(
    trainset, cache_path='../../data/cached_datasets/nmnist_TF_tw1_train')
cached_testset = CachedDataset(
    testset, cache_path='../../data/cached_datasets/nmnist_TF_tw1_test')

train_subset = torch.utils.data.random_split(
    cached_trainset, [train_subset_len, len(trainset)-train_subset_len])[0]
test_subset = torch.utils.data.random_split(
    cached_testset, [test_subset_len, len(testset)-test_subset_len])[0]

train_len = -(-train_subset_len//BATCH_SIZE)
test_len = -(-test_subset_len//BATCH_SIZE)
#Dataloader
train_loader = torch.utils.data.DataLoader(
    dataset=train_subset,
    batch_size=BATCH_SIZE,
    collate_fn=tonic.collation.PadTensors(),
    shuffle=True,
)

test_loader = torch.utils.data.DataLoader(
    dataset=test_subset,
    batch_size=BATCH_SIZE,
    collate_fn=tonic.collation.PadTensors()
)


dt = 1
t = None
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
sensor_size = tonic.datasets.NMNIST.sensor_size
input_size = np.product(tonic.datasets.NMNIST.sensor_size)
n_classes = len(trainset.classes)
exc = 22.5
inh = 120
nu = [1e-4, 1e-2]
n_neurons = 100
wmin = 0.0
wmax = 1.0
norm = input_size*0.2
threshold = -52.0
plot = False

# network = TwoLayerNetworkSimple(
#     n_inpt=input_size,
#     shape=(2, 34, 34),
#     n_neurons=n_neurons,
#     dt=dt,
#     wmin=wmin,
#     wmax=wmax,
#     nu=nu,
#     norm=norm,
#     reduction=None
# )

# network = DiehlAndCook2015(
#     n_inpt=input_size,
#     inpt_shape=(2, 34, 34),
#     n_neurons=n_neurons,
#     dt=dt,
#     wmin=wmin,
#     wmax=wmax,
#     nu=nu,
#     norm=norm,
#     inh=inh,
#     exc=exc,
#     theta_plus=0.05,
#     tc_theta_decay=1e7
# )

# network = TestNet(
#     dt=dt,
#     n_inpt=input_size,
#     shape=(2, 34, 34),
#     n_neurons_output=n_classes,
#     n_neurons_hidden=n_neurons,
#     batch_size=BATCH_SIZE
# )

network = L2_Network_LIF(
        n_inpt=input_size,
        inpt_shape=(2, 34, 34),
        n_neurons=n_neurons,
        n_neurons_output=n_neurons,
        batch_size=BATCH_SIZE,
        dt=dt,
        wmin=wmin,
        wmax=wmax,
        nu=nu,
        norm=norm,
        inh=inh,
        exc=exc
)

spikes = {}


for layer in set(network.layers):
    spikes[layer] = Monitor(
        network.layers[layer], state_vars=["s"], time=t, device=device
    )
    network.add_monitor(spikes[layer], name="%s_spikes" % layer)

voltages = {}
for layer in set(network.layers) - {"X"}:
    voltages[layer] = Monitor(
        network.layers[layer], state_vars=["v"], time=t, device=device
    )
network.add_monitor(voltages[layer], name="%s_voltages" % layer)

network.to(device)

assignments = -torch.ones(n_neurons, device=device)
proportions = torch.zeros((n_neurons, n_classes), device=device)
rates = torch.zeros((n_neurons, n_classes), device=device)
per_class = int(n_neurons / n_classes)


acc_batch_train = []
acc_batch_test = []
acc_train = []
acc_test = []
n_sqrt = int(np.ceil(np.sqrt(n_neurons)))
weights_im = None
assigns_im = None
perf_ax = None
inpt_axes = None
inpt_ims = None
acc = {"train": [], "test": []}
spike_ims, spike_axes = None, None
voltage_axes, voltage_ims = None, None


start_time = time.time()
pbar = tqdm(total=epochs, colour='blue')
for epoch in range(epochs):
    #Training
    network.train(mode=True)
    correct_train = 0
    correct_test = 0
    for events, targets in tqdm(
        train_loader, colour='green', leave=False, total=train_len):
        t = int(events.shape[0])
        spike_record = torch.zeros((1, t, n_neurons), device=device)
        input = {"X": events.to(device)}
        choice = np.random.choice(int(n_neurons / n_classes), size=1, replace=False)
        clamp = {"Y": per_class * targets.long() + torch.Tensor(choice).long()}
        network.run(inputs=input, time=t, clamp=clamp)
        
        spike_record = spikes["Y"].get("s").permute(1, 0, 2)

        all_activity_pred = all_activity(
            spikes=spike_record.to(device),
            assignments=assignments,
            n_labels=n_classes)

        proportion_pred = proportion_weighting(
            spikes=spike_record.to(device),
            assignments=assignments,
            proportions=proportions,
            n_labels=n_classes)
        
        correct_train += torch.sum(targets.long().to(device) ==
                                all_activity_pred).item()

        assignments, proportions, rates = assign_labels(
            spikes=spike_record.to(device),
            labels=targets.to(device),
            n_labels=n_classes,
            rates=rates)


        if plot:
            # image = events[0, 0, 0, :].view((34, 34))
            # inpt = input["X"].view(time, input_size).sum(
            #     0).view(sensor_size)
            input_exc_weights = network.connections[("X", "Y")].w

            square_weights = get_square_weights(
                weights=input_exc_weights.view(input_size, n_neurons), n_sqrt=n_sqrt, side=(68, 34))
            square_assignments = get_square_assignments(
                assignments=assignments, n_sqrt=n_sqrt)

            # Plots
            # inpt_axes, inpt_ims = plot_input(
            # batch,
            # inpt,
            # label=labels[idx],
            # axes=inpt_axes,
            # ims=inpt_ims)
            weights_im = plot_weights(square_weights, im=weights_im)
            # weights_im = plot_weights(input_exc_weights, im=weights_im)
            assigns_im = plot_assignments(
                square_assignments, im=assigns_im, classes=trainset.classes)
            # voltage_ims, voltage_axes = plot_voltages(
            #     voltages, ims=voltage_ims, axes=voltage_axes, plot_type="line"
            # )
            # spike_ims, spike_axes = plot_spikes(
            #     spikes, ims=spike_ims, axes=spike_axes)
            # perf_ax = plot_performance(acc_print, x_scale=time, ax=perf_ax)
            plt.pause(1e-24)
            network.reset_state_variables()
    acc["train"].append(correct_train/len(train_loader.dataset))

    #Testing
    network.train(mode=False)
    for events, targets in tqdm(test_loader, leave=False, colour='red', total=test_len):
        t = int(events.shape[0])
        spike_record = torch.zeros((1, t, n_neurons), device=device)

        input = {"X": events.to(device)}
        network.run(inputs=input, time=t, input_time_dim=1)

        spike_record = spikes["Y"].get("s").permute(1, 0, 2)
        all_activity_pred = all_activity(
            spikes=spike_record.to(device),
            assignments=assignments,
            n_labels=n_classes)

        proportion_pred = proportion_weighting(
            spikes=spike_record.to(device),
            assignments=assignments,
            proportions=proportions,
            n_labels=n_classes)

        correct_test += torch.sum(targets.long().to(device) ==
                                  all_activity_pred).item()

        network.reset_state_variables()
    acc["test"].append(correct_test/len(test_loader.dataset))
    pbar.set_description(
        "Acc: " + ' ' + str(acc["train"][-1]) + ' ' + str(acc["test"][-1]))
    pbar.update()

runtime = time.time() - start_time

date = datetime.datetime.now()
filetype = '.png'
d = str(date.day) + "_" + str(date.month) + "_" + \
    str(date.year) + "_" + str(date.time())
d = d.split('.')[0]
scriptname = sys.argv[0].split('.')[0]
tf_str = str(transform).replace(" ", "").splitlines(keepends=False)[1:-1]
plot_path = "./plots/" + scriptname + "/acc" + "_" + d + filetype
np.save("./plots/" + scriptname + "/acc_data" +
        "_" + d + '.npy', [acc["train"], acc["test"]])
final_acc = {"train": acc["train"], "test": acc["test"]}
network_str = str(network).replace("("," ").split(' ')[0]
plot_performance(performances=final_acc, save=plot_path)
filename_raw = 'nmnist_results_raw.csv'
filename_pretty = 'nmnist_results_pretty.csv'
try:
    tf_str[1] = tf_str[1].replace(",decay='exp'", "")
except:
    pass
fields = ['n',
          ' date',
          ' filename',
          ' device',
          ' runtime',
          ' accuracy (train, test)',
          ' samples (train, test)',
          ' network',
          ' epochs',
          ' batch_size',
          ' nu',
          ' layers',
          ' first saccade only',
          ' exc',
          ' inh',
          ' wmin',
          ' wmax',
          ' norm',
          ' transform']

num_rows = 0
for r in open(filename_raw):
    num_rows += 1
if num_rows != 0:
    num_rows -= 1

rows = [[num_rows,
        d.replace('_', ' '),
        sys.argv[0].split('.')[0] + '.py',
        device,
        runtime,
        (acc["train"][-1], acc["test"][-1]),
        (len(train_loader.dataset), len(test_loader.dataset)),
        network_str,
        epochs,
        BATCH_SIZE,
        nu,
        n_neurons,
        first_saccade_only,
        exc,
        inh,
        wmin,
        wmax,
        norm,
        tf_str]]


with open(filename_raw, 'a') as csvfile:
    csvwriter = csv.writer(csvfile)
    if num_rows == 0:
        csvwriter.writerow(fields)
    csvwriter.writerows(rows)

with open(filename_raw) as fp:
    table = from_csv(fp)

f = open(filename_pretty, "w")
f.write(table.get_string())
f.close()
