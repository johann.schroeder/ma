import gc
from bindsnet.analysis.plotting import plot_assignments, plot_performance, plot_spikes, plot_voltages, plot_weights, plot_input
from custom_models import (DiehlAndCook2015, 
                                    ThreeLayerNetwork, 
                                    TwoLayerLIFReward, 
                                    TwoLayerNetwork, 
                                    TwoLayerNetworkIzhikevich)
from bindsnet.network import network
from bindsnet.utils import get_square_assignments, get_square_weights
import tonic
import torch
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
from bindsnet.evaluation import all_activity, assign_labels, proportion_weighting
from bindsnet.network.monitors import Monitor
from datetime import datetime
plt.style.use('science')
gc.collect()
torch.cuda.empty_cache()

transform = tonic.transforms.Compose(
    [
        # tonic.transforms.Denoise(filter_time=20),
        tonic.transforms.Downsample(time_factor=0.001),  
        tonic.transforms.ToSparseTensor(merge_polarities=True),
    ]
)

download = False
batch_size = 8  # POKERDVS

trainset = tonic.datasets.POKERDVS(
    save_to='../data', download=download, transform=transform, train=True)
testset = tonic.datasets.POKERDVS(
    save_to='../data', download=download, transform=transform, train=False)

train_loader = torch.utils.data.DataLoader(dataset=trainset,
                                           batch_size=batch_size,
                                           collate_fn=tonic.utils.pad_tensors,
                                           shuffle=True,
                                           )

test_loader = torch.utils.data.DataLoader(dataset=testset,
                                          batch_size=batch_size,
                                          collate_fn=tonic.utils.pad_tensors,
                                          shuffle=True
                                          )

dt = 1
time = None
device = torch.device("cuda")
sensor_size = tonic.datasets.POKERDVS.sensor_size
input_size = np.product(sensor_size)
n_classes = len(trainset.classes)
n_neurons_hidden = input_size
n_neurons_out = 16
wmin = 0.0
wmax = 1.0
exc = 0.3
inh = 17.5
# nu = (0.01, 0.001) #DiehlCook2015
nu = (1e-4, 1e-2)
norm = input_size*0.2
# norm = 122.5
threshold = -52.0
rest = -65.0
reset = -65.0
refrac = 5
tc_trace = 20.0
tc_decay = 100.0
theta_plus = 0.5
epochs = 25
plot = True

DiehlCook2015 = DiehlAndCook2015(
    n_inpt=input_size,
    n_neurons=n_neurons_out,
    dt=dt,
    wmin=wmin,
    wmax=wmax,
    nu=nu,
    norm=norm,
    inh=inh,
    exc=exc,
    theta_plus=theta_plus,
    reduction=None
)

TwoLayerLIFR = TwoLayerLIFReward(
    n_inpt=input_size,
    n_neurons=n_neurons_out,
    dt=dt,
    wmin=wmin,
    wmax=wmax,
    nu=nu,
    norm=norm
)

ThreeLayerLIF = ThreeLayerNetwork(
    n_inpt=input_size,
    n_neurons_hidden=n_neurons_hidden,
    n_neurons_out=n_neurons_out,
    dt=dt,
    wmin=wmin,
    wmax=wmax,
    nu=nu,
    norm=norm,
    tc_trace=tc_trace,
    tc_decay=tc_decay,
    threshold=threshold,
    rest=rest,
    reset=reset,
    refrac=refrac,
    batch_size=batch_size,
    reduction=None,
    exc=exc
)

TwoLayerLIF = TwoLayerNetwork(
    n_inpt=input_size,
    n_neurons=n_neurons_out,
    dt=dt,
    wmin=wmin,
    wmax=wmax,
    nu=nu,
    norm=norm,
    tc_trace=tc_trace,
    tc_decay=tc_decay, 
    threshold=threshold,
    rest=rest,
    reset=reset,
    refrac=refrac,
    batch_size=batch_size,
    reduction=None,
    exc=exc
)

TwoLayerIzhikevich = TwoLayerNetworkIzhikevich(
    n_inpt=input_size,
    n_neurons=n_neurons_out,
    dt=dt,
    wmin=wmin,
    wmax=wmax,
    nu=nu,
    norm=norm
)

def run_net(epochs,
            batch_size,
            device,
            network,
            train_loader,
            test_loader,
            n_neurons_hidden,
            n_neurons_out,
            n_classes,
            time):
    
    source_monitor = Monitor(
        obj=network.layers["X"],
        state_vars=["s"],
        time=time,
        device=device
    )

    target_monitor = Monitor(
        obj=network.layers["Y"],
        state_vars=["s", "v"],
        time=time,
        device=device
    )

    network.add_monitor(monitor=source_monitor, name="X")
    network.add_monitor(monitor=target_monitor, name="Y")

    network.to(device)
    
    assignments = -torch.ones_like(torch.Tensor(n_neurons_out), device=device)
    proportions = torch.zeros_like(torch.Tensor(n_neurons_out, n_classes), device=device)
    rates = torch.zeros_like(torch.Tensor(n_neurons_out, n_classes), device=device)
    per_class = int(n_neurons_out / n_classes)
    accuracy_train = {"all": [], "proportion": []}
    acc_batch_train = []
    acc_batch_test = []
    acc_train = []
    acc_test = []
    n_sqrt = int(np.ceil(np.sqrt(n_neurons_out)))
    weights_im = None
    assigns_im = None
    perf_ax = None
    inpt_axes = None
    inpt_ims = None
    acc_print = {"train": [], "test": []}
    
    #Training
    pbar = tqdm(total=epochs, colour='green')
    for epoch in range(epochs):
        network.train()
        for events, targets in (train_loader):
            time = int(events.shape[1])
            b = int(events.shape[0])
            labels = torch.empty(b, device=device)
            spike_record = torch.zeros(b, time, n_neurons_out, device=device)
            for idx, batch in enumerate(events):
                event = batch
                all_activity_pred = all_activity(
                    spikes=spike_record.to(device),
                    assignments=assignments,
                    n_labels=n_classes)

                proportion_pred = proportion_weighting(
                    spikes=spike_record.to(device),
                    assignments=assignments,
                    proportions=proportions,
                    n_labels=n_classes)

                accuracy_train["all"].append(
                    100 * torch.sum(labels.long().to(device) ==
                                    all_activity_pred).item() / b)

                accuracy_train["proportion"].append(
                    100 * torch.sum(labels.long().to(device) ==
                                    proportion_pred).item() / b)
                
                assignments, proportions, rates = assign_labels(
                    spikes=spike_record,
                    labels=labels.to(device),
                    n_labels=n_classes,
                    rates=rates)

                batch = batch.to_dense().squeeze().view(
                    [time, -1]).to(device)
                input = {"X": batch}
                labels[idx] = targets[idx].item()
                choice = np.random.choice(int(n_neurons_out / n_classes), size=1, replace=False)
                clamp = {"Y": per_class *
                         targets[idx].long() + torch.Tensor(choice).long()}
                network.run(inputs=input, time=time, clamp=clamp)
                spikes = {"X": source_monitor.get("s"), "Y": target_monitor.get("s")}
                voltages = {"X": target_monitor.get("v")}

                spike_record[idx] = spikes["Y"].view(time, n_neurons_out)
                
                if plot:
                    inpt = input["X"].view(time, input_size).sum(0).view(sensor_size)
                    input_exc_weights = network.connections[("H", "Y")].w
                    square_weights = get_square_weights(
                        weights=input_exc_weights.view(n_neurons_hidden, n_neurons_out), n_sqrt=n_sqrt, side=35)
                    square_assignments = get_square_assignments(assignments=assignments, n_sqrt=n_sqrt)
                    
                    # Plots
                    # inpt_axes, inpt_ims = plot_input(
                        # batch, 
                        # inpt, 
                        # label=labels[idx], 
                        # axes=inpt_axes,
                        # ims=inpt_ims)
                    weights_im = plot_weights(square_weights, im=weights_im)
                    # assigns_im = plot_assignments(square_assignments, im=assigns_im, classes=trainset.classes)
                    # perf_ax = plot_performance(acc_print, x_scale=time, ax=perf_ax)
                    plt.pause(1e-20)
                
                network.reset_state_variables()
            acc_batch_train.append(accuracy_train["all"][-1])
            acc_print["train"].append(accuracy_train["all"][-1])
        acc_train.append(np.mean(acc_batch_train))
        
        #Testing
        network.train(mode=False)
        for events, targets in (test_loader):
            accuracy_test = {"all": 0, "proportion": 0}
            time = int(events.shape[1])
            b = int(events.shape[0])
            spike_record = torch.zeros(1, int(time/dt), n_neurons_out, device=device)
            for idx, batch in enumerate(events):
                inputs = {"X": batch.to_dense().view(int(time/dt), 1, input_size).to(device)}
                inputs = {k: v.cuda() for k, v in inputs.items()}
                network.run(inputs=inputs, time=time, input_time_dim=1)
                
                spikes = {"X": source_monitor.get("s"), "Y": target_monitor.get("s")}
                voltages = {"X": target_monitor.get("v")}
                spike_record[0] = spikes["Y"].squeeze()
                label_tensor = torch.tensor(targets[idx], device=device)
                
                all_activity_pred = all_activity(
                    spikes=spike_record,
                    assignments=assignments,
                    n_labels=n_classes)

                proportion_pred = proportion_weighting(
                    spikes=spike_record,
                    assignments=assignments,
                    proportions=proportions,
                    n_labels=n_classes)
                
                accuracy_test["all"] += float(torch.sum(label_tensor.long()
                                            == all_activity_pred).item())
                
                # print("Assignments: ", assignments)
                # print(targets, "Targets")
                # print(targets[idx].long().item(), "Lables")
                # print(all_activity_pred.item(), "All activity prediction")
                # print(100*(accuracy_test["all"]/b))

                # Plots per datum
                # plt.ioff()
                # plot_spikes(spikes)
                # plot_voltages(voltages, plot_type="line")
                # plt.show()
                
                # print(100*accuracy_test["all"]/b)
                network.reset_state_variables()
            acc_batch_test.append(100*(accuracy_test["all"]/b))
            acc_print["test"].append(100*(accuracy_test["all"]/b))
        acc_test.append(np.mean(acc_batch_test))
        pbar.set_description_str("Av. Train Acc: [%.2f], Av. Test Acc: [%.2f] " % (
            np.mean(acc_train), np.mean(acc_test)))
        pbar.update()

    return acc_train, acc_test


print("Params: norm: [%.2f], nu: ([%1.2E], [%1.2E]), thres: [%.1fmV], exc: [%.1f]\n" % (
            norm, nu[0], nu[1], threshold, exc))
train_acc, test_acc = run_net(
            epochs=epochs,
            batch_size=batch_size,
            device=device,
            network=ThreeLayerLIF,
            train_loader=train_loader,
            test_loader=test_loader,
            n_neurons_hidden=n_neurons_hidden,
            n_neurons_out=n_neurons_out,
            n_classes=n_classes,
            time=time)
print("Final Train Acc: [%.2f], Final Test Acc: [%.2f]\n" %
      (train_acc[-1], test_acc[-1]))

