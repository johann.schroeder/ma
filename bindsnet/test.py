import csv
import datetime
import gc
import sys
from bindsnet.network import Network
from bindsnet.analysis.plotting import (plot_assignments,
                                        plot_performance,
                                        plot_spikes,
                                        plot_voltages,
                                        plot_weights,
                                        plot_input)
from bindsnet.evaluation import (all_activity,
                                 assign_labels,
                                 proportion_weighting)
from bindsnet.evaluation.evaluation import logreg_fit, logreg_predict
from bindsnet.network.nodes import Input, LIFNodes
from bindsnet.network.topology import Connection
from bindsnet.utils import get_square_assignments, get_square_weights
from matplotlib import cm
from tensorflow.python.ops.gen_clustering_ops import nearest_neighbors
from custom_models import (DiehlCook_Readout, Izhi_Readout, LIF_Readout, Test, TestNet, TwoLayerConvNetwork_LIF) 
from numpy.lib.utils import source
import tonic
import torch
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
from bindsnet.network.monitors import Monitor
import torchvision as tv
import torchvision.transforms.functional as F
import time
from sklearn.linear_model import LogisticRegression 
from prettytable.prettytable import from_csv
from bindsnet.analysis.pipeline_analysis import TensorboardAnalyzer
plt.style.use('science')
gc.collect()
torch.cuda.empty_cache()

transform = tonic.transforms.Compose(
    [
        tonic.transforms.Downsample(time_factor=0.001),  
        # tonic.transforms.Denoise(filter_time=1),
        # tonic.transforms.ToFrame(sensor_size=tonic.datasets.POKERDVS.sensor_size,
                                #  time_window=1,
                                #   event_count=5),
        tonic.transforms.ToVoxelGrid(sensor_size=tonic.datasets.POKERDVS.sensor_size,
                                     n_time_bins=100
                                )
    ]
)

# transform = tonic.transforms.Compose(
#     [
#         # tonic.transforms.Downsample(time_factor=0.001),
#         # tonic.transforms.Denoise(filter_time=1),
#         tonic.transforms.ToVoxelGrid(sensor_size=tonic.datasets.POKERDVS.sensor_size,
#                                  #  time_window=1,
#                                 #  event_count=5,
#                                 n_time_bins=100
#                                  )
#     ]
# )



epochs = 25
BATCH_SIZE = 8
#NMNIST Train Samples: 70000, Test Samples 10000
train_subset_len = 48
test_subset_len = 20
first_saccade_only = True

trainset = tonic.datasets.POKERDVS(save_to='../data',
                                 transform=transform,
                                #  first_saccade_only=first_saccade_only,
                                 train=True)

testset = tonic.datasets.POKERDVS(save_to='../data',
                                transform=transform,
                                # first_saccade_only=first_saccade_only,
                                train=False)

train_subset = torch.utils.data.random_split(
    trainset, [train_subset_len, len(trainset)-train_subset_len])[0]
test_subset = torch.utils.data.random_split(
    testset, [test_subset_len, len(testset)-test_subset_len])[0]

train_len = -(-train_subset_len//BATCH_SIZE)
test_len = -(-test_subset_len//BATCH_SIZE)

#Dataloader
train_loader = torch.utils.data.DataLoader(
    dataset=train_subset,
    batch_size=BATCH_SIZE,
    collate_fn=tonic.collation.PadTensors(),
    shuffle=True,
)

test_loader = torch.utils.data.DataLoader(
    dataset=test_subset,
    batch_size=BATCH_SIZE,
    collate_fn=tonic.collation.PadTensors(),
    shuffle=True
)


dt = 1
t = None
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
# input_size = np.product(tonic.datasets.POKERDVS.sensor_size) #ToFrame
input_size = (35*35) #VoxelGrid
n_classes = len(trainset.classes)
exc = 22.5
inh = 120
nu = (0.0001, 0.01)
n_neurons = 100
wmin = 0.0
wmax = 1.0
norm = input_size*0.15
threshold = -52.0
plot = True

# network = LIF_Readout(
#     n_inpt=input_size,
#     n_neurons=n_classes,
#     dt=dt,
#     wmin=wmin,
#     wmax=wmax,
#     nu=nu,
#     norm=norm,
#     exc=exc,
#     inh=inh
# )


network = TestNet(
    dt=dt,
    n_inpt=input_size,
    # shape=(2, 35, 35),
    n_neurons_output=n_classes,
    n_neurons_hidden=n_neurons,
    batch_size=BATCH_SIZE
)

# network = DiehlCook_Readout(
#     n_inpt=input_size,
#     # shape=(2, 34, 34),
#     n_neurons=n_classes,
#     dt=dt,
#     wmin=wmin,
#     wmax=wmax,
#     nu=nu,
#     norm=norm,
#     inh=inh,
#     exc=exc,
#     theta_plus=0.05,
#     tc_theta_decay=1e7
# )

# network = Test()
spikes = {}

source_monitor = Monitor(
    obj=network.layers["X"],
    state_vars=("s"),
    time=t,
    device=device
)
network.add_monitor(source_monitor, name="X")

hidden_monitor = Monitor(
    obj=network.layers["H"],
    state_vars=("s"),
    time=t,
    device=device
)
network.add_monitor(hidden_monitor, name="H")

output_monitor = Monitor(
    obj=network.layers["Y"],
    state_vars=("s"),
    time=t,
    device=device
)
network.add_monitor(output_monitor, name="Y")

# for layer in set(network.layers):
#     spikes[layer] = Monitor(
#         network.layers[layer], state_vars=["s"], time=t, device=device
#     )
#     network.add_monitor(spikes[layer], name="%s_spikes" % layer)

# voltages = {}
# for layer in set(network.layers) - {"X"}:
#     voltages[layer] = Monitor(
#         network.layers[layer], state_vars=["v"], time=t, device=device
#     )
# network.add_monitor(voltages[layer], name="%s_voltages" % layer)

network.to(device)

assignments = -torch.ones(n_classes, device=device)
proportions = torch.zeros((n_classes, n_classes), device=device)
rates = torch.zeros((n_classes, n_classes), device=device)
per_class = int(n_classes / n_classes)


acc_batch_train = []
acc_batch_test = []
acc_train = []
acc_test = []
n_sqrt = int(np.ceil(np.sqrt(n_neurons)))
weights_im_input_hidden = None
weights_im_hidden_output = None
weights_im_out = None
weights_im_hidden = None
assigns_im = None
perf_ax = None
inpt_axes = None
inpt_ims = None
acc = {"train": [], "test": []}
logreg_acc = {"train": [], "test": []}
spike_ims, spike_axes = None, None
voltage_axes, voltage_ims = None, None
logreg = LogisticRegression()
start_time = time.time()
pbar = tqdm(total=epochs, colour='blue')
for epoch in range(epochs):
    #Training
    network.train(mode=True)
    correct_train = 0
    correct_test = 0
    logreg_correct_train = 0
    logreg_correct_test = 0
    for idx, (events, targets) in enumerate(
            tqdm(train_loader, colour='green', leave=False, total=train_len)):
        t = int(events.shape[0])
        b = int(events.shape[1])
        spike_record = torch.zeros((b, t, n_classes), device=device)
        input = {"X": events.view(t, b, -1).to(device)}
        network.run(inputs=input, time=t, input_time_dim=1, one_step=True)
        # network.run(inputs=input, time=t, one_step=True, reward=0.01)
        
        spikes = {
            "X": source_monitor.get("s"),
            "H": hidden_monitor.get("s"),
            "Y": output_monitor.get("s"),
        }        
        
        spike_record = spikes["Y"].permute((1, 0, 2))        
        all_activity_pred = all_activity(
            spikes=spike_record.to(device),
            assignments=assignments,
            n_labels=n_classes)

        proportion_pred = proportion_weighting(
            spikes=spike_record.to(device),
            assignments=assignments,
            proportions=proportions,
            n_labels=n_classes)
        
        s = spikes["Y"]
        s, _ = torch.max(s, 0)
        s = s.float()
        log_p_y = torch.nn.functional.log_softmax(s, dim=1)
        pred = log_p_y.argmax(dim=1, keepdim=True)
        
        correct_train += torch.sum(targets.long().to(device) ==
                                   all_activity_pred).item()

        # print(all_activity_pred)
        # print(targets)
        summed_spikes = spike_record.sum(dim=1)
        logreg_pred = logreg_predict(summed_spikes, logreg)
        logreg = logreg_fit(spikes=summed_spikes, labels=targets, logreg=logreg)
        logreg_correct_train += torch.sum(targets.long().to(device) ==
                                   logreg_pred.to(device)).item()

        assignments, proportions, rates = assign_labels(
            spikes=spike_record.to(device),
            labels=targets.to(device),
            n_labels=n_classes,
            rates=rates)
        # choice = np.random.choice(
        #     int(n_neurons / n_classes), size=1, replace=False)
        # clamp = {"Y": per_class *
        #             targets[idx].long() + torch.Tensor(choice).long()}

        if plot:
            # inpt = input["X"].view(time, input_size).sum(
                # 0).view(sensor_size)
            input_hidden_weights = network.connections[("X", "H")].w
            hidden_output_weights = network.connections[("H", "Y")].w
            square_weights_input_hidden = get_square_weights(
                weights=input_hidden_weights.view(input_size, n_neurons), n_sqrt=4, side=(35,35))
            square_weights_hidden_output = get_square_weights(
                weights=hidden_output_weights, n_sqrt=4, side=10)
            # square_assignments = get_square_assignments(
            #     assignments=assignments, n_sqrt=n_sqrt)
            # Plotss
           
            weights_im_input_hidden = plot_weights(square_weights_input_hidden, 
                                                   im=weights_im_input_hidden)
            
            weights_im_hidden_output = plot_weights(square_weights_hidden_output,
                                                   im=weights_im_hidden_output)
            # weights_im_hidden = plot_weights(
            #     network.connections["X", "H"].w, im=weights_im_hidden)
            # weights_im_out = plot_weights(
            #     network.connections["H", "Y"].w, im=weights_im_out)

            # assigns_im = plot_assignments(
            #     square_assignments, im=assigns_im, classes=trainset.classes)

            # voltage_ims, voltage_axes = plot_voltages(
            #     voltages, ims=voltage_ims, axes=voltage_axes, plot_type="line"
            # )
            spike_ims, spike_axes = plot_spikes(
                spikes, ims=spike_ims, axes=spike_axes)
            # perf_ax = plot_performance(acc_print, x_scale=time, ax=perf_ax)
            plt.pause(1e-24)
        network.reset_state_variables()
    acc["train"].append(correct_train/len(train_loader.dataset))
    logreg_acc["train"].append(logreg_correct_train/len(train_loader.dataset))

    #Testing
    network.train(mode=False)
    for events, targets in tqdm(test_loader, leave=False, colour='red', total=test_len):
        b = int(events.shape[1])
        t = int(events.shape[0])
        spike_record = torch.zeros((b, t, n_classes), device=device)

        input = {"X": events.view(t, b, -1).to(device)}
        network.run(inputs=input, time=t, input_time_dim=1)
        
        spikes = {
            "X": source_monitor.get("s"),
            "H": hidden_monitor.get("s"),
            "Y": output_monitor.get("s"),
        }
        
        spike_record = spikes["Y"].permute((1, 0, 2))

        all_activity_pred = all_activity(
            spikes=spike_record.to(device),
            assignments=assignments,
            n_labels=n_classes)

        proportion_pred = proportion_weighting(
            spikes=spike_record.to(device),
            assignments=assignments,
            proportions=proportions,
            n_labels=n_classes)

        summed_spikes = spike_record.sum(dim=1)
        correct_test += torch.sum(targets.long().to(device) ==
                                  all_activity_pred.to(device)).item()

        logreg_pred = logreg_predict(summed_spikes, logreg)
        logreg_correct_test += torch.sum(targets.long().to(device) ==
                                          logreg_pred.to(device)).item()
        network.reset_state_variables()
    
    acc["test"].append(correct_test/len(test_loader.dataset))
    logreg_acc["test"].append(logreg_correct_test/len(test_loader.dataset))
    pbar.update()

runtime = time.time() - start_time
date = datetime.datetime.now()
filetype = '.png'
d = str(date.day) + "_" + str(date.month) + "_" + \
    str(date.year) + "_" + str(date.time())
d = d.split('.')[0]
scriptname = sys.argv[0].split('.')[0]
tf_str = str(transform).replace(" ", "").splitlines(keepends=False)[1:-1]
plot_path = "./plots/" + scriptname + "/acc" + "_" + d + filetype
np.save("./plots/" + scriptname + "/acc_data" +
        "_" + d + '.npy', [acc["train"], acc["test"]])
final_acc = {"train": acc["train"], "test": acc["test"]}
network_str = str(network).replace("(", " ").split(' ')[0]
plot_performance(performances=final_acc, save=plot_path)

filename_raw = 'nmnist_results_raw.csv'
filename_pretty = 'nmnist_results_pretty.csv'

fields = ['n',
          ' date',
          ' filename',
          ' device',
          ' runtime',
          ' accuracy (train, test)',
          ' samples (train, test)',
          ' network',
          ' epochs',
          ' batch_size',
          ' nu',
          ' layers',
          ' first saccade only',
          ' exc',
          ' inh',
          ' wmin',
          ' wmax',
          ' norm',
          ' transform']

num_rows = 0
for r in open(filename_raw):
    num_rows += 1
if num_rows != 0:
    num_rows -= 1

rows = [[num_rows,
        d.replace('_', ' '),
        sys.argv[0].split('.')[0] + '.py',
        device,
        runtime,
        [acc["train"][-1], acc["test"][-1], logreg_acc["train"][-1], logreg_acc["test"][-1]],
        (len(train_loader.dataset), len(test_loader.dataset)),
        network_str,
        epochs,
        BATCH_SIZE,
        nu,
        n_neurons,
        first_saccade_only,
        exc,
        inh,
        wmin,
        wmax,
        norm,
        tf_str]]


with open(filename_raw, 'a') as csvfile:
    csvwriter = csv.writer(csvfile)
    if num_rows == 0:
        csvwriter.writerow(fields)
    csvwriter.writerows(rows)

with open(filename_raw) as fp:
    table = from_csv(fp)

f = open(filename_pretty, "w")
f.write(table.get_string())
f.close()
