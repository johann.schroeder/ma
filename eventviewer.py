##dataset shenanigans
import tonic.transforms as transforms
import matplotlib.pyplot as plt
import tonic
import torch
import numpy as np
import torchvision as tv


gesture_sensor_size = tonic.datasets.DVSGesture.sensor_size
nmnist_sensor_size = tonic.datasets.NMNIST.sensor_size
poker_sensor_size = tonic.datasets.POKERDVS.sensor_size
shd_sensor_size = tonic.datasets.SHD.sensor_size

time_surface_transform = transforms.Compose(
    [
        tonic.transforms.Denoise(filter_time=10),
        tonic.transforms.ToTimesurface(
            sensor_size=poker_sensor_size,
            surface_dimensions=[15, 15],
            tau=5000,
            decay="exp"
        ),
    ]
)

frame_transform_poker = transforms.ToFrame(time_window=1, sensor_size=poker_sensor_size)
frame_transform_nmnist = transforms.ToFrame(time_window=1, sensor_size=nmnist_sensor_size)
frame_transform_gesture = transforms.ToFrame(time_window=1, sensor_size=gesture_sensor_size)
frame_transform_shd = transforms.ToFrame(time_window=1, sensor_size=shd_sensor_size)

denoise_transform = transforms.Denoise(filter_time=10000)

padding_crop_transform = transforms.Compose(
    [
        tv.transforms.Pad(padding=(86, 86)),
        tv.transforms.CenterCrop(86)
    ]
)

data_transform = transforms.Compose(
    [
        transforms.ToFrame(n_event_bins=9, sensor_size=poker_sensor_size),
    ]
)

transform = tonic.transforms.Compose(
    [
        tonic.transforms.Downsample(time_factor=0.001),
        # tonic.transforms.Denoise(filter_time=1),
        tonic.transforms.ToFrame(sensor_size=tonic.datasets.DVSGesture.sensor_size,
                                  time_window=100,
                                #  event_count=100,
                                 include_incomplete=True)
    ]
)

center_crop = tv.transforms.CenterCrop(40)
to_tensor = tv.transforms.ToTensor()
random_crop = tonic.transforms.RandomCrop(target_size=[40, 40], sensor_size=gesture_sensor_size)

gesture_transform = tonic.transforms.Compose(
    [
        # tv.transforms.Resize(40),
        # tv.transforms.CenterCrop(64),
        tonic.transforms.Downsample(time_factor=0.001),
        tonic.transforms.Denoise(filter_time=10),
        tonic.transforms.ToFrame(sensor_size=gesture_sensor_size, 
                                #  time_window=1000,
                                 n_time_bins=3, 
                                 include_incomplete=True),
        # tv.transforms.Pad(40),
    ]
)

nmnist_downsample = tonic.transforms.Downsample(time_factor=1e-3)
nmnist_denoise = tonic.transforms.Denoise(filter_time=5)
nmnist_toframe = tonic.transforms.ToFrame(sensor_size=nmnist_sensor_size, time_window=75)

nmnist_transform = tonic.transforms.Compose(
    [ 
        tonic.transforms.Downsample(time_factor=0.001),  
        tonic.transforms.Denoise(filter_time=10),
        tonic.transforms.ToFrame(time_window=1,
                                 sensor_size=nmnist_sensor_size,
                                 include_incomplete=True),
        
    ]
)

downsample = tonic.transforms.Downsample(1e-3)

dvs_tts = tonic.transforms.Compose(
    [
        tonic.transforms.Denoise(filter_time=10),
        tonic.transforms.Downsample(time_factor=1e-3),
        tonic.transforms.ToTimesurface(sensor_size=tonic.datasets.DVSGesture.sensor_size,
           tau=2000,
           surface_dimensions=[85, 85],
           decay='exp'
           )

    ]
)

shd_transform = tonic.transforms.Compose(
    [
        # transforms.Downsample(time_factor=0.001),
        transforms.TimeSkew(coefficient=1e-3),
        transforms.NumpyAsType(float),
        # transforms.ToFrame(time_window=1, sensor_size=(700, 1))
    ]
)

gesture_train = tonic.datasets.DVSGesture(
    save_to='./data', train=True, transform=transform)

# gesture_train = tonic.datasets.DVSGesture(
#     save_to='./data', train=True, transform=frame_transform_gesture)
gesture_test = tonic.datasets.DVSGesture(
    save_to='./data', train=False, transform=transform)

poker_train = tonic.datasets.POKERDVS(
    save_to='./data', train=True, transform=frame_transform_poker)
poker_test = tonic.datasets.POKERDVS(
    save_to='./data', train=False, transform=frame_transform_poker)

nmnist_train = tonic.datasets.NMNIST(
    save_to='./data', train=True, transform=frame_transform_nmnist)
nmnist_test = tonic.datasets.NMNIST(
    save_to='./data', train=False, transform=frame_transform_nmnist)

shd_train = tonic.datasets.SHD(
    save_to='./data', train=True, transform=frame_transform_shd)
shd_test = tonic.datasets.SHD(
    save_to='./data', train=False, transform=frame_transform_shd)

BATCH_SIZE=128

poker_loader_train = torch.utils.data.DataLoader(dataset=poker_train, 
                                           batch_size=1,
                                           collate_fn=tonic.collation.PadTensors())
poker_loader_test = torch.utils.data.DataLoader(dataset=poker_test,
                                                 batch_size=1,
                                                 collate_fn=tonic.collation.PadTensors())

nmnist_loader_train = torch.utils.data.DataLoader(dataset=nmnist_train,
                                            batch_size=1,
                                            collate_fn=tonic.collation.PadTensors())
nmnist_loader_test = torch.utils.data.DataLoader(dataset=nmnist_test,
                                                  batch_size=1,
                                                  collate_fn=tonic.collation.PadTensors())

gesture_loader_train = torch.utils.data.DataLoader(dataset=gesture_train,
                                             batch_size=1,
                                             collate_fn=tonic.collation.PadTensors())
gesture_loader_test = torch.utils.data.DataLoader(dataset=gesture_test,
                                                   batch_size=1,
                                                   collate_fn=tonic.collation.PadTensors())

shd_loader_train = torch.utils.data.DataLoader(dataset=shd_train,
                                             batch_size=1,
                                             collate_fn=tonic.collation.PadTensors())
shd_loader_test = torch.utils.data.DataLoader(dataset=shd_test,
                                               batch_size=1,
                                               collate_fn=tonic.collation.PadTensors())

def plot_frames(frames):
    fig, axes = plt.subplots(1, len(frames))
    for axis, frame in zip(axes, frames):
        axis.imshow(frame[1]-frame[0])
        axis.axis("off")
    plt.tight_layout()
    plt.show()

# for events, targets in gesture_train:
#     # events = padding_crop_transform(events=events)    
#     # tonic.utils.plot_event_grid(events)
#     plot_frames(events)
#
# events = to_sparse_transform(events, sensor_size=700, ordering="txp")
# for events, targets in shd_loader:
    # frames = sparse_tensor_transform(events=events, sensor_size=gesture_sensor_size, ordering='xytp')
    # print(events, targets)
    # tonic.utils.plot_event_grid(events=events, ordering='xytp', axis_array=(1, 3))
    # print(events.shape)
    # break


# for events, targets in gesture_loader:
    # frames = frame_transform(sensor_size=nmnist_sensor_size, events=events, ordering='xypt')
    # plot_frames(events)
    # events = denoise_transform(events=events, sensor_size=gesture_sensor_size, ordering='xypt')
    # print(events)
    # tonic.utils.plot_event_grid(events=events, ordering='xypt', axis_array=(1, 3))

# for frames, targets in nmnist:
#     frames = nmnist_downsample(frames)
#     frames = nmnist_denoise(frames)
#     frames = nmnist_toframe(frames)
#     plot_frames(frames=frames)    

# ts = np.zeros(shape=1, dtype=np.int64) 
i=0
for frames, targets in gesture_loader_train:
    # frames = downsample(frames)
    i = i+1
#     ts.append(frames.shape[0])
print(i)
i=0
for frames, targets in gesture_loader_test:
    i=i+1
#     ts.append(frames.shape[0])
# print(np.mean(ts))

print(i)


